package com.ctask.REPL

import java.io.{ByteArrayInputStream, ByteArrayOutputStream, InputStream, OutputStream}

import org.scalatest.{FlatSpec, Matchers}
import org.scalatest.concurrent.Eventually._

import scala.concurrent._
import java.util.concurrent.Executors

/**
  * REPL spec file.
  */
class REPLSpec extends FlatSpec with Matchers {
  import REPLSpec.withCustomStreams

  behavior of "REPL"

  it should "execute greetings, read, evaluate, print when a command line is provided" in {
    withCustomStreams(new ByteArrayInputStream("SomeCommand\n".getBytes), new ByteArrayOutputStream()) { (os, exc) =>
      implicit val ec: ExecutionContext = exc
      val repl = new DumbREPL()
      Future {
        repl.start()
      }

      eventually {
        repl.greetingCallCount shouldBe 1
        repl.readCallCount shouldBe 1
        repl.printCallCount shouldBe 1
        repl.executeCallCount shouldBe 1
      }
    }
  }

  it should "exit without executing any command if the exit command is provided" in {
    withCustomStreams(new ByteArrayInputStream("exit\n".getBytes), new ByteArrayOutputStream()) { (os, exc) =>
      implicit val ec: ExecutionContext = exc
      val repl = new DumbREPL()
      Future {
        repl.start()
      }

      eventually {
        repl.greetingCallCount shouldBe 1
        repl.readCallCount shouldBe 0
        repl.printCallCount shouldBe 0
        repl.executeCallCount shouldBe 0
        repl.goodbyeCount shouldBe 1
      }
    }
  }

  it should "exit if the end of the input stream is reached" in {
    withCustomStreams(new ByteArrayInputStream("nothing\n".getBytes), new ByteArrayOutputStream()) { (os, exc) =>
      implicit val ec: ExecutionContext = exc

      val repl = new DumbREPL()
      Future {
        repl.start()
      }

      eventually {
        os.toString should include("Reached end of input stream, exiting the repl...")
      }
    }
  }

  it should "exit the repl with a custom message if the Read step returns an Exit command" in {
    withCustomStreams(new ByteArrayInputStream("CustomExit\n".getBytes), new ByteArrayOutputStream()) { (os, exc) =>
      implicit val ec: ExecutionContext = exc

      val repl = new DumbREPL()
      Future {
        repl.start()
      }

      eventually {
        os.toString should include(repl.customExitMsg.get)
      }
    }
  }
}

object REPLSpec {

  /**
    * Allows for the execution of a function in a code block where stdin and stdout are redirected
    * to the provided Input/Output Streams. It provides a newly created ExecutionContext because
    * setting I/O streams and using ExecutionContext does not play well when running multiple tests
    * one after another (second time around the I/O is not correctly redirected to is/os).
    *
    * @param is the InputStream object the Input Stream will be redirected to
    * @param os the OutputStream object the Output Stream will be redirected to
    * @param fn the function to execute
    */
  def withCustomStreams(is: InputStream, os: OutputStream)
                       (fn: (OutputStream, ExecutionContext) => Unit): Unit = {

    try {
      Console.withOut(os) {
        Console.withIn(is) {
          implicit val ec = ExecutionContext.fromExecutorService(Executors.newFixedThreadPool(1))
          fn(os, ec)
        }
      }
    } finally {
      is.close()
      os.close()
    }
  }
}

/**
  * Test REPL implementation.
  */
class DumbREPL extends REPL {

  override val prompt: String = "dumbRepl"

  val customExitMsg = Some("Exiting the repl, death to Zerg")

  var greetingCallCount = 0

  var readCallCount = 0
  var printCallCount = 0
  var executeCallCount = 0
  var goodbyeCount = 0

  override def greetings: Unit = {
    greetingCallCount += 1
  }

  override def goodbye: Unit = goodbyeCount += 1

  override def read(commandLine: String): ReplCommand = {
    readCallCount += 1
    commandLine match {
      case "CustomExit" => Exit(customExitMsg)
      case _ => new EmptyReplCommand
    }
  }

  override def print(result: Result): Unit = {
    printCallCount += 1
  }

  override def evaluate(command: ReplCommand): Result = {
    executeCallCount += 1
    new EmptyResult
  }

  class EmptyReplCommand extends ReplCommand

  class EmptyResult extends Result

}
