package com.ctask.client

import java.time.{ZoneId, ZonedDateTime}

import com.ctask.data.Task.Recurrence
import com.ctask.messages._
import org.scalatest.{FlatSpec, Matchers}

/**
  * Test suite for CommandGenerator.
  */
class ReplCommandGeneratorSpec extends FlatSpec with Matchers {

  behavior of "CommandGenerator with non-existent commands"

  it should "create an InvalidCommand when the command line does not contain a valid command" in {
    val command = "nonexistent command"
    val commandLine = s"$command"
    val generatedCommand = ReplCommandGenerator(commandLine, None)
    val expectedCommand = InvalidReplCommand(s"Unknown command: $command")

    generatedCommand shouldBe expectedCommand
  }

  behavior of "CommandGenerator with add tasklist"

  it should "create an add tasklist command when provided with a valid command line" in {
    val taskListName = "test TaskList"
    val commandLine = s"add tasklist -n $taskListName"
    val generatedCommand = ReplCommandGenerator(commandLine, None)

    generatedCommand.asInstanceOf[ServerCommand].command shouldBe an[AddTaskList]
  }

  it should "create an add tasklist command with the provided email" in {
    val taskListName = "test TaskList"
    val taskListEmail = "mods@mods.com"
    val commandLine = s"add tasklist -n $taskListName -e $taskListEmail"
    val generatedCommand = ReplCommandGenerator(commandLine, None)

    generatedCommand.asInstanceOf[ServerCommand].command shouldBe an[AddTaskList]
    generatedCommand.asInstanceOf[ServerCommand].command.asInstanceOf[AddTaskList].email.get shouldBe taskListEmail
  }

  it should "create an InvalidCommand with a pertinent message when invoked without a tasklist name" in {
    val commandLine = "add tasklist"
    val generatedCommand = ReplCommandGenerator(commandLine, None)
    val expectedCommand = InvalidReplCommand("The add tasklist command was missing the mandatory -n switch.")

    generatedCommand shouldBe expectedCommand
  }


  behavior of "CommandGenerator with rm tasklist"

  it should "create a RemoveTaskList command when the correct command line is provided" in {
    val taskListName = "test TaskList"
    val commandLine = s"rm tasklist -n $taskListName"
    val generatedCommand = ReplCommandGenerator(commandLine, None)

    generatedCommand.asInstanceOf[ServerCommand].command shouldBe a[RemoveTaskList]
  }

  it should "create an InvalidCommand with a pertinent message when invoked without a tasklist name" in {
    val commandLine = "rm tasklist"
    val generatedCommand = ReplCommandGenerator(commandLine, None)
    val expectedCommand = InvalidReplCommand("The rm tasklist command was missing the mandatory -n switch.")

    generatedCommand shouldBe expectedCommand
  }

  behavior of "CommandGenerator with show tasklist"

  it should "create a valid ShowTaskList command when the correct command line is provided" in {
    val commandLine = "show tasklists"
    val generatedCommand = ReplCommandGenerator(commandLine, None)

    generatedCommand.asInstanceOf[ServerCommand].command shouldBe ShowTaskLists
  }

  behavior of "CommandGenerator with add task"

  it should "create a valid AddTaskToTaskList command when the correct command with description" in {
    val taskListName = "test tasklist"
    val expectedCommand = AddTaskToTaskList("new task", s"$taskListName", Option("test task in tasklist"), None, Recurrence.NEVER)
    val commandLine = s"add " +
      s"-n ${expectedCommand.name} " +
      s"-d ${expectedCommand.description.get}"

    val generatedCommand = ReplCommandGenerator(commandLine, Some(taskListName))

    generatedCommand.asInstanceOf[ServerCommand].command shouldBe expectedCommand
  }

  it should "create a valid AddTaskToTaskList command when the correct command without description is provided" in {
    val taskListName = "test tasklist"
    val expectedCommand = AddTaskToTaskList("new task", s"$taskListName", None, None, Recurrence.NEVER)
    val commandLine = s"add " +
      s"-n ${expectedCommand.name} "

    val generatedCommand = ReplCommandGenerator(commandLine, Some(taskListName))

    generatedCommand.asInstanceOf[ServerCommand].command shouldBe expectedCommand
  }

  it should "create an InvalidCommand with a pertinent message when invoked with a missing tasklist name" in {
    val commandLine = s"add -n new task "
    val generatedCommand = ReplCommandGenerator(commandLine, None)

    generatedCommand shouldBe an[InvalidReplCommand]
  }

  it should "create an InvalidCommand with a pertinent message when invoked with a missing task name" in {
    val commandLine = s"add "
    val generatedCommand = ReplCommandGenerator(commandLine, None)

    generatedCommand shouldBe an[InvalidReplCommand]
  }

  it should "create an invalid command if an invalid due date is provided" in {
    ReplCommandGenerator(s"add -n test task -dd invalid date", Some("testList")) shouldBe an[InvalidReplCommand]
  }

  it should "create a valid command if a valid due date is provided" in {
    val dueDateStr = "10:30 3/12/2017"
    val dueDate = ZonedDateTime.of(2017, 12, 3, 10, 30, 0, 0, ZoneId.systemDefault())
    val taskName = "test task"
    val generatedCommand = ReplCommandGenerator(s"add -n $taskName -dd $dueDateStr", Some("testList"))
    generatedCommand shouldBe a[ServerCommand]
    generatedCommand.asInstanceOf[ServerCommand].command shouldBe an[AddTaskToTaskList]
    generatedCommand.asInstanceOf[ServerCommand].command.asInstanceOf[AddTaskToTaskList].name shouldBe taskName
    generatedCommand.asInstanceOf[ServerCommand].command.asInstanceOf[AddTaskToTaskList].dueDate.get shouldBe dueDate
  }

  behavior of "CommandGenerator with rm task from list"

  it should "create a valid RemoveTaskFromTaskList command when the correct command with description" in {
    val taskListName = "test tasklist"
    val taskId = 1
    val expectedCommand = RemoveTaskFromTaskList(taskId, s"$taskListName")
    val commandLine = s"rm -t $taskId"

    val generatedCommand = ReplCommandGenerator(commandLine, Some(taskListName))

    generatedCommand.asInstanceOf[ServerCommand].command shouldBe expectedCommand
  }

  it should "create an InvalidCommand when invoked with a missing task id" in {
    val commandLine = "rm "
    val generatedCommand = ReplCommandGenerator(commandLine, Some("testTaskList"))

    generatedCommand shouldBe an[InvalidReplCommand]
  }

  it should "create an InvalidCommand when invoked with a missing task list name" in {
    val taskId = 1
    val commandLine = s"rm -t $taskId"
    val generatedCommand = ReplCommandGenerator(commandLine, None)

    generatedCommand shouldBe an[InvalidReplCommand]
  }

  it should "create an InvalidCommand when invoked with a non numeric task id" in {
    val commandLine = "rm -t non-numeric"
    val generatedCommand = ReplCommandGenerator(commandLine, Some("testTaskList"))

    generatedCommand shouldBe an[InvalidReplCommand]
  }

  behavior of "ReplCommandGenerator with use tasklist"

  it should "create a UseTaskList command when the correct use command is provided" in {
    val testListName = "test list"
    val generatedCommand = ReplCommandGenerator(s"use $testListName", None)

    generatedCommand shouldBe UseTasklistCommand(testListName)
  }

  it should "create an invalid command when invoked without the task list name argument" in {
    ReplCommandGenerator("use", None) shouldBe an[InvalidReplCommand]
  }

  behavior of "ReplCommandGenerator with ls"

  it should "create an invalid command when invoked without the task list name argument" in {
    ReplCommandGenerator("ls", None) shouldBe an[InvalidReplCommand]
  }

  behavior of "ReplCommandGenerator with done"

  it should "create a MarkDone command when the correct done command is provided" in {
    val testListName = "testListName"
    val generatedCommand = ReplCommandGenerator("done -t 1", Some(testListName))
    generatedCommand.asInstanceOf[ServerCommand].command shouldBe MarkDone(1, testListName)
  }

  it should "create an invalid command when the mandatory -t switch is not provided" in {
    ReplCommandGenerator("done", Some("testListName")) shouldBe an[InvalidReplCommand]
  }

  it should "create an invalid command when invoked without the task list name argument" in {
    ReplCommandGenerator("done -t 3", None) shouldBe an[InvalidReplCommand]
  }

  it should "create an invalid command when invoked with a non numeric task id" in {
    ReplCommandGenerator("done -t nonnumeric", Some("testListName")) shouldBe an[InvalidReplCommand]
  }

  behavior of "ReplCommandGenerator with modify tasklist"

  it should "create a ModifyTasklist command when a command without other switches is provided" in {
    val taskListName = "taskListName"
    val generatedCommand = ReplCommandGenerator(s"modify tasklist -n $taskListName", None).asInstanceOf[ServerCommand].command

    generatedCommand shouldBe a[ModifyTaskList]
    generatedCommand.asInstanceOf[ModifyTaskList].taskListName shouldBe taskListName
  }

  it should "create a ModifyTasklist command when a command only the -e switch is provided" in {
    val taskListName = "taskListName"
    val taskListEmail = "my@email.com"
    val generatedCommand = ReplCommandGenerator(s"modify tasklist -n $taskListName -e $taskListEmail", None).asInstanceOf[ServerCommand].command

    generatedCommand shouldBe a[ModifyTaskList]
    generatedCommand.asInstanceOf[ModifyTaskList].taskListName shouldBe taskListName
    generatedCommand.asInstanceOf[ModifyTaskList].email.get shouldBe taskListEmail
  }

  it should "create a ModifyTasklist command when a command only the -nn switch is provided" in {
    val taskListName = "taskListName"
    val taskListNewName = "taskListNewName"
    val generatedCommand = ReplCommandGenerator(s"modify tasklist -n $taskListName -nn $taskListNewName", None).asInstanceOf[ServerCommand].command

    generatedCommand shouldBe a[ModifyTaskList]
    generatedCommand.asInstanceOf[ModifyTaskList].taskListName shouldBe taskListName
    generatedCommand.asInstanceOf[ModifyTaskList].email shouldBe None
    generatedCommand.asInstanceOf[ModifyTaskList].newName.get shouldBe taskListNewName
  }

  it should "create an InvalidCommand when a command with an empty -nn switch value is provided" in {
    ReplCommandGenerator(s"modify tasklist -n testList -nn", None) shouldBe an[InvalidReplCommand]
  }

  it should "create an InvalidCommand when a command without the -n switch is provided" in {
    ReplCommandGenerator(s"modify tasklist -nn newName", None) shouldBe an[InvalidReplCommand]
  }

  behavior of "ReplCommandGenerator with modify task"

  it should "create a ModifyTask command when only the taskId is provided" in {
    val taskId = 3
    val selectedTaskList = "testList"
    val generatedCommand = ReplCommandGenerator(s"modify -t $taskId", Some(selectedTaskList))

    generatedCommand.asInstanceOf[ServerCommand].command shouldBe a[ModifyTask]
    generatedCommand.asInstanceOf[ServerCommand].command.asInstanceOf[ModifyTask].taskId shouldBe taskId
    generatedCommand.asInstanceOf[ServerCommand].command.asInstanceOf[ModifyTask].description shouldBe None
    generatedCommand.asInstanceOf[ServerCommand].command.asInstanceOf[ModifyTask].dueDate shouldBe None
    generatedCommand.asInstanceOf[ServerCommand].command.asInstanceOf[ModifyTask].taskListName shouldBe selectedTaskList
    generatedCommand.asInstanceOf[ServerCommand].command.asInstanceOf[ModifyTask].taskNameOpt shouldBe None
  }

  it should "create an invalid command when an invalid task id is provided" in {
    val invalidTaskId = "non valid id"
    val selectedTaskList = "testList"
    ReplCommandGenerator(s"modify -t $invalidTaskId", Some(selectedTaskList)) shouldBe an[InvalidReplCommand]
  }

  it should "create an invalid command when an invalid due date is provided" in {
    ReplCommandGenerator(s"modify -t 3 -dd invaliddate", Some("testList")) shouldBe an[InvalidReplCommand]
  }

  it should "create a ModifyTask command when a valid due date is provided" in {
    val taskId = 8
    val selectedTaskList = "testList"
    val dueDateStr = "11:30 22/10/1999"
    val zonedDateTime = ZonedDateTime.of(1999, 10, 22, 11, 30, 0, 0, ZoneId.systemDefault())
    val generatedCommand = ReplCommandGenerator(s"modify -t $taskId -dd $dueDateStr", Some(selectedTaskList))

    generatedCommand.asInstanceOf[ServerCommand].command shouldBe a[ModifyTask]
    generatedCommand.asInstanceOf[ServerCommand].command.asInstanceOf[ModifyTask].taskId shouldBe taskId
    generatedCommand.asInstanceOf[ServerCommand].command.asInstanceOf[ModifyTask].taskListName shouldBe selectedTaskList
    generatedCommand.asInstanceOf[ServerCommand].command.asInstanceOf[ModifyTask].dueDate.get shouldBe zonedDateTime
  }

  it should "create an invalid command if a task list is not already selected" in {
    ReplCommandGenerator(s"modify -t 3", None) shouldBe an[InvalidReplCommand]
  }

  it should "create an invalid command if a task id is not provided" in {
    ReplCommandGenerator(s"modify", None) shouldBe an[InvalidReplCommand]
  }

  it should "create a ModifyTask when all the optional parameters are provided" in {
    val taskId = 8
    val selectedTaskList = "testList"
    val dueDateStr = "11:30 22/10/1999"
    val taskName = "new task name"
    val description = "new description"
    val zonedDateTime = ZonedDateTime.of(1999, 10, 22, 11, 30, 0, 0, ZoneId.systemDefault())
    val generatedCommand = ReplCommandGenerator(s"modify -t $taskId -n $taskName -d $description -dd $dueDateStr", Some(selectedTaskList))

    generatedCommand.asInstanceOf[ServerCommand].command shouldBe a[ModifyTask]
    generatedCommand.asInstanceOf[ServerCommand].command.asInstanceOf[ModifyTask].taskId shouldBe taskId
    generatedCommand.asInstanceOf[ServerCommand].command.asInstanceOf[ModifyTask].taskListName shouldBe selectedTaskList
    generatedCommand.asInstanceOf[ServerCommand].command.asInstanceOf[ModifyTask].dueDate.get shouldBe zonedDateTime
    generatedCommand.asInstanceOf[ServerCommand].command.asInstanceOf[ModifyTask].taskNameOpt.get shouldBe taskName
    generatedCommand.asInstanceOf[ServerCommand].command.asInstanceOf[ModifyTask].description.get shouldBe description
  }

  it should "create an AddTaskToTaskList command with the correct recurrence" in {
    val taskListName = "test tasklist"
    val expectedCommand = AddTaskToTaskList("new task", s"$taskListName", Option("test task in tasklist"), None, Recurrence.DAILY)
    val commandLine = s"add " +
      s"-n ${expectedCommand.name} " +
      s"-d ${expectedCommand.description.get} " +
       "-r daily"

    val generatedCommand = ReplCommandGenerator(commandLine, Some(taskListName))

    generatedCommand.asInstanceOf[ServerCommand].command shouldBe expectedCommand
  }

  it should "fail to create an AddTaskToTaskList command if an invalid recurrence is provided" in {
    val taskListName = "test tasklist"
    val commandLine = s"add " +
      s"-n name " +
      s"-d desc " +
       "-r nonvalid"

     ReplCommandGenerator(commandLine, Some(taskListName)) shouldBe an[InvalidReplCommand]
  }

  behavior of "ReplCommandGenerator with get tasklist"

  it should "create a GetTaskList command when the correct command line is provided" in {
    val taskListName = "test tasklist"
    val expectedCommand = GetTaskList(taskListName, false)
    val commandLine = s"ls -n $taskListName"

    val generatedCommand = ReplCommandGenerator(commandLine, None)
    generatedCommand.asInstanceOf[ServerCommand].command shouldBe expectedCommand
  }

  it should "create an InvalidCommand when the mandatory -n switch is not provided" in {
    val taskListName = "test tasklist"
    val commandLine = s"ls"

    ReplCommandGenerator(commandLine, None) shouldBe an[InvalidReplCommand]
  }
}
