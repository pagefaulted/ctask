package com.ctask.client

import java.nio.file.{Files, Paths}

import com.ctask.client.util.ClientProperties
import com.ctask.utils.FileUtils
import org.scalatest.{BeforeAndAfterAll, BeforeAndAfterEach, FlatSpec, Matchers}

class TaskListInUseSpec extends FlatSpec with Matchers with BeforeAndAfterAll with BeforeAndAfterEach {

  private def testRoot = "/tmp/" + System.nanoTime()

  behavior of "TaskListInUse"

  it should "persist the task list in use" in {
    withTestDir(testRoot) { testDirPath =>
      val testListInUse = "in use"
      TaskListInUse.persist(testListInUse)
      Files.exists(Paths.get(testDirPath)) shouldBe true
    }
  }

  it should "load the task list in use" in {
    withTestDir(testRoot) { _ =>
      val testListInUse = "in use"
      TaskListInUse.persist(testListInUse)
      TaskListInUse.load.get shouldBe testListInUse
    }
  }

  it should "override a previously persisted task list in use" in {
    withTestDir(testRoot) { _ =>
      val firstTaskList = "in use2"
      val secondTaskList = "in use2"
      TaskListInUse.persist(firstTaskList)
      TaskListInUse.persist(secondTaskList)
      TaskListInUse.load.get shouldBe secondTaskList
    }
  }

  it should "return None if there is no task list in use previously persisted" in {
    withTestDir(testRoot) { _ =>
      TaskListInUse.load shouldBe None
    }
  }

  /**
    * Execute the provided test function with the provided test dir.
    *
    * @param testDir the test dir, will be created and removed within this function
    * @param test    the test to execute, which will be provided with the test dir
    */
  def withTestDir(testDir: String)(test: (String) => Unit): Unit = {
    FileUtils.mkdir(testDir)
    System.setProperty(ClientProperties.rootCtaskClientStorageFolderProp, testDir)

    test(testDir)

    FileUtils.rmDir(testDir)
    System.clearProperty(ClientProperties.rootCtaskClientStorageFolderProp)
  }
}
