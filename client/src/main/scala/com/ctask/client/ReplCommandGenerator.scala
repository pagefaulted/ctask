package com.ctask.client

import com.ctask.messages._
import com.ctask.REPL.ReplCommand
import com.ctask.utils.ArgsUtils

/**
  * Generates a Command from the provided command line.
  */
object ReplCommandGenerator {

  /**
    * Creates an instance of a specific command from the provided command line.
    *
    * @param commandLine      the command line
    * @param selectedTaskList the option with the selected task list
    * @return an instance the ReplCommand to execute
    */
  def apply(commandLine: String, selectedTaskList: Option[String]): ReplCommand = {
    // Parse client command first, if no client command is recognized, parse server command
    parseClientCmd(commandLine).getOrElse(parseServerCmd(commandLine, selectedTaskList))
  }

  // TODO: refactor, see #126
  // scalastyle:off
  def parseServerCmd(commandLine: String, selectedTaskList: Option[String]): ReplCommand = {

    val mainCommand = commandLine.split("-").head.trim
    val args = {
      val indexOfFirstSwitch = commandLine.indexOf('-')
      if (indexOfFirstSwitch != -1) {
        commandLine.substring(indexOfFirstSwitch, commandLine.length)
      }
      else {
        ""
      }
    }

    val argsMap = ArgsUtils.parse(args.split(" "))

    val serverCmd = mainCommand match {
      case AddTaskToTaskListFactory.commandString => AddTaskToTaskListFactory(argsMap, selectedTaskList)
      case AddTaskListFactory.commandString => AddTaskListFactory(argsMap)
      case RemoveTaskListFactory.commandString => RemoveTaskListFactory(argsMap)
      case ShowTaskListsFactory.commandString => ShowTaskLists
      case RemoveTaskFromListFactory.commandString => RemoveTaskFromListFactory(argsMap, selectedTaskList)
      case MarkTaskAsDoneFactory.commandString => MarkTaskAsDoneFactory(argsMap, selectedTaskList)
      case ModifyTaskListFactory.commandString => ModifyTaskListFactory(argsMap)
      case ModifyTaskFactory.commandString => ModifyTaskFactory(argsMap, selectedTaskList)
      case GetTaskListFactory.commandString => GetTaskListFactory(argsMap, selectedTaskList)
      case _ => InvalidCommand(s"Unknown command: $mainCommand")
    }

    serverCmd match {
      case invalid: InvalidCommand => InvalidReplCommand(invalid.message)
      case valid => ServerCommand(valid)
    }
  }
  // scalastyle:on

  def parseClientCmd(commandLine: String): Option[ReplCommand] = {
    if (commandLine.split(" ").head == "use") {
      val taskListName = commandLine.split(" ").tail.mkString(" ")
      if (taskListName.nonEmpty) {
        Some(UseTasklistCommand(taskListName))
      } else {
        Some(InvalidReplCommand(s"The use command requires a tasklist name"))
      }
    } else {
      None
    }
  }
}
