package com.ctask.client

import java.nio.file.{Files, Paths, StandardOpenOption}

import com.ctask.client.util.ClientProperties
import play.api.libs.json.Json

import scala.util.{Failure, Success, Try}

/**
  * Handles persistence for the task list in use.
  */
object TaskListInUse {

  implicit val taskListInUseJsonRead = Json.reads[TaskListInUse]

  implicit val taskListWrites = Json.writes[TaskListInUse]

  def taskListInUsePath: String = s"${ClientProperties.rootCtaskStorageFolder}/taskListInUse"

  /**
    * Persists the current task list in use.
    * @param taskListName the task list in use to be persisted
    */
  def persist(taskListName: String): Unit = {
    Try {
      val jsValue = Json.toJson(TaskListInUse(taskListName))
      val path = Paths.get(taskListInUsePath)
      Files.write(path, jsValue.toString().getBytes(), StandardOpenOption.CREATE, StandardOpenOption.TRUNCATE_EXISTING)
      taskListName
    } match {
      case Success(_) =>
      case Failure(ex) => // TODO: cannot log here for now. Log to log file when proper logging is implemented.
    }
  }

  /**
    * Load the last task list in use.
    * @return the name of the last task list in use
    */
  def load: Option[String] = {
    Try {
      val path = Paths.get(taskListInUsePath)
      val jsonString = String.join("", Files.readAllLines(path))
      Json.parse(jsonString).validate[TaskListInUse].get
    } match {
      case Success(taskListInUse) => Some(taskListInUse.name)
      case Failure(ex) =>
        // TODO: cannot log here for now. Log to log file when proper logging is implemented.
        None
    }
  }
}

case class TaskListInUse(name: String)
