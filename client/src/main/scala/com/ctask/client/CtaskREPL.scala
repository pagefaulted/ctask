package com.ctask.client

import java.time.{LocalDate, ZonedDateTime}

import akka.actor.ActorSystem
import akka.util.Timeout
import com.ctask.data.{Task, TaskList}
import com.ctask.data.TaskJsonUtils.TaskOrdering
import com.ctask.messages.Command
import com.ctask.messages._
import com.ctask.REPL._
import com.ctask.http.client.RequestExecutor
import com.ctask.http.client.RequestExecutor.AnswerOrError
import com.ctask.http.client.akka.AkkaClient
import com.ctask.messages.ServerResponses.{AddedTaskList, RemovedTask, RemovedTaskList, ServerError, SingleList, TaskLists, UpdatedTaskList}

import scala.concurrent.ExecutionContextExecutor
import scala.concurrent.duration._
import scala.jdk.CollectionConverters._

// Repl results

case class AllTaskLists(taskLists: List[TaskList]) extends Result {
  override def toString: String = {
    if (taskLists.isEmpty) {
      "There are no task lists"
    } else {
      s"Task Lists:\n${taskLists.mkString("* ", "\n* ", "")}"
    }
  }
}

case class TasksInList(tasks: List[Task]) extends Result {

  //scalastyle:off
  override def toString: String = {

    val taskPrependString = "  * "
    val taskAppendString = "\n  * "

    if(tasks.isEmpty) {
      "There are no tasks in this list"
    } else {
      val (completed, nonCompleted) = tasks.partition(_.done)

      val nonCompletedStr = if(nonCompleted.nonEmpty) {
        val end = if(completed.isEmpty) "" else "\n"

        val (withDueDate, withoutDueDate) = nonCompleted.partition(_.dueDate != null)

        val (nonExpired, expired) = withDueDate.partition(_.dueDate.isAfter(ZonedDateTime.now))

        val today = nonExpired.collect {
          case task: Task if task.dueDate != null && task.dueDate.toLocalDate.equals(LocalDate.now) => task
        }.sorted

        val tomorrow = nonExpired.collect {
          case task: Task if task.dueDate != null && task.dueDate.toLocalDate.equals(LocalDate.now.plusDays(1)) => task
        }.sorted

        val othersWithDueDate = (nonExpired diff today diff tomorrow).sorted

        val expiredStr = if(expired.nonEmpty) {
          val expSorted = expired.sorted
          val endString = if(today.nonEmpty || tomorrow.nonEmpty || othersWithDueDate.nonEmpty || withoutDueDate.nonEmpty) {
            "\n"
          } else { "" }
          s"EXPIRED:\n" +
            s"${expSorted.mkString(taskPrependString, taskAppendString, endString)}"
        } else { "" }

        val todayStr = if(today.nonEmpty) {
          val endString = if(tomorrow.nonEmpty || othersWithDueDate.nonEmpty || withoutDueDate.nonEmpty) "\n" else ""
          "Today:\n" +
            s"${today.mkString(taskPrependString, taskAppendString, endString)}"
        } else { "" }

        val tomorrowStr = if(tomorrow.nonEmpty) {
          val endString = if(othersWithDueDate.nonEmpty || withoutDueDate.nonEmpty) "\n" else ""
          "Tomorrow:\n" +
            s"${tomorrow.mkString(taskPrependString, taskAppendString, endString)}"
        } else { "" }

        val otherWithDueDateStr = if(othersWithDueDate.nonEmpty) {
          val endString = if(withoutDueDate.nonEmpty) "\n" else ""
          s"${othersWithDueDate.mkString(taskPrependString, taskAppendString, endString)}"
        } else { "" }

        val withoutDueDateStr = if(withoutDueDate.nonEmpty) {
          s"${withoutDueDate.mkString(taskPrependString, taskAppendString, "")}"
        } else { "" }

        val othersStr = if(othersWithDueDate.nonEmpty || withoutDueDate.nonEmpty) {
          "Others:\n" +
            s"$otherWithDueDateStr$withoutDueDateStr"
        } else { "" }

        s"$expiredStr$todayStr$tomorrowStr$othersStr$end"
      } else { "" }

      val completedStr = if(completed.nonEmpty) {
        s"Completed:\n${completed.sorted.mkString(taskPrependString, taskAppendString, "")}"
      } else { "" }

      s"\n$nonCompletedStr$completedStr"
    }
  }
  //scalastyle:on
}

case class CommandSuccessful(msg: String) extends Result {
  override def toString(): String = {
    s"Success: $msg"
  }
}

case class Error(msg: String, throwable: Option[Throwable] = None) extends Result {
  override def toString: String = {
    s"$msg ${throwable.getOrElse("")}"
  }
}

case class InvalidCommandError(msg: String) extends Result {
  override def toString: String = {
    s"$msg"
  }
}

case class UnknownServerResponse(msg: String) extends Result {
  override def toString: String = {
    s"Unknown server response: $msg"
  }
}

// Repl commands

class ClientCommand extends ReplCommand

case class UseTasklistCommand(taskListName: String) extends ClientCommand

case class ServerCommand(command: Command) extends ReplCommand

case class InvalidReplCommand(msg: String) extends ReplCommand

/**
  * Repl implementation for Ctask
  */
class CtaskREPL() extends REPL {

  implicit val system: ActorSystem = ActorSystem()
  implicit val executionContext: ExecutionContextExecutor  = system.dispatcher
  val requestExecutor = new RequestExecutor(new AkkaClient(system))

  implicit val duration: Timeout = 20 seconds

  var selectedTaskList: Option[String] = TaskListInUse.load

  override def prompt: String = {
    val repl = "ctask"
    selectedTaskList match {
      case Some(taskList) => s"$repl:$taskList"
      case None => repl
    }
  }

  override def greetings(): Unit = println(
      "   _____ _            _      _____  ______ _____  _      \n" +
      "  / ____| |          | |    |  __ \\|  ____|  __ \\| |     \n" +
      " | |    | |_ __ _ ___| | __ | |__) | |__  | |__) | |     \n" +
      " | |    | __/ _` / __| |/ / |  _  /|  __| |  ___/| |     \n" +
      " | |____| || (_| \\__ \\   <  | | \\ \\| |____| |    | |____ \n" +
      "  \\_____|\\__\\__,_|___/_|\\_\\ |_|  \\_\\______|_|    |______|\n" +
      "                                     roberto@pagefault.io\n\n")

  override def goodbye(): Unit = {
    println("See you soon!")
    system.terminate()
  }

  override def read(commandLine: String): ReplCommand = {
    ReplCommandGenerator(commandLine.trim, selectedTaskList)
  }

  override def evaluate(command: ReplCommand): Result = {
    command match {
      case serverCmd: ServerCommand =>
        handleServerCommandWithHttpRequest(serverCmd)
      case clientCmd: ClientCommand =>
        handleClientCommand(clientCmd)
      case invalidCmd: InvalidReplCommand =>
        InvalidCommandError(s"${invalidCmd.msg}")
    }
  }

  override def print(result: Result): Unit = {
    println(s"$result\n")
  }

  /**
    * Transforms the server response to a Result for the print step.
    *
    * @param serverResponse the server response
    * @return a result that represents the server response
    */
  def response2result(serverResponse: Any): Result = {
    serverResponse match {
      case taskLists: TaskLists =>
        AllTaskLists(taskLists.taskLists.asScala.toList)
      case addedTaskList: AddedTaskList =>
        CommandSuccessful(s"Task list added: '${addedTaskList.list.name}'")
      case removedTaskList: RemovedTaskList =>
        CommandSuccessful(s"Task list removed: '${removedTaskList.list.name}'")
      case updatedTaskList: UpdatedTaskList =>
        CommandSuccessful(s"Task list updated: '${updatedTaskList.list.name}'")
      case removedTaskList: RemovedTask =>
        CommandSuccessful(s"Task ${removedTaskList.removedTask} removed")
      case singleList: SingleList =>
        TasksInList(singleList.list.tasks.toList)
      case serverError: ServerError =>
        Error(s"Server error: ${serverError.msg}")
      case unknown =>
        UnknownServerResponse(s"$unknown")
    }
  }

  def handleClientCommand(command: ClientCommand): Result = {
    command match {
      case UseTasklistCommand(taskListName) =>
        selectedTaskList = Option(taskListName)
        persistTaskListInUse(taskListName)
        CommandSuccessful(s"Selected task list $taskListName")
      case cmd =>
        InvalidCommandError(s"Could not recognize client command $cmd")
    }
  }

  //noinspection ScalaStyle
  def handleServerCommandWithHttpRequest(command: ServerCommand): Result = {
    val answerOrError = command.command match {
      case addTask: AddTaskToTaskList =>
        val dueDateStr = addTask.dueDate match {
          case Some(zonedDateTime) => zonedDateTime.toString
          case None => ""
        }

        requestExecutor.execAddTaskRequest(addTask.name,
          addTask.taskListName,
          addTask.description.getOrElse(""),
          dueDateStr,
          addTask.recurrence)
      case addTaskList: AddTaskList =>
        requestExecutor.execAddTaskList(addTaskList.name, addTaskList.email.getOrElse(""))
      case getTaskList: GetTaskList  =>
        requestExecutor.execGetTaskList(getTaskList.taskListName, getTaskList.withCompleted)
      case markDone: MarkDone =>
        requestExecutor.execMaskTaskDoneRequest(markDone.taskId, markDone.taskListName)
      case modifyTask: ModifyTask =>
        requestExecutor.execModifyTaskRequest(
          modifyTask.taskId,
          modifyTask.taskListName,
          modifyTask.taskNameOpt.orNull,
          modifyTask.description.orNull,
          modifyTask.dueDate.map(_.toString).orNull,
          modifyTask.recurrence)
      case _ => AnswerOrError.left(new ServerError(s"Could not handle command $command with http request"))
    }

    if (answerOrError.isAnswer) {
      response2result(answerOrError.response)
    } else {
      response2result(answerOrError.error)
    }
  }

  /**
    * Persists the tasklist in use.
    *
    * @param taskListName the task list in use
    */
  def persistTaskListInUse(taskListName: String): Unit = {
    TaskListInUse.persist(taskListName)
  }
}
