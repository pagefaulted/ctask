package com.ctask.client.util

import com.ctask.utils.PropertiesRetriever
import com.typesafe.config.{Config, ConfigFactory}

object ClientProperties extends PropertiesRetriever {

  override def config: Config = ConfigFactory.load()

  /**
    * Client root storage folder.
    */
  val rootCtaskClientStorageFolderProp: String = "com.ctask.client.storage.root"
  val rootCtaskClientStorageFolderDefault: String = System.getProperty("user.home") + "/.ctask"
  def rootCtaskStorageFolder: String = getPropertyStr(rootCtaskClientStorageFolderProp, rootCtaskClientStorageFolderDefault)
}
