package com.ctask.client

import akka.actor.ActorSystem
import com.typesafe.config.ConfigFactory

import scala.concurrent.Await
import scala.concurrent.duration._

/**
  * CLI main.
  */
object ClientMain {
  def main(args: Array[String]): Unit = {
    val configFactory = ConfigFactory.load("client")
    val system = ActorSystem("CtaskServerSystem", configFactory)

    new CtaskREPL().start()
    Await.result(system.terminate(), 10 seconds)
  }
}
