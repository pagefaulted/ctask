package com.ctask.REPL

/**
  * This is a bare minimum trait for the implementation of a REPL.
  */
trait REPL {

  /**
    * The prompt of the REPL.
    */
  def prompt: String

  /**
    * Greetings for the REPL.
    */
  def greetings(): Unit

  /**
    * Goodbye.
    */
  def goodbye(): Unit

  /**
    * Reads the input and produces a Command.
    *
    * @param commandLine the command line
    * @return the command
    */
  def read(commandLine: String): ReplCommand

  /**
    * Executes the provided command.
    *
    * @param command the command to execute
    * @return the result of the execution
    */
  def evaluate(command: ReplCommand): Result

  /**
    * Prints the provided results.
    *
    * @param result the results to print
    */
  def print(result: Result): Unit

  /**
    * Main REPL loop.
    */
  def start(): Unit = {
    greetings()

    var running = true

    while (running) {
      try {
        doRead() match {
          case exit: Exit =>
            println(exit.msg.getOrElse(""))
            running = false
          case command: ReplCommand =>
            print(evaluate(command))
        }
      } catch {
        case ex: Exception => println(s"An error occurred: ${ex}")
      }
    }

    goodbye()
  }

  private def doRead(): ReplCommand = {
    scala.Predef.print(s"$prompt " + "$> ")

    //noinspection ScalaStyle
    scala.io.StdIn.readLine match {
      case null =>
        Exit(Some("Reached end of input stream, exiting the repl..."))
      case "exit" =>
        Exit()
      case commandLine: String =>
        read(commandLine)
    }
  }
}

/**
  * Instances of Command are returned by the Read step to be passed to the Evaluate step.
  */
trait ReplCommand

/**
  * Instances of Result are returned by the Evaluate step to be passed to the Print step.
  */
trait Result

/**
  * If the Read step returns an instance of this Command, the REPL will exit.
  *
  * @param msg an optional exit message that will be printed upon exit.
  */
case class Exit(msg: Option[String] = None) extends ReplCommand
