package com.ctask.http.util

import akka.http.scaladsl.model.{HttpRequest, HttpResponse}
import akka.http.scaladsl.unmarshalling.{FromRequestUnmarshaller, FromResponseUnmarshaller, PredefinedFromEntityUnmarshallers}
import akka.stream.Materializer
import com.ctask.protocol.Envelope.RootEnvelopeProto
import com.ctask.protocol.Envelope.RootEnvelopeProto.MissiveType
import com.ctask.protocol.commands.Commands.{AddTaskListProto, AddTaskToTaskListProto, CommandEnvelopeProto,
  GetTaskListProto, MarkTaskDoneProto, ModifyTaskProto}

import scala.language.implicitConversions
import scala.concurrent.{ExecutionContext, Future}

/**
  * Implicit converters for proto classes
  */
object ProtoConverters {

  implicit val requestUnmarshaller: RequestUnmarshaller = new RequestUnmarshaller
  implicit val responseUnmarshaller: ResponseUnmarshaller = new ResponseUnmarshaller

  /**
    * RootEnvelopeProto -> CommandEnvelopeProto
    */
  implicit def rootEnv2CommandEnv(rootEnv: RootEnvelopeProto): CommandEnvelopeProto =
    CommandEnvelopeProto.parseFrom(rootEnv.getPayload)

  /**
    * CommandEnvelopeProto -> GetTaskListProto
    */
  implicit def commandEnvProto2GetTaskListProto(cmdEnvProto: CommandEnvelopeProto): GetTaskListProto =
    GetTaskListProto.parseFrom(cmdEnvProto.getPayload)

  /**
    * CommandEnvelopeProto -> MarkTaskDoneProto
    */
  implicit def commandEnvProto2MarkTaskDoneProto(cmdEnvProto: CommandEnvelopeProto): MarkTaskDoneProto =
    MarkTaskDoneProto.parseFrom(cmdEnvProto.getPayload)

  /**
    * CommandEnvelopeProto -> AddTaskListProto
    */
  implicit def commandEnvProto2AddTaskListProto(cmdEnvProto: CommandEnvelopeProto): AddTaskListProto =
    AddTaskListProto.parseFrom(cmdEnvProto.getPayload)

  /**
    * CommandEnvelopeProto -> AddTaskToTaskListProto
    */
  implicit def commandEnvProto2AddTaskToTaskListProto(cmdEnvProto: CommandEnvelopeProto): AddTaskToTaskListProto =
    AddTaskToTaskListProto.parseFrom(cmdEnvProto.getPayload)

  /**
    * CommandEnvelopeProto -> ModifyTaskProto
    */
  implicit def commandEnvProto2ModifyTaskProto(cmdEnvProto: CommandEnvelopeProto): ModifyTaskProto =
    ModifyTaskProto.parseFrom(cmdEnvProto.getPayload)

  implicit class EnvelopeDescriber(val envelope: RootEnvelopeProto) extends AnyVal {
    def desc(): String = {
      val envelopeDesc = s"Envelope with missive type ${envelope.getMissiveType}."
      val missiveDesc = envelope.getMissiveType match {
        case MissiveType.COMMAND =>
          val commandType = CommandEnvelopeProto.parseFrom(envelope.getPayload).getCommandType
          s" Command: $commandType."
        case _ => s" Unknown missive type ${envelope.getMissiveType}"
      }

      envelopeDesc + missiveDesc
    }
  }

  class RequestUnmarshaller extends FromRequestUnmarshaller[RootEnvelopeProto] {
    override def apply(value: HttpRequest)(implicit ec: ExecutionContext, m: Materializer): Future[RootEnvelopeProto] = {
      PredefinedFromEntityUnmarshallers.byteStringUnmarshaller.apply(value.entity).map(x => RootEnvelopeProto.parseFrom(x.toArray))
    }
  }

  class ResponseUnmarshaller extends FromResponseUnmarshaller[RootEnvelopeProto] {
    override def apply(value: HttpResponse)(implicit ec: ExecutionContext, m: Materializer): Future[RootEnvelopeProto] = {
      PredefinedFromEntityUnmarshallers.byteStringUnmarshaller.apply(value.entity).map(x => RootEnvelopeProto.parseFrom(x.toArray))
    }
  }
}
