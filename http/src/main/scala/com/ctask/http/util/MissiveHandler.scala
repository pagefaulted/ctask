package com.ctask.http.util

import java.time.ZonedDateTime

import akka.http.scaladsl.marshalling.ToResponseMarshallable
import akka.http.scaladsl.model.{ContentTypes, HttpEntity}
import akka.pattern.ask
import com.ctask.http.server.Routes
import com.ctask.http.util.ProtoConverters._
import com.ctask.messages.ServerResponses.{AddedTaskList, Answer, ServerError, SingleList, UpdatedTaskList}
import com.ctask.protocol.util.Response2Proto._
import com.ctask.protocol.commands.Commands.ModifyTaskProto.{NewDescCase, NewDueDateCase, NewTaskNameCase}
import com.ctask.protocol.commands.Commands.{AddTaskListProto, AddTaskToTaskListProto, CommandEnvelopeProto, CommandTypeProto, GetTaskListProto, MarkTaskDoneProto, ModifyTaskProto}
import com.ctask.protocol.data.Data.ErrorProto
import com.ctask.protocol.util.Proto2Response
import com.typesafe.scalalogging.LazyLogging

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future

/**
  * Mixin for missive handling.
  */
trait MissiveHandler extends LazyLogging {
  this: Routes =>

  def handleCommand(commandEnvelopeProto: CommandEnvelopeProto): Future[ToResponseMarshallable] = {
    commandEnvelopeProto.getCommandType match {
      case CommandTypeProto.GET_TASK_LIST => getTaskList(commandEnvelopeProto)
      case CommandTypeProto.MARK_TASK_DONE => markTaskDone(commandEnvelopeProto)
      case CommandTypeProto.ADD_TASK_LIST => addTaskList(commandEnvelopeProto)
      case CommandTypeProto.ADD_TASK_TO_TASKLIST => addTaskToTaskList(commandEnvelopeProto)
      case CommandTypeProto.MODIFY_TASK => modifyTask(commandEnvelopeProto)
      case _ => Future.failed(new UnsupportedOperationException(s"Unsupported command: ${commandEnvelopeProto.getCommandType}"))
    }
  }

  val handleCommandErrors: PartialFunction[Answer, ToResponseMarshallable] = {
    case sError: ServerError =>
      logger.error(s"Server Error received $sError")
      HttpEntity(ContentTypes.`application/octet-stream`, serverError2ErrorProto(sError).toByteArray)
    case unknownServerAnswer =>
      logger.warn(s"Unknown answer received $unknownServerAnswer")
      HttpEntity(ContentTypes.`application/octet-stream`,
        ErrorProto.newBuilder().setMessage(s"Unknown server answer $unknownServerAnswer").build().toByteArray)
  }

  def addTaskList(addTaskListProto: AddTaskListProto): Future[ToResponseMarshallable] = {
    val handler: PartialFunction[Answer, ToResponseMarshallable] = {
      case addedTaskList: AddedTaskList =>
        logger.info(s"Successfully added task list ${addTaskListProto.getName}")
        HttpEntity(ContentTypes.`application/octet-stream`, addedTaskList2TaskListProto(addedTaskList).toByteArray)
    }

    logger.info(s"Handling AddTaskList: task list name ${addTaskListProto.getName}, email ${addTaskListProto.getEmail}")

    val email = if (addTaskListProto.getEmail.isEmpty) None else Option(addTaskListProto.getEmail)

    val addTaskList = (serviceActor ? com.ctask.messages.AddTaskList(addTaskListProto.getName, email)).mapTo[Answer]
    addTaskList.map(handler orElse handleCommandErrors)
  }

  def markTaskDone(markTaskDoneProto: MarkTaskDoneProto): Future[ToResponseMarshallable] = {
    val handler: PartialFunction[Answer, ToResponseMarshallable] = {
      case updatedTaskList: UpdatedTaskList =>
        logger.info(s"Successfully marked task ${markTaskDoneProto.getTaskId} as done " +
          s"in tasklist ${markTaskDoneProto.getTaskListName}")
        HttpEntity(ContentTypes.`application/octet-stream`, updatedList2TaskListProto(updatedTaskList).toByteArray)
    }

    logger.info(s"Handling MarkTaskDone: task list name ${markTaskDoneProto.getTaskListName}, " +
      s"task id: ${markTaskDoneProto.getTaskId}")

    val markDone = (serviceActor ? com.ctask.messages.MarkDone(markTaskDoneProto.getTaskId, markTaskDoneProto.getTaskListName)).mapTo[Answer]
    markDone.map(handler orElse handleCommandErrors)
  }

  def getTaskList(getTaskListProto: GetTaskListProto): Future[ToResponseMarshallable] = {
    val handler: PartialFunction[Answer, ToResponseMarshallable] = {
      case sl: SingleList =>
        logger.info(s"Successfully retrieved tasklist ${getTaskListProto.getTaskListName}")
        HttpEntity(ContentTypes.`application/octet-stream`, singleList2TaskListProto(sl).toByteArray)
    }

    logger.info(s"Handling GetTaskList: task list name ${getTaskListProto.getTaskListName}")
    val taskList = (serviceActor ? com.ctask.messages.GetTaskList(getTaskListProto.getTaskListName, getTaskListProto.getWithCompleted)).mapTo[Answer]
    taskList.map(handler orElse handleCommandErrors)
  }

  def addTaskToTaskList(addTaskToTaskListProto: AddTaskToTaskListProto): Future[ToResponseMarshallable] = {
    val handler: PartialFunction[Answer, ToResponseMarshallable] = {
      case updatedTaskList: UpdatedTaskList =>
        logger.info(s"Successfully updated tasklist ${addTaskToTaskListProto.getName}")
        HttpEntity(ContentTypes.`application/octet-stream`, updatedList2TaskListProto(updatedTaskList).toByteArray)
    }

    logger.info(s"Handling AddTaskToTaskList: task list name ${addTaskToTaskListProto.getTaskListName}")

    val dueDate = if (addTaskToTaskListProto.getDueDate.getIsPresent) Option(ZonedDateTime.parse(addTaskToTaskListProto.getDueDate.getDateTimeStr)) else None

    val updatedList = (serviceActor ? com.ctask.messages.AddTaskToTaskList(
      addTaskToTaskListProto.getName,
      addTaskToTaskListProto.getTaskListName,
      Option(addTaskToTaskListProto.getDescription),
      dueDate,
      Proto2Response.recurrenceTypeToRecurrence(addTaskToTaskListProto.getRecurrence.getRecurrence)))
      .mapTo[Answer]

    updatedList.map(handler orElse handleCommandErrors)
  }

  def modifyTask(modifyTaskProto: ModifyTaskProto): Future[ToResponseMarshallable] = {
    val handler: PartialFunction[Answer, ToResponseMarshallable] = {
      case updatedTaskList: UpdatedTaskList =>
        logger.info(s"Successfully updated tasklist ${modifyTaskProto.getTaskListName}")
        HttpEntity(ContentTypes.`application/octet-stream`, updatedList2TaskListProto(updatedTaskList).toByteArray)
    }

    logger.info(s"Handling ModifyTask: task list name ${modifyTaskProto.getTaskListName}, task id ${modifyTaskProto.getTaskId}")

    val newName = modifyTaskProto.getNewTaskNameCase match {
      case NewTaskNameCase.NULLTASKNAME => None
      case NewTaskNameCase.NAME => Option(modifyTaskProto.getName)
    }

    val newDesc = modifyTaskProto.getNewDescCase match {
      case NewDescCase.NULLDESC => None
      case NewDescCase.DESC => Option(modifyTaskProto.getDesc)
    }

    val newDueDate = modifyTaskProto.getNewDueDateCase match {
      case NewDueDateCase.NULLDUEDATE => None
      case NewDueDateCase.DUEDATE => Option(ZonedDateTime.parse(modifyTaskProto.getDueDate.getDateTimeStr))
    }

    val newRecurrence = Proto2Response.recurrenceTypeToRecurrence(modifyTaskProto.getRecurrence.getRecurrence)

    val updatedList = (serviceActor ? com.ctask.messages.ModifyTask(
      modifyTaskProto.getTaskId,
      modifyTaskProto.getTaskListName,
      newName,
      newDesc,
      newDueDate,
      newRecurrence))
      .mapTo[Answer]

    updatedList.map(handler orElse handleCommandErrors)
  }
}
