package com.ctask.http.util

import com.ctask.utils.PropertiesRetriever
import com.typesafe.config.{Config, ConfigFactory}

object HttpProperties extends PropertiesRetriever {
  override def config: Config = ConfigFactory.load()

  /**
    * Service address.
    */
  val serviceAddressProp: String = "com.ctask.http.serviceAddress"
  val serviceAddressDefault: String = "127.0.0.1"
  def serviceAddress: String = getPropertyStr(serviceAddressProp, serviceAddressDefault)

  /**
    * Service port.
    */
  val servicePortProp: String = "com.ctask.http.servicePort"
  val servicePortDefault: Int = 1234
  def servicePort: Int = getPropertyInt(servicePortProp, servicePortDefault)
}
