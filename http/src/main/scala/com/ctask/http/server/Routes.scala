package com.ctask.http.server

import akka.actor.ActorRef
import akka.http.scaladsl.model.{ContentTypes, HttpEntity}
import akka.http.scaladsl.server.Directives._
import akka.http.scaladsl.server.Route
import akka.util.Timeout
import com.ctask.http.util.MissiveHandler
import com.ctask.http.util.ProtoConverters._
import com.ctask.protocol.Envelope.RootEnvelopeProto
import com.ctask.protocol.data.Data.ErrorProto
import com.typesafe.scalalogging.LazyLogging

import scala.concurrent.duration._
import scala.util.{Failure, Success}

trait Routes extends LazyLogging with MissiveHandler {
  val serviceActor: ActorRef

  implicit val timeout: Timeout = Timeout(10 seconds)

  val taskListsRoute: Route =
      pathPrefix("command") {
        post {
          pathEnd {
            entity(as[RootEnvelopeProto]) { rootEnvelope =>
              rootEnvelope.getMissiveType match {
                case RootEnvelopeProto.MissiveType.COMMAND =>
                  onComplete(handleCommand(rootEnvelope)) {
                    case Success(entity) =>
                      logger.info(s"Successfully handled command ${rootEnvelope.desc()}")
                      complete(entity)
                    case Failure(exception) =>
                      logger.error(s"Failed to handle command ${rootEnvelope.desc()}", exception)
                      // TODO: use response proto
                      val errorResponse = HttpEntity(ContentTypes.`application/octet-stream`,
                        ErrorProto.newBuilder().setMessage(s"Command processing failed with exception $exception").
                          build().
                          toByteArray)

                      complete(errorResponse)
                  }
                case unknownMissiveType =>
                  val errorResponse = HttpEntity(ContentTypes.`application/octet-stream`,
                    // TODO: use reponse proto
                    ErrorProto.newBuilder().setMessage(s"Unknown missive type $unknownMissiveType").
                    build().
                    toByteArray)

                  complete(errorResponse)
              }
            }
          }
        }
      }
}
