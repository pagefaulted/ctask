package com.ctask.http.server

import akka.actor.ActorSystem
import akka.http.scaladsl.Http
import akka.stream.Materializer
import com.ctask.http.util.HttpProperties

import scala.concurrent.ExecutionContext.Implicits.global

trait HttpService extends Routes {

  implicit val httpSystem: ActorSystem

  def startHttpService(): Unit = {
    implicit val materializer: Materializer = Materializer.createMaterializer(httpSystem)
    val bindingFuture = Http().bindAndHandle(taskListsRoute, HttpProperties.serviceAddress, HttpProperties.servicePort)

    logger.info("Ctask-Http is online...\n" +
      s"Listening on ${HttpProperties.serviceAddress}:${HttpProperties.servicePort} ")

    sys.addShutdownHook {
      bindingFuture.
        map(_.unbind()).
        onComplete(_ => httpSystem.terminate())
    }
  }
}
