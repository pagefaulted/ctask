package com.ctask.http.server

import akka.actor.{Actor, ActorRef, Props}
import akka.http.scaladsl.model.{ContentTypes, HttpEntity, HttpMethods, HttpRequest, HttpResponse}
import akka.http.scaladsl.testkit.ScalatestRouteTest
import com.ctask.data.TaskList
import com.ctask.messages.{Command, GetTaskList}
import com.ctask.protocol.Envelope.RootEnvelopeProto
import com.ctask.protocol.data.Data.TaskListProto
import com.ctask.protocol.response.Response.ResponseProto
import com.ctask.http.util.ProtoConverters.responseUnmarshaller
import com.ctask.messages.ServerResponses.SingleList
import com.ctask.protocol.util.RequestEnvelopeUtils
import org.scalatest.{FlatSpec, Matchers}

class RoutesSpec
  extends FlatSpec
    with Routes
    with Matchers
    with ScalatestRouteTest {

  val serviceActor: ActorRef = system.actorOf(TestActor.props(GetTaskList("TODOs", withCompleted = true)))

  "The route" should "unmarshal" in {
    val commandEnv = RequestEnvelopeUtils.createGetTaskListCommandEnvelope("TODOs", true)
    val getTaskListRequest = HttpRequest(
      HttpMethods.POST,
      uri = "/command",
      entity = HttpEntity(ContentTypes.`application/octet-stream`, commandEnv.toByteArray))

    getTaskListRequest ~> taskListsRoute ~> check {
      handled shouldBe true
      val envelope = responseAs[RootEnvelopeProto]
      val response = ResponseProto.parseFrom(envelope.getPayload)
      TaskListProto.parseFrom(response.getPayload).getName == "TODOs"
    }
  }
}

object TestActor {
  def props(expectedCommand: Command): Props = Props(new TestActor(expectedCommand))
}

class TestActor(val expectedCommand: Command) extends Actor with Matchers {
  def receive: PartialFunction[Any, Unit] = {
    case actual if actual == expectedCommand => sender() ! new SingleList(new TaskList("TODOs", Array.empty, None.orNull))
    case unexpected => sender() ! new RuntimeException("error")
  }
}
