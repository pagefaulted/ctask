import scala.reflect.io.Directory

lazy val root = project.in(file(".")).aggregate(client, server, common, protocol, http, protoutils, http_client, java_common)

val scalaV = "2.13.1"
val akkaVersion = "2.6.0"
val scalatestVersion = "3.0.8"
val playVersion = "2.7.4"
val akkaHttpVersion = "10.1.11"
val akkaStreamVersion = "2.5.21"

lazy val scalacSettings = Seq(
  scalaVersion := s"$scalaV",
  scalacOptions += "-deprecation",
  scalacOptions += "-feature",
  scalacOptions += "-language:postfixOps",
)

lazy val javaSettings = Seq(
  // Remove scalalib
  autoScalaLibrary := false,
  // Remove cross-scala version from artifact name
  crossPaths := false,
)

lazy val commonSettings = scalacSettings ++ Seq(
  // scalatest
  libraryDependencies += "org.scalactic" %% "scalactic" % s"$scalatestVersion",
  libraryDependencies += "org.scalatest" %% "scalatest" % s"$scalatestVersion" % "test",

  // akka
  libraryDependencies += "com.typesafe.akka" %% "akka-actor" % s"$akkaVersion",
  libraryDependencies += "com.typesafe.akka" %% "akka-remote" % s"$akkaVersion",
  libraryDependencies += "com.typesafe.akka" %% "akka-testkit" % s"$akkaVersion",

  // play
  libraryDependencies += "com.typesafe.play" %% "play-json" % playVersion,
  git.useGitDescribe := true,
  testOptions in Test += Tests.Argument("-oDF"),

  // logger
  libraryDependencies += "ch.qos.logback" % "logback-classic" % "1.2.3",
  libraryDependencies += "com.typesafe.scala-logging" %% "scala-logging" % "3.9.2",

  licenses  += ("GPL-3.0", url("https://www.gnu.org/licenses/gpl-3.0.en.html"))
)

// Limit the test execution to a single thread. This has to do with remote actor testing. The port provider is shared
// between modules, hence it might assign the same port number to client and server actor systems running in parallel,
// which would cause test failure. If test exec time becomes an issue, move the PortProvider in the modules and change
// the port range.
concurrentRestrictions in Global := Seq(
  Tags.limit(Tags.Test, 1)
)

lazy val common = (project in file("common")).
  settings(
    commonSettings,
    coverageEnabled := false,
    name := "ctask-common"
  )
  .dependsOn(java_common)

lazy val client = (project in file("client")).
  settings(
    commonSettings,
    name := "ctask-client",
    mainClass in Compile := Some("com.ctask.client.ClientMain"),
    mappings in Universal ++= {
      ((resourceDirectory in Compile).value * "*").get.map { f =>
        f -> s"conf/${f.name}"
      }
    },
    mappings in (Compile, packageBin) ~= {_.filterNot { case (_, name) =>
        name.endsWith(".conf")
    }},
    scriptClasspath += "../conf/",
    libraryDependencies += "com.typesafe.akka" %% "akka-http" % s"$akkaHttpVersion",
  )
  .dependsOn(common, http_client)
  .enablePlugins(GitVersioning, JavaAppPackaging)

lazy val server = (project in file("server")).
  settings(
    commonSettings,
    name := "ctask-server",
    mainClass in Compile := Some("com.ctask.server.CtaskServer"),
    mappings in Universal ++= {
      ((resourceDirectory in Compile).value * "*").get.map { f =>
        f -> s"conf/${f.name}"
      }
    },
    mappings in (Compile, packageBin) ~= {_.filterNot { case (_, name) =>
        name.endsWith(".conf")
    }},
    scriptClasspath += "../conf/",
    libraryDependencies += "javax.mail" % "mail" % "1.4.7"
  )
  .dependsOn(common, http, java_common)
  .enablePlugins(GitVersioning, JavaAppPackaging)

lazy val http = (project in file("http")).
  settings(
    commonSettings,
    name := "ctask-http",
    mainClass in Compile := Some("com.ctask.http.CtaskHttp"),

    // akka http
    libraryDependencies += "com.typesafe.akka" %% "akka-http" % s"$akkaHttpVersion",
    libraryDependencies += "com.typesafe.akka" %% "akka-http-testkit" % s"$akkaHttpVersion",
    libraryDependencies += "com.typesafe.akka" %% "akka-stream" % s"$akkaStreamVersion",

    mappings in Universal ++= {
      ((resourceDirectory in Compile).value * "*").get.map { f =>
        f -> s"conf/${f.name}"
      }
    },
    mappings in (Compile, packageBin) ~= {_.filterNot { case (_, name) =>
        name.endsWith(".conf")
    }},
    scriptClasspath += "../conf/"
  )
  .dependsOn(common, protoutils)
  .enablePlugins(GitVersioning, JavaAppPackaging)

lazy val http_client = (project in file("http-client")).
  settings(
    commonSettings,
    name := "ctask-http-client",

    // akka http
    libraryDependencies += "com.typesafe.akka" %% "akka-http" % s"$akkaHttpVersion",
    libraryDependencies += "com.typesafe.akka" %% "akka-http-testkit" % s"$akkaHttpVersion",
    libraryDependencies += "com.typesafe.akka" %% "akka-stream" % s"$akkaStreamVersion"
  )
  .dependsOn(common, protoutils, java_common)
  .enablePlugins(GitVersioning, JavaAppPackaging)

lazy val protocol = (project in file("protocol")).
  settings(
    javaSettings,
    coverageEnabled := false,
    name := "ctask-protocol",
    version in ProtobufConfig := "3.10.0",
    cleanFiles := {
        new Directory(new File(baseDirectory.value.absolutePath + "/target")).deleteRecursively()
        Seq.empty
      },

      javaSource in ProtobufConfig := ((sourceDirectory in Compile).value / "java")

    ).enablePlugins(GitVersioning, ProtobufPlugin)

lazy val protoutils = (project in file("protoutils")).
  settings(
    javaSettings,
    coverageEnabled := false,
    name := "ctask-protoutils",
  )
  .dependsOn(protocol, java_common)
  .enablePlugins(GitVersioning, JavaAppPackaging)

lazy val java_common = (project in file("java-common")).
  settings(
    javaSettings,
    coverageEnabled := false,
    name := "ctask-java-common"
  ).enablePlugins(GitVersioning)

addCommandAlias("buildall", ";clean ; coverage ; test ; coverageReport ; coverageAggregate")
addCommandAlias("build", ";clean ; test")
addCommandAlias("pkg", ";clean ; universal:packageZipTarball")
addCommandAlias("proto", ";clean ; protobufGenerate")
