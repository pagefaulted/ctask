package com.ctask.util;

import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.Optional;

public class DateUtils {

    public static Optional<ZonedDateTime> parseDateString(final String str) {
        final String splitted[] = str.split(" ");
        if (splitted.length != 2) {
            return Optional.empty();
        } else {
            final String time = splitted[0];
            final String date = splitted[1];

            final String timeSplit[] = time.split(":");
            if (timeSplit.length != 2) {
                return Optional.empty();
            } else {
                final String hour = timeSplit[0];
                final String minutes = timeSplit[1];

                final String dateSplit[] = date.split("/");
                if (dateSplit.length != 3) {
                    return Optional.empty();
                } else {
                    final String day = dateSplit[0];
                    final String month = dateSplit[1];
                    final String year = dateSplit[2];
                    return Optional.of(ZonedDateTime.of(
                            Integer.parseInt(year),
                            Integer.parseInt(month),
                            Integer.parseInt(day),
                            Integer.parseInt(hour),
                            Integer.parseInt(minutes),
                            0,
                            0,
                            ZoneId.systemDefault()));
                }
            }
        }
    }
}
