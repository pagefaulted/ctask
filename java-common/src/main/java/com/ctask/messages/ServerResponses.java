package com.ctask.messages;

import com.ctask.data.Task;
import com.ctask.data.TaskList;

import java.util.List;

public class ServerResponses {
    // Ctask server responses
    public static abstract class Answer {}

    public static class TaskLists extends Answer {
        public final List<TaskList> taskLists;

        public TaskLists(final List<TaskList> taskLists) {
            this.taskLists = taskLists;
        }

        @Override
        public boolean equals(Object that) {
            return that instanceof TaskLists
                    && this.taskLists.equals(((TaskLists) that).taskLists);
        }

        @Override
        public int hashCode() {
            return taskLists.hashCode();
        }
    }

    public static class SingleList extends Answer {
        public final TaskList list;

        public SingleList(final TaskList list) {
            this.list = list;
        }

        @Override
        public boolean equals(Object that) {
            return that instanceof SingleList
                    && this.list.equals(((SingleList) that).list);
        }

        @Override
        public int hashCode() {
            return list.hashCode();
        }
    }

    public static class AddedTaskList extends Answer {
        public final TaskList list;

        public AddedTaskList(final TaskList list) {
            this.list = list;
        }

        @Override
        public boolean equals(Object that) {
            return that instanceof AddedTaskList
                    && this.list.equals(((AddedTaskList) that).list);
        }

        @Override
        public int hashCode() {
            return this.list.hashCode();
        }
    }

    public static class RemovedTaskList extends Answer {
        public final TaskList list;

        public RemovedTaskList(final TaskList list) {
            this.list = list;
        }

        @Override
        public boolean equals(Object that) {
            return that instanceof RemovedTaskList
                    && this.list.equals(((RemovedTaskList) that).list);
        }

        @Override
        public int hashCode() {
            return list.hashCode();
        }
    }

    public static class UpdatedTaskList extends Answer {
        public final TaskList list;

        public UpdatedTaskList(final TaskList list) {
            this.list = list;
        }

        @Override
        public boolean equals(Object that) {
            return that instanceof UpdatedTaskList
                    && this.list.equals(((UpdatedTaskList) that).list);
        }

        @Override
        public int hashCode() {
            return this.list.hashCode();
        }
    }

    public static class RemovedTask extends Answer {
        public final Task removedTask;

        public RemovedTask(final Task removedTask) {
            this.removedTask = removedTask;
        }

        @Override
        public boolean equals(Object that) {
            return that instanceof RemovedTask
                    && this.removedTask.equals(((RemovedTask) that).removedTask);
        }

        @Override
        public int hashCode() {
            return this.removedTask.hashCode();
        }
    }

    public static class MarkedReminderSent extends Answer {
        public final Task markedTask;
        public final String taskListName;

        public MarkedReminderSent(final Task markedTask, final String taskListName) {
            this.markedTask = markedTask;
            this.taskListName = taskListName;
        }

        @Override
        public boolean equals(Object that) {
            return that instanceof MarkedReminderSent
                    && this.markedTask.equals(((MarkedReminderSent) that).markedTask)
                    && this.taskListName.equals(((MarkedReminderSent) that).taskListName);
        }

        @Override
        public int hashCode() {
            return this.markedTask.hashCode() +
                    this.taskListName.hashCode();
        }
    }

    public static class ServerError extends Answer {
        public final String msg;

        public ServerError(final String msg) {
            this.msg = msg;
        }

        @Override
        public boolean equals(Object that) {
            return that instanceof ServerError
                    && this.msg.equals(((ServerError) that).msg);
        }

        @Override
        public int hashCode() {
            return this.msg.hashCode();
        }
    }
    public static class Success extends Answer {}
}

