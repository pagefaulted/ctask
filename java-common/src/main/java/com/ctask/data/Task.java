package com.ctask.data;

import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;

public class Task implements Comparable<Task> {
    public final String name;
    public final String description;
    public final long id;
    public final ZonedDateTime dueDate;
    public final boolean done;
    public final Recurrence recurrence;
    public final boolean reminderSent;

    public Task(final String name,
                final String description,
                final long id,
                final ZonedDateTime dueDate,
                final boolean done,
                final Recurrence recurrence,
                final boolean reminderSent) {

        this.name = name;
        this.description = description;
        this.id = id;
        this.dueDate = dueDate;
        this.done = done;
        this.recurrence = recurrence;
        this.reminderSent = reminderSent;
    }

    @Override
    public String toString() {
        final String desc = description.isEmpty() ? "" : " - " + description;
        final DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("HH:mm dd/MMM/yyyy");
        final String dueDateStr = dueDate == null ? "" : " - Due: " + dueDate.format(dateTimeFormatter);
        final String recurrenceStr = recurrence == Recurrence.NEVER ? "" : " - Repeats: " + recurrence.name();

        return id + ": " + name + desc + dueDateStr + recurrenceStr;
    }

    @Override
    public int compareTo(final Task that) {
        if (dueDate == null && that.dueDate == null) {
            // compare by id
            return (int) (id - that.id);
        }
        else if (that.dueDate == dueDate) {
            return 0;
        }
        else if (dueDate == null) {
            return 1;
        }
        else if (that.dueDate == null) {
            return -1;
        }
        else {
            return (int) (dueDate.toEpochSecond() - that.dueDate.toEpochSecond());
        }
    }

    @Override
    public int hashCode() {
        return this.name.hashCode()
                + this.description.hashCode()
                + Long.hashCode(this.id)
                + (this.dueDate == null ? 0 : this.dueDate.hashCode())
                + this.recurrence.hashCode()
                + Boolean.hashCode(this.done)
                + Boolean.hashCode(this.reminderSent);
    }

    @Override
    public boolean equals(final Object other) {
        return other instanceof Task
                && this.name.equals(((Task) other).name)
                && this.description.equals(((Task) other).description)
                && this.id == ((Task) other).id
                && (
                        (this.dueDate == null && ((Task) other).dueDate == null)
                                || (this.dueDate != null && ((Task) other).dueDate != null && this.dueDate.equals(((Task) other).dueDate)))
                && this.recurrence.equals(((Task) other).recurrence)
                && this.done == ((Task) other).done
                && this.reminderSent == ((Task) other).reminderSent;
    }

    public enum Recurrence {
        DAILY("daily") {
            @Override
            public ZonedDateTime applyRecurrenceToDate(final ZonedDateTime date) {
                return date.plusDays(1);
            }
        },
        WEEKLY("weekly") {
            @Override
            public ZonedDateTime applyRecurrenceToDate(final ZonedDateTime date) {
                return date.plusWeeks(1);
            }
        },
        MONTHLY("monthly") {
            @Override
            public ZonedDateTime applyRecurrenceToDate(final ZonedDateTime date) {
                return date.plusMonths(1);
            }
        },
        YEARLY("yearly") {
            @Override
            public ZonedDateTime applyRecurrenceToDate(final ZonedDateTime date) {
                return date.plusYears(1);
            }
        },
        NEVER("never") {
            @Override
            public ZonedDateTime applyRecurrenceToDate(final ZonedDateTime date) {
                return date;
            }
        };

        public final String id;

        Recurrence(final String id) {
            this.id = id;
        }

        public abstract ZonedDateTime applyRecurrenceToDate(final ZonedDateTime date);

        public static Recurrence getRecurrence(final String name) {
            return Recurrence.valueOf(name.toUpperCase());
        }
    }
}
