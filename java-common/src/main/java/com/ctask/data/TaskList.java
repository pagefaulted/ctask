package com.ctask.data;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Objects;

public class TaskList {
    public final String name;
    public final Task[] tasks;
    public final String email;

    public TaskList(final String name, final Task[] tasks, final String email) {
        this.name = name.trim();
        this.tasks = tasks;
        this.email = email;
    }

    @Override
    public boolean equals(final Object that) {
        final boolean tmp =
                that instanceof TaskList
                && Objects.equals(this.name, ((TaskList) that).name)
                && Objects.equals(this.email, ((TaskList) that).email);

        if (tmp) {
            final List<Task> thisTasks = Arrays.asList(this.tasks);
            final List<Task> thatTasks = Arrays.asList(((TaskList) that).tasks);
            Collections.sort(thisTasks);
            Collections.sort(thatTasks);

            return Objects.equals(thisTasks, thatTasks);
        } else {
            return tmp;
        }
    }

    @Override
    public int hashCode() {
        return this.name.hashCode()
                + Arrays.hashCode(this.tasks)
                + (this.email == null ? 0 : this.email.hashCode());
    }

    @Override
    public String toString() {
        final String emailStr = this.email == null ? "" : " - " + this.email;

        return name + " - Tasks count: " + this.tasks.length + " - Completed: "
                + Arrays.stream(this.tasks).filter(t -> t.done).count();
    }
}
