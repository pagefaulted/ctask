package com.ctask.data

import com.ctask.utils.UniqueId
import play.api.libs.json.{JsValue, Json, Reads, Writes}
import play.api.libs.json._
import play.api.libs.functional.syntax._

object TaskListJsonUtils {
  val idGenerator = new UniqueId(1)

  // Json stuff
  implicit val taskListJsonRead: Reads[TaskList] = (
    (__ \ "nameStr").read[String] and
      (__ \ "tasks").read(Reads.seq[Task](TaskJsonUtils.reads)) and
      (__ \ "email").readNullable[String]
  )((name, tasks, email) => new TaskList(name, tasks.toArray, email.orNull))

  implicit val taskListWrites: Writes[TaskList] = (taskList: TaskList) => Json.obj(
    "nameStr" -> taskList.name,
    "tasks" -> taskList.tasks.map(TaskJsonUtils.taskWrites.writes),
    "email" -> taskList.email
  )
}
