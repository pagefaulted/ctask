package com.ctask.data

import java.time.ZonedDateTime

import play.api.libs.json._
import play.api.libs.functional.syntax._

object TaskJsonUtils {

  implicit object TaskOrdering extends Ordering[Task] {
    override def compare(x: Task, y: Task): Int = x.compareTo(y)
  }

  // Json stuff
  implicit val reads: Reads[Task] = (
    (__ \ "name").read[String] and
      (__ \ "description").read[String] and
      (__ \ "id").read[Long] and
      (__ \ "dueDate").read[String].map {
        {
          case "none" =>
            None.orNull
          case dueDate =>
            ZonedDateTime.parse(dueDate)
        }
      } and
      (__ \ "done").read[Boolean] and
      (__ \ "recurrence").read[String].map(Task.Recurrence.getRecurrence) and
      (__ \ "reminderSent").read[Boolean]
    )(
    (name, description, id, dueDate, done, recurrence, reminderSent) =>
      new Task(name, description, id, dueDate, done, recurrence, reminderSent))

  implicit val taskWrites: Writes[Task] = (task) => Json.obj(
    "name" -> task.name,
    "description" -> task.description,
    "id" -> task.id,
    "dueDate" -> { if(task.dueDate != null) task.dueDate else "none" },
    "done" -> task.done,
    "recurrence" -> task.recurrence.id,
    "reminderSent" -> task.reminderSent
  )
}
