package com.ctask.storage

/**
  * Generic storage exception.
  */
case class StorageException(message: String) extends java.lang.Exception(message)
