package com.ctask.utils

import java.time.{ZoneId, ZonedDateTime}

import com.ctask.util.DateUtils

import scala.util.{Failure, Try}

/**
  * Argument parsing utils.
  */
object ArgsUtils {

  val DEFAULT_ARG_SEPARATOR = "-"

  /**
    * Parses the provided args using the provided args separator.
    *
    * @param args      the args to parse.
    * @param separator the args separator, "-" by default.
    * @return a map from arg to value.
    */
  def parse(args: Array[String], separator: String = DEFAULT_ARG_SEPARATOR): Map[String, String] = {
    val trimmedArgs = args.map(_.trim).filter(_.nonEmpty)
    if (trimmedArgs.length == 0 || !trimmedArgs(0).startsWith(separator)) {
      Map.empty
    }
    else {
      var switch = ""
      trimmedArgs.foldLeft(Map.empty[String, String]) { case (acc, elem) =>
        if (elem.startsWith(separator)) {
          switch = elem
          acc + (switch -> acc.getOrElse(switch, ""))
        }
        else {
          val value = acc(switch)
          val newValue = if(value.isEmpty) elem else value + " " + elem
          acc + (switch -> newValue)
        }
      }
    }
  }

  /**
    * Given a command line, returns the arguments provided to the command.
    *
    * @param cmdLine   the command line to extract the args from
    * @param separator the command switch string, "-" by default
    * @return a map from arg switch to arg value
    */
  def extractArgsFromCommandLine(cmdLine: String, separator: String = DEFAULT_ARG_SEPARATOR): Map[String, String] = {
    val args = {
      val indexOfFirstSwitch = cmdLine.indexOf(separator)
      if (indexOfFirstSwitch != -1) {
        cmdLine.substring(indexOfFirstSwitch, cmdLine.length)
      }
      else {
        ""
      }
    }

    ArgsUtils.parse(args.split(" "))
  }

  /**
    * Base trait for text based dates.
    */
  sealed trait TextDate {
    def text: String
    def hourOfTheDay: Int

    /**
      * Create a ZonedDateTime corresponding to the relative TextDate.
      *
      * @param baseDate the base date used to create the text date.
      * @return the ZonedDateTime corresponding to the relative TextDate
      */
    def extractDate(baseDate: ZonedDateTime): ZonedDateTime = {
      if(baseDate.getHour >= hourOfTheDay) {
        baseDate.withHour(hourOfTheDay).withMinute(0).plusDays(1)
      } else {
        baseDate.withHour(hourOfTheDay).withMinute(0)
      }
    }
  }

  /**
    * Morning is the first 10 o'clock from now.
    */
  case object MORNING extends TextDate {
    override val text = "morning"
    override val hourOfTheDay = 10
  }

  /**
    * Afternoon is the first 14 o'clock from now.
    */
  case object AFTERNOON extends TextDate {
    override val text = "afternoon"
    override val hourOfTheDay = 14
  }

  /**
    * Evening is the first 18 o'clock from now.
    */
  case object EVENING extends TextDate {
    override val text = "evening"
    override val hourOfTheDay = 18
  }

  /**
    * Tomorrow is next day's 10 o'clock.
    */
  case object TOMORROW extends TextDate {
    override val text = "tomorrow"
    override val hourOfTheDay = 10

    override def extractDate(baseDate: ZonedDateTime): ZonedDateTime =
      baseDate.withHour(hourOfTheDay).withMinute(0).plusDays(1)
  }

  /**
    * Now is now.
    */
  case object NOW extends TextDate {
    override val text = "now"
    override val hourOfTheDay: Int = -1

    override def extractDate(baseDate: ZonedDateTime): ZonedDateTime = baseDate
  }

  case object NODATE {
    val text = "none"
  }

  /**
    * Transform the provided string to a ZoneDateTime object.
    * The date time string provided is expected to be either a text following TextDate formats (e.g. now, tomorrow etc)
    * or a date string in 24h format, as follows: 11:45 8/10/2017.
    *
    * @param str      the date time string, either in TextDate formats or in the following format: 11:45 8/10/2017
    * @param baseTime the base time used for TextDate extraction. This is ignored if the provided string is not a
    *                 TextDate.
    * @return the Try containing the ZoneDateTime instance representing the provided string
    */
  def extractDateTimeFromString(str: String, baseTime: ZonedDateTime = ZonedDateTime.now): Try[Option[ZonedDateTime]] = {

    def getTextDate(str: String): Try[Option[ZonedDateTime]] = {
      str match {
        case MORNING.text => Try(Some(MORNING.extractDate(baseTime)))
        case AFTERNOON.text => Try(Some(AFTERNOON.extractDate(baseTime)))
        case EVENING.text => Try(Some(EVENING.extractDate(baseTime)))
        case TOMORROW.text => Try(Some(TOMORROW.extractDate(baseTime)))
        case NOW.text => Try(Some(NOW.extractDate(baseTime)))
        case NODATE.text => Try(None)
        case _ => Failure(new IllegalArgumentException("Could not recognize text date."))
      }
    }

    def parseDateString(str: String): Try[Option[ZonedDateTime]] = {
      val errorMsgString = s"The provided due date $str is not in the expected format. Format should be hh:mm dd/mm/yyyy"

      val res = DateUtils.parseDateString(str)
      if (res.isPresent) {
        Try(Option(res.get()))
      } else {
        Failure(new IllegalArgumentException(errorMsgString))
      }
    }

    getTextDate(str) orElse parseDateString(str)
  }
}
