package com.ctask.utils

object PortProvider {

  private val portRange = List.range(3230, 3330)

  private var availablePorts: List[Int] = portRange

  /**
    * Provides a port number that will not be reuses within this test session.
    * @return a port number.
    */
  def unique: Int = availablePorts.synchronized {
    val port = availablePorts.head
    availablePorts = availablePorts.tail
    port
  }

  /**
    * Returns all the ports that this port provider can return.
    *
    * @return the list of ports that this port provider will return.
    */
  def getPortRange: List[Int] = portRange
}
