package com.ctask.utils

import com.typesafe.config.Config

import scala.util.{Failure, Success, Try}

/**
  * Mixin trait to handle properties both from configuration and command line. System properties take precedence
  * on the config object.
  */
trait PropertiesRetriever {

  /**
    * Returns the configuration to be read.
    *
    * @return the configuration
    */
  def config: Config

  /**
    * Gets the value of the provided string property, from the system props or the config, in that order.
    * If no prop is found, the default value is returned.
    * @param prop the property
    * @param default the default value
    * @return the string value for the provided property
    */
  def getPropertyStr(prop: String, default: String): String = {
    doGetProperty(prop) match {
      case Success(propVal) => if(propVal.nonEmpty) propVal else default
      case Failure(_) => default
    }
  }

  /**
    * Gets the value of the provided int property, from the system props or the config, in that order.
    * If no prop is found, the default value is returned.
    * @param prop the property
    * @param default the default value
    * @return the int value for the provided property
    */
  def getPropertyInt(prop: String, default: Int): Int = {
    doGetProperty(prop) match {
      case Success(propVal) => Try(propVal.toInt).getOrElse(default)
      case Failure(_) => default
    }
  }

  private def doGetProperty(propStr: String): Try[String] =
    Try(Option(System.getProperty(propStr)).getOrElse(config.getString(propStr)))
}
