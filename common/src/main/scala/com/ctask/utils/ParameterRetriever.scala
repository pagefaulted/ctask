package com.ctask.utils

import com.typesafe.config.Config

import scala.util.{Failure, Success, Try}

/**
  * Represents a parameter, with its switch and configuration key.
  *
  * @param switch      the parameter's command line switch.
  * @param configKey   the parameter's config file key.
  */
case class Parameter(switch: String, configKey: String)

/**
  * Base class for parameter extraction.
  * The parameter in the args has higher priority than the one in the config.
  *
  * @param args   the args
  * @param config the config
  */
abstract class ParameterRetriever(args: Array[String], config: Config) {

  private val parsedArgs = ArgsUtils.parse(args)

  /**
    * Gets the provided parameter from the command line or from the config, in that order.
    *
    * @param param the parameter to be retrieved.
    * @return an Option containing the parameter value, or None if the parameter is not present
    *         in the command line nor in the config.
    */
  protected[utils] def getParameter(param: Parameter): Option[String] = {
    Try {
      parsedArgs.getOrElse(param.switch, config.getString(param.configKey))
    } match {
        case Success(paramVal) => Some(paramVal)
        case Failure(_) => None
    }
  }
}
