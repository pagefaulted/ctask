package com.ctask.utils

import java.util.concurrent.atomic.AtomicLong

/**
  * Generates unique ids, starting from the provided initial value, 1 by default.
  *
  * @param initialValue the initial value the ids start from, 1 by default.
  */
class UniqueId(initialValue: Long = 1) {

  private val currentId = new AtomicLong(initialValue)

  /**
    * Gets the next available unique id.
    * @return the next available unique id.
    */
  def getUniqueId: Long = currentId.getAndIncrement()

  /**
    * Resets the current id to the provided value, 1 by default.
    *
    * @param value the value to reset the unique id to, 1 by default.
    */
  def reset(value: Long = 1): Unit = while(!currentId.compareAndSet(currentId.get(), value)) {}
}

/**
  * Static companion object that generates unique ids, starting from 1.
  */
object UniqueId {

  private val uniqueId = new UniqueId

  /**
    * Gets the next available unique id.
    * @return the next available unique id.
    */
  def getUniqueId: Long = uniqueId.getUniqueId

  /**
    * Resets the current id to the provided value, 1 by default.
    *
    * @param value the value to reset the unique id to, 1 by default.
    */
  def reset(value: Long = 1): Unit = uniqueId.reset(value)
}
