package com.ctask.utils

import java.io.File

import scala.util.{Failure, Try}

/**
  * File system related utility functions.
  * // TODO: add logging, #79
  */
object FileUtils {

  /**
    * Creates a new dir with the provided pull path.
    * @param fullDirPath the full path of the dir to create.
    * @return a Try containing the create dir file or a Failure with the exception.
    */
  def mkdir(fullDirPath: String): Try[File] = {
    //println(s"INFO: creating folder $fullDirPath")
    val folderFile = new File(fullDirPath)

    folderFile.mkdir() match {
      case true => Try(folderFile)
      case false => Failure(
        new RuntimeException(s"Could not create folder ${folderFile.getCanonicalPath}."))
    }
  }

  /**
    * Removed the dir a the provided path. If the dir is not empty, it will remove all the files.
    * inside it. It will not remove non-empty subdirs.
    * @param fullDirPath the full path to the dir to remove.
    * @return the list of removed files.
    */
  def rmDir(fullDirPath: String): List[String] = {
    //println(s"INFO: removing dir $fullDirPath")
    val removedFiles = rmInDir(fullDirPath)
    new File(fullDirPath).delete() match {
      case true => fullDirPath :: removedFiles
      case false =>
        println(s"WARN: could not remove folder $fullDirPath")
        removedFiles
    }
  }

  /**
    * Removes all the files and empty folders in the provided dir.
    * @param fullDirPath the folder to empty.
    * @return a list containing the full paths of the removed files and folders.
    */
  def rmInDir(fullDirPath: String): List[String] = {
    //println(s"INFO: removing files in dir $fullDirPath")
    val dirFile = new File(fullDirPath)

    def rmInDirImpl(dirFile: File) = {
      dirFile.list.collect {
        case fileName if new File(dirFile, fileName).delete() => s"$fullDirPath/$fileName"
      }.toList
    }

    dirFile.isDirectory match {
      case true => rmInDirImpl(dirFile)
      case false =>
        println(s"WARN: $fullDirPath is not a directory.")
        Nil
    }
  }
}
