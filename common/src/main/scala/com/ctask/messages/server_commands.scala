package com.ctask.messages

import java.time.ZonedDateTime

import com.ctask.data.Task.Recurrence
import com.ctask.utils.ArgsUtils

import scala.util.{Failure, Success, Try}

/**
  * Common trait for all commands' factory objects.
  */
sealed trait CommandFactory {

  /**
    * The command string associated with the specific command.
    */
  val commandString: String
}

object AddTaskListFactory extends CommandFactory {

  override val commandString = "add tasklist"

  def apply(switchesValues: Map[String, String]): Command = {
    val taskListEmailOpt = switchesValues.get("-e")
    switchesValues.get("-n") match {
      case Some(listName) =>
        AddTaskList(listName, taskListEmailOpt)
      case None => InvalidCommand(s"The $commandString command was missing the mandatory -n switch.")
    }
  }
}

object RemoveTaskListFactory extends CommandFactory {

  override val commandString = "rm tasklist"

  def apply(switchesValues: Map[String, String]): Command = {
    switchesValues.get("-n") match {
      case Some(listName) => RemoveTaskList(listName)
      case None => InvalidCommand(s"The $commandString command was missing the mandatory -n switch.")
    }
  }
}

object ShowTaskListsFactory extends CommandFactory {
  override val commandString = "show tasklists"
}

object AddTaskToTaskListFactory extends CommandFactory {

  override val commandString = "add"

  // TODO: the consumer here should only get a command containing a request
  //scalastyle:off
  def apply(switchValues: Map[String, String], selectedTaskList: Option[String]): Command = {
    val taskNameOpt = switchValues.get("-n")
    val taskDescriptionOpt = switchValues.get("-d")
    val taskDueDateOpt = switchValues.get("-dd")
    val taskRecurrence = Try(Recurrence.getRecurrence(switchValues.getOrElse("-r", Recurrence.NEVER.name)))

    taskNameOpt match {
      case Some(taskName) =>
        selectedTaskList match {
          case Some(taskListName) =>
            taskRecurrence match {
              case Success(recurrence) =>
                taskDueDateOpt match {
                  case Some(taskDueDateStr) =>
                    ArgsUtils.extractDateTimeFromString(taskDueDateStr) match {
                      case Success(zonedDateTime) =>
                        AddTaskToTaskList(taskName, taskListName, taskDescriptionOpt, zonedDateTime, recurrence)
                      case Failure(ex) =>
                        InvalidCommand(s"The provided due date is not valid: ${ex.getMessage}")
                    }
                  case None =>
                    AddTaskToTaskList(taskName, taskListName, taskDescriptionOpt, None, recurrence)
                }
              case Failure(ex) => InvalidCommand(s"The $commandString cannot be executed with an invalid recurrence string: ${ex.getMessage}.")
            }

          case None =>
            InvalidCommand(s"The $commandString command cannot be executed if a task list is not selected")
        }
      case None =>
        InvalidCommand(s"The $commandString command is missing the mandatory -n switch")
    }
  }
  //scalastyle:on
}

object RemoveTaskFromListFactory extends CommandFactory {

  override val commandString = "rm"

  def apply(switchesValues: Map[String, String], selectedTaskList: Option[String]): Command = {
    val taskIdOpt = switchesValues.get("-t")

    taskIdOpt match {
      case Some(taskIdStr) =>
        Try {
          taskIdStr.toLong
        } match {
          case Success(taskId) =>
            selectedTaskList match {
              case Some(taskListName) =>
                RemoveTaskFromTaskList(taskId.toLong, taskListName)
              case None =>
                InvalidCommand(s"The $commandString command cannot be executed if a task list is not selected")
            }
          case Failure(_) => InvalidCommand(s"Could not generate command with non-numeric task id $taskIdStr")
        }
      case None =>
        InvalidCommand(s"The $commandString command is missing the mandatory -t switch")
    }
  }
}

object MarkTaskAsDoneFactory extends CommandFactory {

  override val commandString = "done"

  def apply(switchesValues: Map[String, String], selectedTaskList: Option[String]): Command = {
    val taskIdOpt = switchesValues.get("-t")

    taskIdOpt match {
      case Some(taskIdStr) =>
        Try {
          taskIdStr.toLong
        } match {
          case Success(taskId) =>
            selectedTaskList match {
              case Some(taskListName) =>
                MarkDone(taskId.toLong, taskListName)
              case None =>
                InvalidCommand(s"The $commandString command cannot be executed if a task list is not selected")
            }
          case Failure(_) => InvalidCommand(s"Could not generate command with non-numeric task id $taskIdStr")
        }
      case None =>
        InvalidCommand(s"The $commandString command is missing the mandatory -t switch")
    }
  }
}

object ModifyTaskListFactory extends CommandFactory {

  override val commandString = "modify tasklist"

  def apply(switchesValues: Map[String, String]): Command = {
    val taskListNameOpt = switchesValues.get("-n")
    val taskListEmailOpt = switchesValues.get("-e")
    val taskListNewNameOpt = switchesValues.get("-nn")

    taskListNameOpt match {
      case Some(taskListName) =>
        if (taskListNewNameOpt.nonEmpty && taskListNewNameOpt.get.isEmpty) {
          InvalidCommand(s"The $commandString command cannot be executed without a value for the -nn switch")
        } else {
          ModifyTaskList(taskListName, taskListNewNameOpt, taskListEmailOpt)
        }
      case None =>
        InvalidCommand(s"The $commandString command is missing the mandatory -n switch")
    }
  }
}

object ModifyTaskFactory extends CommandFactory {

  override val commandString = "modify"

  //scalastyle:off
  def apply(switchValues: Map[String, String], selectedTaskList: Option[String]): Command = {
    val taskIdOpt = switchValues.get("-t")
    val taskNameOpt = switchValues.get("-n")
    val taskDescriptionOpt = switchValues.get("-d")
    val taskDueDateOpt = switchValues.get("-dd")
    val taskRecurrence = Try(Recurrence.getRecurrence(switchValues.getOrElse("-r", Recurrence.NEVER.name)))

    taskIdOpt match {
      case Some(taskIdStr) =>
        Try {
          taskIdStr.toLong
        } match {
          case Success(taskId) =>
            selectedTaskList match {
              case Some(taskList) =>
                taskRecurrence match {
                  case Success(recurrence) =>
                    taskDueDateOpt match {
                      case Some(taskDueDateStr) =>
                        ArgsUtils.extractDateTimeFromString(taskDueDateStr) match {
                          case Success(zonedDateTime) =>
                            ModifyTask(taskId, taskList, taskNameOpt, taskDescriptionOpt, zonedDateTime, recurrence)
                          case Failure(ex) =>
                            InvalidCommand(s"The provided due date is not valid: ${ex.getMessage}")
                        }
                      case None =>
                        ModifyTask(taskId, taskList, taskNameOpt, taskDescriptionOpt, None, recurrence)
                    }
                  case Failure(ex) => InvalidCommand(s"The $commandString cannot be executed with an invalid recurrence string: ${ex.getMessage}.")
                }
              case None =>
                InvalidCommand(s"The $commandString command cannot be executed if a task list is not selected")
            }
          case Failure(_) =>
            InvalidCommand(s"Could not generate command with non-numeric task id $taskIdStr")
        }
      case None =>
        InvalidCommand(s"The $commandString command is missing the mandatory -t switch")
    }
  }
  //scalastyle:on
}

object GetTaskListFactory extends CommandFactory {
  override val commandString: String = "ls"

  def apply(switchValues: Map[String, String], selectedTaskList: Option[String]): Command = {
    val taskListNameOpt = selectedTaskList.orElse(switchValues.get("-n"))
    val withCompleted = switchValues.get("-c").isDefined

    taskListNameOpt match {
      case Some(taskListName) =>
        GetTaskList(taskListName, withCompleted)
      case None =>
        InvalidCommand(s"The $commandString command is missing the mandatory -n switch")
    }
  }
}

// Ctask commands

sealed trait Command

case object ShowTaskLists extends Command

case class AddTaskList(name: String, email: Option[String]) extends Command

case class RemoveTaskList(name: String) extends Command

case class AddTaskToTaskList(name: String,
                             taskListName: String,
                             description: Option[String],
                             dueDate: Option[ZonedDateTime],
                             recurrence: Recurrence) extends Command

case class RemoveTaskFromTaskList(taskId: Long, taskListName: String) extends Command

case class MarkDone(taskId: Long, taskListName: String) extends Command

case class MarkReminderSent(taskId: Long, taskListName: String) extends Command

case class ModifyTaskList(taskListName: String, newName: Option[String], email: Option[String]) extends Command

case class ModifyTask(taskId: Long,
                      taskListName: String,
                      taskNameOpt: Option[String],
                      description: Option[String],
                      dueDate: Option[ZonedDateTime],
                      recurrence: Recurrence) extends Command

case class GetTaskList(taskListName: String, withCompleted: Boolean) extends Command

case class InvalidCommand(message: String) extends Command
