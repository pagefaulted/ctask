package com.ctask.data

import java.time.{ZoneId, ZonedDateTime}

import com.fasterxml.jackson.core.JsonParseException
import org.scalatest.{FlatSpec, Matchers}
import play.api.libs.json.{JsError, JsSuccess, Json}

import scala.collection.mutable
import scala.util.{Failure, Random, Success, Try}
import com.ctask.data.TaskJsonUtils.TaskOrdering

/**
  * Spec file for the Task class.
  */
class TaskSpec extends FlatSpec with Matchers {

  behavior of "Task Json converters"

  it should "convert a Task without a due date to the expected json string" in {
    val task = new Task("taskName",
      "desc",
      2, null,
      false,
      Task.Recurrence.NEVER,
      false)

    val expectedJsonString = "{\"name\":\"taskName\"," +
                             "\"description\":\"desc\"," +
                             "\"id\":2," +
                             "\"dueDate\":\"none\"," +
                             "\"done\":false," +
                             "\"recurrence\":\"never\"," +
                             "\"reminderSent\":false}"

    Json.toJson(task)(TaskJsonUtils.taskWrites).toString shouldBe expectedJsonString
  }

  it should "convert a Task with a due date to the expected json string" in {
    val task = new Task("taskName",
      "desc",
      2,
      ZonedDateTime.of(2000, 10, 1, 12, 30, 0, 0, ZoneId.of("Europe/Dublin")),
      false,
      Task.Recurrence.NEVER,
      false)

    val expectedJsonString = "{\"name\":\"taskName\"," +
                             "\"description\":\"desc\"," +
                             "\"id\":2," +
                             "\"dueDate\":\"2000-10-01T12:30:00+01:00[Europe/Dublin]\"," +
                             "\"done\":false," +
                             "\"recurrence\":\"never\"," +
                             "\"reminderSent\":false}"

    Json.toJson(task)(TaskJsonUtils.taskWrites).toString shouldBe expectedJsonString
  }

  it should "convert a json string with a null due date to the expected Task object" in {
    val expectedTask = new Task("taskName",
      "desc",
      2,
      null,
      false,
      Task.Recurrence.NEVER,
      false)

    val jsonString = "{\"name\":\"taskName\"," +
                     "\"description\":\"desc\"," +
                     "\"id\":2," +
                     "\"dueDate\":\"none\"," +
                     "\"done\":false," +
                     "\"recurrence\":\"never\"," +
                     "\"reminderSent\":false}"

    Json.parse(jsonString).validate[Task](TaskJsonUtils.reads) match {
      case t: JsSuccess[Task] =>
        t.value shouldBe expectedTask
      case error: JsError => fail(s"Expected object was not a Task: $error")
    }
  }

  it should "convert a json string with a due date to the expected Task object" in {
    val jsonString = "{\"name\":\"taskName\"," +
                     "\"description\":\"desc\"," +
                     "\"id\":2," +
                     "\"dueDate\":\"2000-10-01T12:30:00+01:00[Europe/Dublin]\"" +
                     ",\"done\":false," +
                     "\"recurrence\":\"never\"," +
                     "\"reminderSent\":false}"

    val expectedTask = new Task("taskName",
      "desc",
      2,
      ZonedDateTime.of(2000, 10, 1, 12, 30, 0, 0, ZoneId.of("Europe/Dublin")),
      false,
      Task.Recurrence.NEVER,
      false)

    Json.parse(jsonString).validate[Task](TaskJsonUtils.reads) match {
      case t: JsSuccess[Task] =>
        t.value shouldBe expectedTask
      case error: JsError => fail(s"Expected object was not a Task: $error")
    }
  }

  it should "report an error when converting a json string that does not map to a Task" in {
    // String with invalid name member (am)
    val jsonString = "{\"am\":\"taskName\"," +
                     "\"description\":\"desc\"," +
                     "\"id\":2," +
                     "\"dueDate\":null," +
                     "\"done\":false," +
                     "\"recurrence\":\"never\"," +
                     "\"reminderSent\":false}"

    Json.parse(jsonString).validate[Task](TaskJsonUtils.reads).isSuccess shouldBe false
  }

  it should "report an error when converting an invalid json string" in {
    // Invalid json string
    val jsonString = "{:\"taskName\"," +
                     "\"description\":\"desc\"," +
                     "\"id\":2," +
                     "\"dueDate\":null," +
                     "\"done\":false," +
                     "\"recurrence\":\"never\"," +
                     "\"reminderSent\":false}"

    Try(Json.parse(jsonString)) match {
      case Success(_) => fail("Should have thrown an exception since the json string is invalid.")
      case Failure(ex) => ex shouldBe a[JsonParseException]
    }
  }

  it should "convert a json string with a non-never recurrence" in {
    val jsonString = "{\"name\":\"taskName\"," +
                     "\"description\":\"desc\"," +
                     "\"id\":2," +
                     "\"dueDate\":\"2000-10-01T12:30:00+01:00[Europe/Dublin]\"" +
                     ",\"done\":false," +
                     "\"recurrence\":\"daily\"," +
                     "\"reminderSent\":false}"

    val expectedTask = new Task("taskName",
      "desc",
      2,
      ZonedDateTime.of(2000, 10, 1, 12, 30, 0, 0, ZoneId.of("Europe/Dublin")),
      false,
      Task.Recurrence.DAILY,
      false)

    Json.parse(jsonString).validate[Task](TaskJsonUtils.reads) match {
      case t: JsSuccess[Task] =>
        t.value shouldBe expectedTask
      case error: JsError => fail(s"Expected object was not a Task: $error")
    }
  }

  it should "report an error when converting a json string containing a non-existent recurrence string" in {
    val jsonString = "{\"name\":\"taskName\"," +
                     "\"description\":\"desc\"," +
                     "\"id\":2," +
                     "\"dueDate\":\"none\"," +
                     "\"done\":false," +
                     "\"recurrence\":\"wrong recurrence\"," +
                     "\"reminderSent\":false}"

    an[IllegalArgumentException] shouldBe thrownBy(Json.parse(jsonString).validate[Task](TaskJsonUtils.reads))
  }

  it should "report an error when converting a json string containing an invalid due date" in {
    val jsonString = "{\"name\":\"taskName\"," +
                     "\"description\":\"desc\"," +
                     "\"id\":2," +
                     "\"dueDate\":\"non valid\"," +
                     "\"done\":false," +
                     "\"recurrence\":\"never\"," +
                     "\"reminderSent\":false}"

    an[Exception] shouldBe thrownBy(Json.parse(jsonString).validate[Task](TaskJsonUtils.reads))
  }

  it should "convert a json string with a reminderSent flag set" in {
    val jsonString = "{\"name\":\"taskName\"," +
                     "\"description\":\"desc\"," +
                     "\"id\":2," +
                     "\"dueDate\":\"2000-10-01T12:30:00+01:00[Europe/Dublin]\"" +
                     ",\"done\":false," +
                     "\"recurrence\":\"daily\"," +
                     "\"reminderSent\":true}"

    val expectedTask = new Task("taskName",
      "desc",
      2,
      ZonedDateTime.of(2000, 10, 1, 12, 30, 0, 0, ZoneId.of("Europe/Dublin")),
      false,
      Task.Recurrence.DAILY,
      true)

    Json.parse(jsonString).validate[Task](TaskJsonUtils.reads) match {
      case t: JsSuccess[Task] =>
        t.value shouldBe expectedTask
      case error: JsError => fail(s"Expected object was not a Task: $error")
    }
  }

  it should "order tasks by due date" in {
    val now = ZonedDateTime.now
    val firstTask = new Task("name", "", 1, now, false, Task.Recurrence.NEVER, false)
    val secondTask = new Task("name", "", 2, now.plusMinutes(1), false, Task.Recurrence.NEVER, false)
    val thirdTask = new Task("name", "", 3, now.plusMinutes(2), false, Task.Recurrence.NEVER, false)
    val fourthTask = new Task("name", "", 4, None.orNull, false, Task.Recurrence.NEVER, false)
    val fifthTask = new Task("name", "", 5, None.orNull, false, Task.Recurrence.NEVER, false)

    val expectedOrder = mutable.LinkedHashSet(firstTask, secondTask, thirdTask, fourthTask, fifthTask)
    val shuffled = Random.shuffle(expectedOrder)
    val ordered = shuffled.toList.sorted
    ordered should contain theSameElementsInOrderAs expectedOrder
  }
}
