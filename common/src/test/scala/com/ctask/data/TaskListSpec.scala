package com.ctask.data

import org.scalatest.{FlatSpec, Matchers}
import play.api.libs.json.Json

/**
  * Spec file for TaskList.
  */
class TaskListSpec extends FlatSpec with Matchers {

  behavior of "TaskList Json converters"

  it should "convert a TaskList with no tasks" in {
    val expectedJson = "{\"nameStr\":\"taskList\"," +
                       "\"tasks\":[]," +
                       "\"email\":\"my@email.it\"}"

    val taskList = new TaskList("taskList", Array.empty, "my@email.it")

    Json.toJson(taskList)(TaskListJsonUtils.taskListWrites).toString shouldBe expectedJson
  }

  it should "convert a TaskList with no email" in {
    val expectedJson = "{\"nameStr\":\"taskList\"," +
                       "\"tasks\":[]," +
                       "\"email\":null}"

    val taskList = new TaskList("taskList", Array.empty, None.orNull)

    Json.toJson(taskList)(TaskListJsonUtils.taskListWrites).toString shouldBe expectedJson
  }

  it should "convert a TaskList with tasks" in {
    val expectedJson = "{\"nameStr\":\"taskList\"," +
                       "\"tasks\":[" +
                         "{\"name\":\"taskName\"," +
                         "\"description\":\"desk\"," +
                         "\"id\":2," +
                         "\"dueDate\":\"none\"," +
                         "\"done\":false," +
                         "\"recurrence\":\"never\"," +
                         "\"reminderSent\":false}" +
                       "]," +
                       "\"email\":\"my@email.it\"}"

    val taskList = new TaskList("taskList",
      Array(new Task("taskName", "desk", 2, None.orNull, false, Task.Recurrence.NEVER, false)),
      "my@email.it")

    Json.toJson(taskList)(TaskListJsonUtils.taskListWrites).toString shouldBe expectedJson
  }
}
