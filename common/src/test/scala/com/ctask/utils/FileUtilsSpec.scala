package com.ctask.utils

import java.io.File

import org.scalatest.{BeforeAndAfterEach, FlatSpec, Matchers}

import scala.util.{Failure, Random, Success}

/**
  * Spec file for FileUtils
  */
class FileUtilsSpec extends FlatSpec with Matchers with BeforeAndAfterEach {

  private val randomDirNameLength = 12
  private val specDirPath = new File(
    s"/tmp/${Random.alphanumeric.take(randomDirNameLength).mkString}").getCanonicalPath

  override def beforeEach(): Unit = {
    super.beforeEach()

    // Create random dir in /tmp/ that will hold the test dirs and files.
    val retVal = new File(specDirPath).mkdir()
    assert(retVal === true, "Could not create test dir.")
  }

  override def afterEach(): Unit = {
    super.afterEach()

    val removedPaths = rmDirRecursive(specDirPath)
    assert(removedPaths.contains(specDirPath), "Could not remove the test dir.")
  }

  behavior of "mkdir"

  it should "create a dir when a valid full path is provided" in {
    val testDirPath = s"$specDirPath/mkdirTest"
    FileUtils.mkdir(testDirPath) match {
      case Success(dirFile) => dirFile.exists() shouldBe true
      case _ => fail("Could not create the test dir.")
    }
  }

  it should "return a failure with a RuntimeException when the dir could not be created" in {
    // try create a dir that already exists
    FileUtils.mkdir(specDirPath) match {
      case Success(dirFile) => fail("Should have failed since the dir already exists.")
      case Failure(ex) => ex shouldBe a[RuntimeException]
    }
  }

  behavior of "rmdir"

  it should "remove an empty dir" in {
    val testDirPath = s"$specDirPath/mkdirTest"
    FileUtils.mkdir(testDirPath) match {
      case Success(dirFile) =>
        dirFile.exists() shouldBe true
        FileUtils.rmDir(testDirPath) should contain(testDirPath)
      case Failure(ex) => fail("Could not create the test dir.")
    }
  }

  it should "remove a dir containing files" in {
    val testDirPath = s"$specDirPath/mkdirTest"
    FileUtils.mkdir(testDirPath) match {
      case Success(dirFile) =>
        dirFile.exists() shouldBe true
        new File(dirFile, "testFile").createNewFile() shouldBe true
        FileUtils.rmDir(testDirPath) should contain(testDirPath)
      case Failure(ex) => fail("Could not create the test dir.")
    }
  }

  it should "remove a dir containing an empty dir" in {
    val testDirPath = s"$specDirPath/mkdirTest"
    val subDirPath = s"$testDirPath/subDir"
    FileUtils.mkdir(testDirPath) match {
      case Success(dirFile) =>
        dirFile.exists() shouldBe true
        FileUtils.mkdir(subDirPath) match {
          case Success(subDirFile) =>
            FileUtils.rmDir(testDirPath) should contain(testDirPath)
          case Failure(ex) => fail("Could not create sub test dir.")
        }
      case Failure(ex) => fail("Could not create the test dir.")
    }
  }

  it should "not remove a dir containing a non empty dir" in {
    val testDirPath = s"$specDirPath/mkdirTest"
    val subDirPath = s"$testDirPath/subDir"
    FileUtils.mkdir(testDirPath) match {
      case Success(dirFile) =>
        dirFile.exists() shouldBe true
        FileUtils.mkdir(subDirPath) match {
          case Success(subDirFile) =>
            // add file to subdir
            new File(subDirFile, "testFile").createNewFile() shouldBe true
            FileUtils.rmDir(testDirPath) shouldBe empty
          case Failure(ex) => fail("Could not create sub test dir.")
        }
      case Failure(ex) => fail("Could not create the test dir.")
    }
  }

  behavior of "rmFilesInDir"

  it should "remove all the files in the provided directory" in {
    val testDirPath = s"$specDirPath/mkdirTest"
    val testFilesPaths = s"$testDirPath/testFile1" :: s"$testDirPath/testFile2" :: Nil
    FileUtils.mkdir(testDirPath) match {
      case Success(dirFile) =>
        dirFile.exists() shouldBe true
        testFilesPaths.foreach { testFilePath =>
          new File(testFilePath).createNewFile() shouldBe true
        }
        val removedFiles = FileUtils.rmInDir(testDirPath)
        removedFiles should contain theSameElementsAs testFilesPaths
      case Failure(ex) => fail("Could not create the test dir.")
    }
  }

  it should "not remove a directory" in {
    val testDirPath = s"$specDirPath/mkdirTest"
    FileUtils.mkdir(testDirPath) match {
      case Success(dirFile) =>
        dirFile.exists() shouldBe true
        FileUtils.rmInDir(testDirPath) shouldNot contain(testDirPath)
      case Failure(ex) => fail("Could not create the test dir.")
    }
  }

  it should "not remove a non-empty dir contained in the provided directory whilst removing files" +
    " contained in the top dir" in {
    val testDirPath = s"$specDirPath/mkdirTest"
    val subDirPath = s"$testDirPath/subDir"
    val testFileName = "testFile"
    FileUtils.mkdir(testDirPath) match {
      case Success(dirFile) =>
        dirFile.exists() shouldBe true
        // add file to top dir
        new File(testDirPath, testFileName).createNewFile() shouldBe true
        FileUtils.mkdir(subDirPath) match {
          case Success(subDirFile) =>
            // add file to subdir
            new File(subDirFile, testFileName).createNewFile() shouldBe true
            FileUtils.rmInDir(testDirPath) should contain only(s"$testDirPath/$testFileName")
          case Failure(ex) => fail("Could not create sub test dir.")
        }
      case Failure(ex) => fail("Could not create the test dir.")
    }
  }

  it should "do nothing if the provided path does not identify a dir" in {
    val testDirPath = s"$specDirPath/mkdirTest"
    val testFileName = "testFileName"

    FileUtils.mkdir(testDirPath) match {
      case Success(dirFile) =>
        dirFile.exists() shouldBe true
        new File(dirFile, testFileName).createNewFile() shouldBe true
        FileUtils.rmInDir(s"$testDirPath/$testFileName") shouldBe empty
      case Failure(ex) => fail("Could not create test dir.")
    }
  }

  /**
    * This is a scary method. It rm -fr the provided path. It is used only for cleaning up test
    * dirs and it guards against executing outside of the /tmp/ dir.
    *
    * @param dirPath the dir path to remove recursively.
    * @return the list of removed files.
    */
  private def rmDirRecursive(dirPath: String): List[String] = {
    val dirFile = new File(dirPath)
    // It is important to use canonical path here, since .. can navigate out of /tmp/
    require(dirFile.getCanonicalPath.startsWith(s"$specDirPath"))

    def rmDirRecImpl(dirFile: File): List[String] = {
      dirFile.list().collect {
        case fileName if new File(dirFile, fileName).isDirectory =>
          rmDirRecImpl(new File(dirFile, fileName))
      }.toList.flatten ::: FileUtils.rmDir(dirFile.getCanonicalPath)
    }

    dirFile.isDirectory match {
      case true => rmDirRecImpl(dirFile)
      case false =>
        println(s"WARN: $dirPath is not a directory.")
        Nil
    }
  }
}
