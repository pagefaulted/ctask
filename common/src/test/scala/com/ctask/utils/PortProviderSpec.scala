package com.ctask.utils

import org.scalatest.{FlatSpec, Matchers}

/**
  * Spec file for PortProvider
  */
class PortProviderSpec extends FlatSpec with Matchers {

  behavior of "PortProvider"

  it should "provide a unique port" in {
    val portsList = PortProvider.getPortRange

    portsList.foreach { expectedPort =>
      PortProvider.unique shouldBe expectedPort
    }
  }
}
