package com.ctask.utils

import com.typesafe.config.{Config, ConfigException, ConfigFactory}
import org.scalatest.{FlatSpec, Matchers}

/**
  * Spec file for ParameterRetriever.
  */
class ParameterRetrieverSpec extends FlatSpec with Matchers {

  behavior of "ParameterRetriever"

  val testConfig = ConfigFactory.load("parameterRetriever").getConfig("parameters")

  it should "return the arg from the command line over the arg from the config" in {
    val arg = Parameter("-race", "race")
    val commandLineArgValue = "zerg"
    val commandLineArgs = s"${arg.switch} $commandLineArgValue".split(" ")

    testConfig.getString(arg.configKey) should not be commandLineArgValue

    val argsTest = new TestParameterRetriever(commandLineArgs, testConfig)
    argsTest.getParameter(arg).get shouldBe commandLineArgValue
  }

  it should "return the arg from the command line if the arg is only in the command line" in {
    val arg = Parameter("-map", "map")
    val commandLineArgValue = "whirlwind"
    val commandLineArgs = s"${arg.switch} $commandLineArgValue".split(" ")

    a[ConfigException] should be thrownBy testConfig.getString(arg.configKey)

    val argsTest = new TestParameterRetriever(commandLineArgs, testConfig)
    argsTest.getParameter(arg).get shouldBe commandLineArgValue
  }

  it should "return the arg from the config if it is not in the command line" in {
    val arg = Parameter("-difficulty", "difficulty")
    val commandLineArgs = "".split(" ")
    val argsTest = new TestParameterRetriever(commandLineArgs, testConfig)

    argsTest.getParameter(arg).get shouldBe "elite"
  }

  it should "return None if the arg is not present" in {
    val arg = Parameter("-missingArg", "missingArg")
    val commandLineArgs = "".split(" ")
    val argsTest = new TestParameterRetriever(commandLineArgs, testConfig)

    argsTest.getParameter(arg) shouldBe None
  }

  class TestParameterRetriever(args: Array[String], config: Config)
    extends ParameterRetriever(args, config)
}
