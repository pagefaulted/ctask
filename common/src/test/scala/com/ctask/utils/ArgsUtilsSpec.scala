package com.ctask.utils

import java.time.{ZoneId, ZonedDateTime}

import com.ctask.utils.ArgsUtils._
import org.scalatest.{FlatSpec, Matchers}

import scala.util.{Failure, Success}

/**
  * Spec file for args utils.
  */
class ArgsUtilsSpec extends FlatSpec with Matchers {

  behavior of "parse"

  it should "return an empty map if no args are provided" in {
    ArgsUtils.parse(Array.empty[String]) shouldBe empty
  }

  it should "return an empty map if the first elements in the args is not prepended with the " +
    "separator" in {

    val args = Array("not the separator", "-n", "argument")
    ArgsUtils.parse(args) shouldBe empty
  }

  it should "return a map with an entry whose key is the arg switch and whose value is every " +
    "element up to the next arg switch" in {

    val argSwitch = "-a"
    val argValue = "more than one after arg switch"
    val args = s"$argSwitch $argValue"
    val parsedArgs = ArgsUtils.parse(args.split(" "))

    parsedArgs.size shouldBe 1
    parsedArgs(argSwitch) shouldBe argValue
  }

  it should "be able to parse args containing several arg switches" in {
    val arg1Switch = "-first"
    val arg1Value = "value1"
    val arg2Switch = "-second"
    val arg2Value = "value2"
    val args = s"$arg1Switch $arg1Value $arg2Switch $arg2Value"
    val parsedArgs = ArgsUtils.parse(args.split(" "))

    parsedArgs.size shouldBe 2
    parsedArgs(arg1Switch) shouldBe arg1Value
    parsedArgs(arg2Switch) shouldBe arg2Value
  }

  it should "return a map with an arg switch as key and an empty value if only the arg switch is " +
    " provided" in {

    val arg1Switch = "-one"
    val arg2Switch = "-two"
    val args = s"$arg1Switch $arg2Switch"
    val parsedArgs = ArgsUtils.parse(args.split(" "))

    parsedArgs.size shouldBe 2
    parsedArgs(arg1Switch) shouldBe empty
    parsedArgs(arg2Switch) shouldBe empty
  }

  it should "handle a single arg switch that contains the separator" in {
    val argSwitchContainingSeparator = "-arg-withswitch"
    val argValue = "value"
    val args = s"$argSwitchContainingSeparator $argValue"
    val parsedArgs = ArgsUtils.parse(args.split(" "))

    parsedArgs.size shouldBe 1
    parsedArgs(argSwitchContainingSeparator) shouldBe argValue
  }

  it should "be able to handle an args separator different from the default" in {
    val argSeparator = "+"
    val argSwitch = s"${argSeparator}p"
    val argValue = "value"
    val args = s"$argSwitch $argValue"
    val parsedArgs = ArgsUtils.parse(args.split(" "), argSeparator)

    parsedArgs.size shouldBe 1
    parsedArgs(argSwitch) shouldBe argValue
  }

  it should "be able to handle an arg switch that contains only the separator" in {
    val argSeparator = "+"
    val argSwitch = s"$argSeparator"
    val argValue = "value"
    val args = s"$argSwitch $argValue"
    val parsedArgs = ArgsUtils.parse(args.split(" "), argSeparator)

    parsedArgs.size shouldBe 1
    parsedArgs(argSwitch) shouldBe argValue
  }

  it should "handle having multiple arg switches by appending the arg values" in {
    val argSwitch = "-a"
    val arg1Value = "value1"
    val arg2Value = "value2"
    val args = s"$argSwitch $arg1Value $argSwitch $arg2Value"
    val parsedArgs = ArgsUtils.parse(args.split(" "))

    parsedArgs.size shouldBe 1
    parsedArgs(argSwitch) shouldBe s"$arg1Value $arg2Value"
  }

  // This exposed an issue using the map.last method, which can return different results over
  // different iterations unless ordering is implemented.
  it should "be able to handle longer args list" in {
    val args = "-c addtask -n task name -d task description " +
      "-a 03/06/2017,18:30 -t task list name"

    val expectedParsedArgs = Map("-n" -> "task name", "-d" -> "task description",
                                 "-a" -> "03/06/2017,18:30", "-t" -> "task list name",
                                 "-c" -> "addtask")

    val parsedArgs = ArgsUtils.parse(args.split(" "))

    parsedArgs shouldBe expectedParsedArgs
  }

  it should "handle strings with extra white spaces prepended and appended" in {
    val args = "    -n task name   "
    val expectedParsedArgs = Map("-n" -> "task name")

    val actualResult = ArgsUtils.parse(args.split(" "))

    assert(actualResult === expectedParsedArgs)
  }

  behavior of "extractDateTimeFromString"

  it should "fail if an empty string is provided" in {
    ArgsUtils.extractDateTimeFromString("") shouldBe a[Failure[_]]
  }

  it should "fail if the date time string does not contain the date" in {
    ArgsUtils.extractDateTimeFromString("11:30") shouldBe a[Failure[_]]
  }

  it should "fail if the date time string does not contain the time" in {
    ArgsUtils.extractDateTimeFromString("11/12/2019") shouldBe a[Failure[_]]
  }

  it should "fail if an invalid date is provided" in {
    ArgsUtils.extractDateTimeFromString("11:30 33/12/2019") shouldBe a[Failure[_]]
  }

  it should "fail if an invalid hour is provided" in {
    ArgsUtils.extractDateTimeFromString("11:66 11/12/2019") shouldBe a[Failure[_]]
  }

  it should "return a ZonedDateTime with the correct date time when the correct date time string is provided" in {
    val zonedDateTime = ArgsUtils.extractDateTimeFromString("11:30 8/10/2018")
    zonedDateTime shouldBe a[Success[_]]
    zonedDateTime.get.get.getHour shouldBe 11
    zonedDateTime.get.get.getMinute shouldBe 30
    zonedDateTime.get.get.getDayOfMonth shouldBe 8
    zonedDateTime.get.get.getMonthValue shouldBe 10
    zonedDateTime.get.get.getYear shouldBe 2018
    zonedDateTime.get.get.getZone shouldBe ZoneId.systemDefault()
  }

  it should "return a ZonedDateTime with the correct date when a TextDate string is provided" in {
    val baseDate = ZonedDateTime.now.withHour(MORNING.hourOfTheDay - 1)
    ArgsUtils.extractDateTimeFromString(MORNING.text, baseDate).get.get shouldBe baseDate.withHour(MORNING.hourOfTheDay).withMinute(0)
    ArgsUtils.extractDateTimeFromString(AFTERNOON.text, baseDate).get.get shouldBe baseDate.withHour(AFTERNOON.hourOfTheDay).withMinute(0)
    ArgsUtils.extractDateTimeFromString(EVENING.text, baseDate).get.get shouldBe baseDate.withHour(EVENING.hourOfTheDay).withMinute(0)
    ArgsUtils.extractDateTimeFromString(TOMORROW.text, baseDate).get.get shouldBe baseDate.withHour(TOMORROW.hourOfTheDay).plusDays(1).withMinute(0)
  }

  behavior of "TextDate"

  it should "build a morning date with the same day as the base if the base date has not passed morning's hour of the day" in {
    val baseDate = ZonedDateTime.now.withHour(MORNING.hourOfTheDay - 1)
    MORNING.extractDate(baseDate) shouldBe baseDate.withHour(MORNING.hourOfTheDay).withMinute(0)
  }

  it should "build a morning date with next day if the base date has passed morning's hour of the day" in {
    val baseDate = ZonedDateTime.now.withHour(MORNING.hourOfTheDay)
    MORNING.extractDate(baseDate) shouldBe baseDate.withHour(MORNING.hourOfTheDay).plusDays(1).withMinute(0)
  }

  it should "build a tomorrow date" in {
    val baseDate = ZonedDateTime.now
    TOMORROW.extractDate(baseDate) shouldBe baseDate.plusDays(1).withHour(TOMORROW.hourOfTheDay).withMinute(0)
  }

  it should "build a date of now" in {
    val baseDateTime = ZonedDateTime.now
    NOW.extractDate(baseDateTime) shouldBe baseDateTime
  }
}
