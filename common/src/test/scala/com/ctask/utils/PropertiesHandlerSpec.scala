package com.ctask.utils
import com.typesafe.config.{Config, ConfigFactory}
import org.scalatest.{BeforeAndAfterAll, FlatSpec, Matchers}

class PropertiesRetrieverSpec extends FlatSpec with Matchers with BeforeAndAfterAll {

  val (systemOnlyPropString, systemOnlyPropStringVal) = ("systemOnlyPropString", "system1")
  val (configOnlyPropString, configOnlyPropStringVal) = ("configOnlyPropString", "config1")
  val (systemAndConfigPropString, systemAndConfigPropStringVal) = ("systemAndConfigPropString", "system2")
  val defaultPropString = "default"

  val (systemOnlyPropInt, systemOnlyPropIntVal) = ("systemOnlyPropInt", "1")
  val (configOnlyPropInt, configOnlyPropIntVal) = ("configOnlyPropInt", "5")
  val (systemAndConfigPropInt, systemAndConfigPropIntVal) = ("systemAndConfigPropInt", "3")
  val defaultPropInt = 4

  override def beforeAll(): Unit = {
    System.setProperty(systemOnlyPropString, systemOnlyPropStringVal)
    System.setProperty(systemAndConfigPropString, systemAndConfigPropStringVal)
    System.setProperty(systemOnlyPropInt, systemOnlyPropIntVal)
    System.setProperty(systemAndConfigPropInt, systemAndConfigPropIntVal)
  }

  override def afterAll(): Unit = {
    System.clearProperty(systemOnlyPropString)
    System.clearProperty(systemAndConfigPropString)
    System.clearProperty(systemOnlyPropInt)
    System.clearProperty(systemAndConfigPropInt)
  }

  behavior of "PropertiesRetriever with strings"

  it should "return the system property over the property stored in the config" in {
    TestPropertiesRetriever.getPropertyStr(systemAndConfigPropString, defaultPropString) shouldBe systemAndConfigPropStringVal
  }

  it should "return the property stored in the config if there isn't a corresponding system property" in {
    TestPropertiesRetriever.getPropertyStr(configOnlyPropString, defaultPropString) shouldBe configOnlyPropStringVal
  }

  it should "return the default value if the property is not a system property nor is in the config" in {
    TestPropertiesRetriever.getPropertyStr("non existing", defaultPropString) shouldBe defaultPropString
  }

  behavior of "PropertiesRetriever with ints"

  it should "return the system property over the property stored in the config" in {
    TestPropertiesRetriever.getPropertyInt(systemAndConfigPropInt, defaultPropInt) shouldBe systemAndConfigPropIntVal.toInt
  }

  it should "return the property stored in the config if there isn't a corresponding system property" in {
    TestPropertiesRetriever.getPropertyInt(configOnlyPropInt, defaultPropInt) shouldBe configOnlyPropIntVal.toInt
  }

  it should "return the default value if the property is not a system property nor is in the config" in {
    TestPropertiesRetriever.getPropertyInt("non existing", defaultPropInt) shouldBe defaultPropInt
  }

  object TestPropertiesRetriever extends PropertiesRetriever {
    override val config: Config = ConfigFactory.load("propertiesHandlerSpecConfig")

    def getConfig: Config = config
  }
}
