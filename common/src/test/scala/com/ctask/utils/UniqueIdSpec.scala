package com.ctask.utils

import org.scalatest.concurrent.Conductors
import org.scalatest.{BeforeAndAfterEach, FlatSpec, Matchers}

/**
  * Spec file for UniqueId
  */
class UniqueIdSpec extends FlatSpec with Matchers with Conductors with BeforeAndAfterEach {

  override def afterEach(): Unit = {
    super.afterEach()
    UniqueId.reset()
  }

  behavior of "UniqueId"

  it should "generate monotonically increasing ids" in {
    UniqueId.getUniqueId shouldBe 1
    UniqueId.getUniqueId shouldBe 2
    UniqueId.getUniqueId shouldBe 3
  }

  it should "be able to handle multiple threads asking for a unique id" in {
    val conductor = new Conductor
    import conductor._

    var id1 = 0L
    var id2 = 0L
    var id3 = 0L
    var id4 = 0L
    var id5 = 0L
    var id6 = 0L

    threadNamed("t1") {
      id1 = UniqueId.getUniqueId
      id1 should not be 0
    }
    threadNamed("t2") {
      id2 = UniqueId.getUniqueId
      id2 should not be 0
    }
    threadNamed("t3") {
      id3 = UniqueId.getUniqueId
      id3 should not be 0
    }
    threadNamed("t4") {
      id4 = UniqueId.getUniqueId
      id4 should not be 0
    }
    threadNamed("t5") {
      id5 = UniqueId.getUniqueId
      id5 should not be 0
    }
    threadNamed("t6") {
      id6 = UniqueId.getUniqueId
      id6 should not be 0
    }

    whenFinished {
      val ids = Set(id1, id2, id3, id4, id5, id6)
      ids.size shouldBe 6
      ids.max shouldBe 6
    }
  }

  it should "be able to reset the initial value" in {
    UniqueId.getUniqueId shouldBe 1

    val customId = 5
    UniqueId.reset(customId)
    UniqueId.getUniqueId shouldBe customId
  }
}
