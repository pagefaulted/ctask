package com.ctask

import org.scalatest.{FlatSpec, Matchers}

/**
  * Spec file for settings.
  */
class ServerPropertiesSpec extends FlatSpec with Matchers {

  behavior of "ServerProperties"

  val defaultValueStr = "def"
  val defaultValueInt = 3

  it should "return the default value if a property is not in the config file" in {
    ServerProperties.getPropertyInt("non existent property", defaultValueInt) shouldBe defaultValueInt
    ServerProperties.getPropertyStr("non existent property", defaultValueStr) shouldBe defaultValueStr
  }

  it should "return the default value if a property is empty in the config file" in {
    ServerProperties.getPropertyInt("com.ctask.nop.prop", defaultValueInt) shouldBe defaultValueInt
    ServerProperties.getPropertyStr("com.ctask.nop.prop", defaultValueStr) shouldBe defaultValueStr
  }

  it should "give priority to a property defined via java properties over the config file" in {
    val propKey = "com.ctask.storage.root"

    try {
      val javaPropVal = "java prop val "
      System.setProperty(propKey, javaPropVal)
      ServerProperties.getPropertyStr(propKey, defaultValueStr) shouldBe javaPropVal
    } finally {
      System.clearProperty(propKey)
    }
  }
}
