package com.ctask.engine

import java.time.ZonedDateTime

import akka.actor.ActorSystem
import akka.testkit.{EventFilter, TestKit}
import com.ctask.data.Task.Recurrence
import com.ctask.data.{Task, TaskList}
import com.ctask.engine.DueDateChecker.CheckDueDate
import com.typesafe.config.ConfigFactory
import org.scalatest.{BeforeAndAfterAll, FlatSpecLike, Matchers}
import org.scalatest.concurrent.Eventually._

import scala.jdk.CollectionConverters._

object DueDateCheckerSpec {
  val configMap = Map("akka.loggers" -> List("akka.testkit.TestEventListener").asJava)
    .asJava

  val actorSysConfig = ConfigFactory.parseMap(configMap)
}

/**
  * Spec class for the DueDateChecker.
  */
class DueDateCheckerSpec extends TestKit(ActorSystem("DueDateCheckerSpec", DueDateCheckerSpec.actorSysConfig.withFallback(ConfigFactory.load())))
  with FlatSpecLike with BeforeAndAfterAll with Matchers {

  override def afterAll: Unit = {
    TestKit.shutdownActorSystem(system)
  }

  behavior of "DueDateChecker"

  it should "ignore and log unknown messages" in {
    val actor = system.actorOf(DueDateChecker.props(NopExpiredDueDateHandler, NopReminderListener))
    val unknownMessage = None
    EventFilter.warning(message = s"Received and ignored message: $unknownMessage",
      occurrences = 1) intercept {
      actor ! unknownMessage
    }
  }

  it should "execute the due date callback for a task not marked as done with an expired due date" in {
    val countingExpiredDueDateHandler = new CountingExpiredDueDateHandler
    val actor = system.actorOf(DueDateChecker.props(countingExpiredDueDateHandler, NopReminderListener))
    val expiredDueDate = ZonedDateTime.now.minusMinutes(10)
    val testList = new TaskList("Test list", Array(
      new Task("TestTask", "", 1, expiredDueDate, false, Recurrence.NEVER, false)),
      None.orNull)

    actor ! CheckDueDate(testList)
    eventually {
      countingExpiredDueDateHandler.expiredCount shouldBe 1
    }
  }

  it should "not execute the due date callback for a task without a due date" in {
    val countingExpiredDueDateHandler = new CountingExpiredDueDateHandler
    val actor = system.actorOf(DueDateChecker.props(countingExpiredDueDateHandler, NopReminderListener))
    val testList = new TaskList("Test list", Array(
      new Task("TestTask", "", 1, None.orNull, false, Recurrence.NEVER, false)),
      None.orNull)

    actor ! CheckDueDate(testList)
    eventually {
      countingExpiredDueDateHandler.nonExpiredCount shouldBe 1
    }
  }

  it should "not execute the due date callback for a task with a non-expired due date" in {
    val countingExpiredDueDateHandler = new CountingExpiredDueDateHandler
    val actor = system.actorOf(DueDateChecker.props(countingExpiredDueDateHandler, NopReminderListener))
    val expiredDueDate = ZonedDateTime.now.plusMinutes(10)
    val testList = new TaskList("Test list", Array(
      new Task("TestTask", "", 1, expiredDueDate, false, Recurrence.NEVER, false)),
      None.orNull)

    actor ! CheckDueDate(testList)
    eventually {
      countingExpiredDueDateHandler.nonExpiredCount shouldBe 1
    }
  }

  it should "not execute the due date callback for a task with an expired due date but marked as done" in {
    val countingExpiredDueDateHandler = new CountingExpiredDueDateHandler
    val actor = system.actorOf(DueDateChecker.props(countingExpiredDueDateHandler, NopReminderListener))
    val expiredDueDate = ZonedDateTime.now.minusMinutes(10)
    val testList = new TaskList("Test list", Array(
      new Task("TestTask", "", 1, expiredDueDate, true, Recurrence.NEVER, false)),
      None.orNull)

    actor ! CheckDueDate(testList)
    eventually {
      countingExpiredDueDateHandler.nonExpiredCount shouldBe 1
    }
  }

  it should "not execute the due date callback for a task with a reminderSent flag already set" in {
    val countingExpiredDueDateHandler = new CountingExpiredDueDateHandler
    val actor = system.actorOf(DueDateChecker.props(countingExpiredDueDateHandler, NopReminderListener))
    val expiredDueDate = ZonedDateTime.now.minusMinutes(10)
    val testList = new TaskList("Test list", Array(
      new Task("TestTask", "", 1, expiredDueDate, false, Recurrence.NEVER, true)),
      None.orNull)

    actor ! CheckDueDate(testList)
    eventually {
      countingExpiredDueDateHandler.nonExpiredCount shouldBe 1
    }
  }

}

class CountingExpiredDueDateHandler extends ExpiredDueDateHandler {
  var expiredCount = 0
  var nonExpiredCount = 0

  override def onDueDateExpired(task: Task, taskList: TaskList): Unit = expiredCount += 1

  override def onDueDateNonExpired: Unit = nonExpiredCount += 1
}

object NopReminderListener extends ReminderListener {
  override def reminderSent(taskListName: String, taskId: Long): Unit = {}
}
