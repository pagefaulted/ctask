package com.ctask.engine

import java.time.{ZoneId, ZonedDateTime}

import akka.actor.ActorSystem
import akka.testkit.{EventFilter, ImplicitSender, TestKit}
import com.ctask.data.Task.Recurrence
import com.ctask.data.{Task, TaskList}
import com.ctask.messages.ServerResponses.{AddedTaskList, RemovedTask, RemovedTaskList, ServerError, SingleList, TaskLists, UpdatedTaskList}
import com.ctask.messages._
import com.ctask.storage.{BackingStorage, InMemory, StorageException}
import com.ctask.utils.PortProvider
import com.typesafe.config.ConfigFactory
import org.scalatest.{BeforeAndAfterAll, FlatSpecLike, Matchers}
import org.scalatest.concurrent.Eventually._

import scala.jdk.CollectionConverters._
import scala.concurrent.duration._
import scala.util.Failure

private object CommandExecutorSpec {
  val configMap = Map("akka.loggers" -> List("akka.testkit.TestEventListener").asJava,
    "akka.remote.netty.tcp.port" -> PortProvider.unique.toString)
    .asJava

  val actorSysConfig = ConfigFactory.parseMap(configMap)
}

/**
  * Spec for the CommandExecutor.
  */
class CommandExecutorSpec
  extends TestKit(ActorSystem("CommandExecutorSpec", CommandExecutorSpec.actorSysConfig.withFallback(ConfigFactory.load())))
    with FlatSpecLike with Matchers with BeforeAndAfterAll with ImplicitSender {

  val maxWaitTime = 100.millis

  override def afterAll: Unit = {
    TestKit.shutdownActorSystem(system)
  }

  behavior of "CommandExecutor"

  it should "respond with a TaskLists message when it receives a ShowTaskLists message" in {
    val backingStorage = BackingStorage(InMemory)
    val commandExecutor = system.actorOf(CommandExecutor.props(backingStorage))

    // Add tasklist to backing storage
    val testListName = "testlist"
    backingStorage.createTaskList(new TaskList(testListName, Array.empty, None.orNull))

    commandExecutor ! ShowTaskLists
    expectMsgPF(maxWaitTime) {
      case t: TaskLists =>
        t.taskLists.size shouldBe 1
        t.taskLists.asScala.map(_.name) should contain theSameElementsAs List(testListName)
      case unexpected => fail(s"It should have returned a TaskList message, instead it responded with $unexpected")
    }
  }

  it should "respond with an AddedTaskList message with the added TaskList when it receives an " +
    "AddTaskList message" in {

    val backingStorage = BackingStorage(InMemory)
    val commandExecutor = system.actorOf(CommandExecutor.props(backingStorage))

    val testListName = "testlist"
    val testListEmail = "io@me.com"

    commandExecutor ! AddTaskList(testListName, Some(testListEmail))
    expectMsgPF(maxWaitTime) {
      case t: AddedTaskList =>
        t.list.name shouldBe testListName
        t.list.email shouldBe testListEmail
      case unexpected => fail(s"It should have returned an AddedTaskList message, instead it responded with $unexpected")
    }
  }

  it should "respond with a RemovedTaskList message with the removed TaskList when it receives a " +
    "RemoveTaskList message" in {

    val backingStorage = BackingStorage(InMemory)
    val commandExecutor = system.actorOf(CommandExecutor.props(backingStorage))

    // Add tasklist to backing storage
    val testListName = "testlist"
    backingStorage.createTaskList(new TaskList(testListName, Array.empty, None.orNull))

    commandExecutor ! RemoveTaskList(testListName)
    expectMsgPF(maxWaitTime) {
      case t: RemovedTaskList => t.list.name shouldBe testListName
      case unexpected => fail(s"It should have returned a RemovedTaskList message, instead it responded with $unexpected")
    }
  }

  it should "respond with a Failure when it receives an AddTaskList message with a list name that " +
    "already exists" in {

    val backingStorage = BackingStorage(InMemory)
    val commandExecutor = system.actorOf(CommandExecutor.props(backingStorage))

    // Add tasklist to backing storage
    val testListName = "testlist"
    backingStorage.createTaskList(new TaskList(testListName, Array.empty, None.orNull))

    commandExecutor ! AddTaskList(testListName, None)
    expectMsgPF(maxWaitTime) {
      case Failure(ex) => ex shouldBe a[StorageException]
      case unexpected => fail(s"It should have returned a failure, instead it responded with $unexpected")
    }
  }

  it should "respond with Failure when it receives a RemoveTaskList message with a list name that " +
    "does not exist" in {

    val backingStorage = BackingStorage(InMemory)
    val commandExecutor = system.actorOf(CommandExecutor.props(backingStorage))

    val nonExistingTaskList = "iDoNotExist"

    commandExecutor ! RemoveTaskList(nonExistingTaskList)
    expectMsgPF(maxWaitTime) {
      case Failure(ex) => ex shouldBe a[StorageException]
      case unexpected => fail(s"It should have returned a failure, instead it responded with $unexpected")
    }
  }

  it should "log warn when receives an unknown message" in {
    val backingStorage = BackingStorage(InMemory)
    val commandExecutor = system.actorOf(CommandExecutor.props(backingStorage))

    val nonExistentCommand = None

    EventFilter.warning(message = s"Received and ignored command: $nonExistentCommand",
      occurrences = 1) intercept {
      commandExecutor ! nonExistentCommand
    }

    expectMsgPF(maxWaitTime) {
      case e: ServerError => e.msg shouldBe s"Command executor could not handle command: $nonExistentCommand"
    }
  }

  it should "add a task to a task list" in {
    val backingStorage = BackingStorage(InMemory)
    val commandExecutor = system.actorOf(CommandExecutor.props(backingStorage))

    val testListName = "testlist"

    commandExecutor ! AddTaskList(testListName, None)
    expectMsgPF(maxWaitTime) {
      case t: AddedTaskList => t.list.name shouldBe testListName
      case unexpected => fail(s"It should have returned the AddedTaskList message, instead it responded with $unexpected")
    }

    val taskName = "testTask"

    commandExecutor ! AddTaskToTaskList(taskName, testListName, None, None, Recurrence.NEVER)
    expectMsgPF(maxWaitTime) {
      case t: UpdatedTaskList =>
        val expectedTaskList = new TaskList(testListName, Array(
          new Task(taskName, "", t.list.tasks.head.id, None.orNull, false, Recurrence.NEVER, false)),
          None.orNull)

        t.list shouldBe expectedTaskList
      case unexpected => fail(s"It should have returned the UpdatedTaskList message, instead it responded with $unexpected")
    }
  }

  it should "add a task with a due date" in {
    val backingStorage = BackingStorage(InMemory)
    val commandExecutor = system.actorOf(CommandExecutor.props(backingStorage))

    val testListName = "testlist"

    commandExecutor ! AddTaskList(testListName, None)
    expectMsgPF(maxWaitTime) {
      case t: AddedTaskList => t.list.name shouldBe testListName
      case unexpected => fail(s"It should have returned the AddedTaskList message, instead it responded with $unexpected")
    }

    val taskName = "testTask"
    val dueDate = ZonedDateTime.of(18, 10, 8, 11, 39, 0, 0, ZoneId.systemDefault())

    commandExecutor ! AddTaskToTaskList(taskName, testListName, None, Some(dueDate), Recurrence.NEVER)
    expectMsgPF(maxWaitTime) {
      case t: UpdatedTaskList =>
        val expectedTaskList = new TaskList(testListName, Array(
          new Task(taskName, "", t.list.tasks.head.id, dueDate, false, Recurrence.NEVER, false)),
          None.orNull)

        t.list shouldBe expectedTaskList
      case unexpected => fail(s"It should have returned the UpdatedTaskList message, instead it responded with $unexpected")
    }
  }

  it should "respond with a failure if a task is added to a task list that does not exists" in {
    val backingStorage = BackingStorage(InMemory)
    val commandExecutor = system.actorOf(CommandExecutor.props(backingStorage))

    val taskName = "testTask"
    val inexistentTaskListName = "iDoNotExist"

    commandExecutor ! AddTaskToTaskList(taskName, inexistentTaskListName, None, None, Recurrence.NEVER)
    expectMsgPF(maxWaitTime) {
      case Failure(ex) => ex shouldBe a[StorageException]
      case unexpected => fail(s"It should have returned a failure, instead it responded with $unexpected")
    }
  }

  it should "add a task to a task list even if a task with the same name already exists" in {
    val backingStorage = BackingStorage(InMemory)
    val commandExecutor = system.actorOf(CommandExecutor.props(backingStorage))

    val testListName = "testlist"

    commandExecutor ! AddTaskList(testListName, None)
    expectMsgPF(maxWaitTime) {
      case t: AddedTaskList => t.list.name shouldBe testListName
      case unexpected => fail(s"It should have returned the AddedTaskList message, instead it responded with $unexpected")
    }

    val taskName = "testTask"
    var firstTaskId = 0L

    commandExecutor ! AddTaskToTaskList(taskName, testListName, None, None, Recurrence.NEVER)
    expectMsgPF(maxWaitTime) {
      case t: UpdatedTaskList =>
        firstTaskId = t.list.tasks.head.id
        val expectedTaskListFirst = new TaskList(testListName, Array(
          new Task(taskName, "", firstTaskId, None.orNull, false, Recurrence.NEVER, false)),
          None.orNull)

        t.list shouldBe expectedTaskListFirst
      case unexpected => fail(s"It should have returned the UpdatedTaskList message, instead it responded with $unexpected")
    }

    commandExecutor ! AddTaskToTaskList(taskName, testListName, None, None, Recurrence.NEVER)

    expectMsgPF(maxWaitTime) {
      case t: UpdatedTaskList =>
        val expectedTaskListSecond = new TaskList(testListName, Array(
          new Task(taskName, "", firstTaskId, None.orNull, false, Recurrence.NEVER, false),
          new Task(taskName, "", firstTaskId + 1, None.orNull, false, Recurrence.NEVER, false)),
          None.orNull)

        t.list shouldBe expectedTaskListSecond
      case unexpected => fail(s"It should have returned the UpdatedTaskList message, instead it responded with $unexpected")
    }
  }

  it should "remove a task from a task list" in {
    val backingStorage = BackingStorage(InMemory)
    val commandExecutor = system.actorOf(CommandExecutor.props(backingStorage))

    val testListName = "testlist"

    // Add tasklist
    commandExecutor ! AddTaskList(testListName, None)
    expectMsgPF(maxWaitTime) {
      case t: AddedTaskList => t.list.name shouldBe testListName
      case unexpected => fail(s"It should have returned the AddedTaskList message, instead it responded with $unexpected")
    }

    val taskName = "testTask"

    // Add task to tasklist
    commandExecutor ! AddTaskToTaskList(taskName, testListName, None, None, Recurrence.NEVER)
    expectMsgPF(maxWaitTime) {
      case t: UpdatedTaskList =>
        val taskId = t.list.tasks.head.id
        val expectedTask = new Task(taskName, "", taskId, None.orNull, false, Recurrence.NEVER, false)
        val expectedTaskList = new TaskList(testListName, Array(expectedTask), None.orNull)
        t.list shouldBe expectedTaskList

        // Remove task from tasklist
        commandExecutor ! RemoveTaskFromTaskList(taskId, testListName)
        expectMsgPF(maxWaitTime) {
          case t: RemovedTask =>
            t.removedTask shouldBe expectedTask
          case unexpected => fail(s"It should have returned the RemovedTask message, instead it responded with $unexpected")
        }
      case unexpected => fail(s"It should have returned the UpdatedTaskList message, instead it responded with $unexpected")
    }
  }

  it should "respond with a server error when an unknown command is received" in {
    val backingStorage = BackingStorage(InMemory)
    val commandExecutor = system.actorOf(CommandExecutor.props(backingStorage))

    // Unknown command
    val unknownCommand = "unknown command"

    commandExecutor ! unknownCommand
    expectMsgPF(maxWaitTime) {
      case error: ServerError =>
        error.msg shouldBe "Command executor could not handle command: unknown command"
      case unexpected => fail(s"It should have returned a ServerError message, instead it responded with $unexpected")
    }
  }

  it should "respond with a list of tasks in the selected task list when the ls command is received" in {
    val backingStorage = BackingStorage(InMemory)
    val commandExecutor = system.actorOf(CommandExecutor.props(backingStorage))

    val testListName = "test list"
    val testTask = new Task("test task", "", 0xCAFE, None.orNull, false, Recurrence.NEVER, false)
    backingStorage.createTaskList(new TaskList(testListName, Array.empty, None.orNull))
    backingStorage.replaceTaskList(testListName, new TaskList(testListName, Array(testTask), None.orNull))

    commandExecutor ! GetTaskList(testListName, true)
    expectMsgPF(maxWaitTime) {
      case t: SingleList =>
        t.list.tasks.size shouldBe 1
        t.list.tasks should contain only testTask
      case unexpected => fail(s"It should have returned the SingleList message, instead it responded with $unexpected")
    }
  }

  it should "mark a task as done when the done command is received" in {
    val backingStorage = BackingStorage(InMemory)
    val commandExecutor = system.actorOf(CommandExecutor.props(backingStorage))

    val testListName = "test list"
    val taskId = 0xBEEF
    val testTask = new Task("test task", "", taskId, None.orNull, false, Recurrence.NEVER, false)
    testTask.done shouldBe false

    backingStorage.createTaskList(new TaskList(testListName, Array.empty, None.orNull))
    backingStorage.replaceTaskList(testListName, new TaskList(testListName, Array(testTask), None.orNull))

    commandExecutor ! MarkDone(taskId, testListName)
    expectMsgPF(maxWaitTime) {
      case t: UpdatedTaskList =>
        t.list.tasks.length shouldBe 1
        t.list.tasks.head.done shouldBe true
      case unexpected => fail(s"It should have returned the updated task list, instead it responded with $unexpected")
    }
  }

  it should "fail when requested to mark a non-existent task as done" in {
    val backingStorage = BackingStorage(InMemory)
    val commandExecutor = system.actorOf(CommandExecutor.props(backingStorage))

    val testListName = "test list"
    val taskId = 0xBEEF
    val testTask = new Task("test task", "", taskId, None.orNull, false, Recurrence.NEVER, false)
    testTask.done shouldBe false

    backingStorage.createTaskList(new TaskList(testListName, Array.empty, None.orNull))

    commandExecutor ! MarkDone(taskId, testListName)
    expectMsgPF(maxWaitTime) {
      case Failure(ex) => ex shouldBe a[UnknownTaskException]
      case unexpected => fail(s"It should have failed with an UnknowkTaskException, instead it responded with $unexpected")
    }
  }

  it should "modify the name of an existing task list" in {
    val backingStorage = BackingStorage(InMemory)
    val commandExecutor = system.actorOf(CommandExecutor.props(backingStorage))

    val testListName = "test list"
    val testListEmail = "my@email.com"

    backingStorage.createTaskList(new TaskList(testListName, Array.empty, testListEmail))

    val newTaskListName = "new test list"
    commandExecutor ! ModifyTaskList(testListName, Some(newTaskListName), None)
    expectMsgPF(maxWaitTime) {
      case t: UpdatedTaskList =>
        t.list.name shouldBe newTaskListName
        t.list.email shouldBe testListEmail
      case unexpected => fail(s"It should have returned the UpdatedTaskList, instead it responded with $unexpected")
    }
  }

  it should "fail when requested to modify a non-existent task list" in {
    val backingStorage = BackingStorage(InMemory)
    val commandExecutor = system.actorOf(CommandExecutor.props(backingStorage))

    val testListName = "test list"

    val newTaskListName = "new test list"
    commandExecutor ! ModifyTaskList(testListName, Some(newTaskListName), None)
    expectMsgPF(maxWaitTime) {
      case Failure(ex) => ex shouldBe a[StorageException]
      case unexpected => fail(s"It should have returned a StorageException, instead it responded with $unexpected")
    }
  }

  it should "modify the email of an existing task list" in {
    val backingStorage = BackingStorage(InMemory)
    val commandExecutor = system.actorOf(CommandExecutor.props(backingStorage))

    val testListName = "test list"
    val testListEmail = "my@email.com"

    backingStorage.createTaskList(new TaskList(testListName, Array.empty, testListEmail))

    val newTestListEmail = "new@email.com"
    commandExecutor ! ModifyTaskList(testListName, None, Some(newTestListEmail))
    expectMsgPF(maxWaitTime) {
      case t: UpdatedTaskList =>
        t.list.email shouldBe newTestListEmail
      case unexpected => fail(s"It should have returned the UpdatedTaskList, instead it responded with $unexpected")
    }
  }

  it should "fail when requested to modify a task that does not exist" in {
    val backingStorage = BackingStorage(InMemory)
    val commandExecutor = system.actorOf(CommandExecutor.props(backingStorage))

    val taskListName = "test list"
    val taskName = "task name"
    val taskId = 1
    val nonExistentTaskId = 2
    backingStorage.createTaskList(new TaskList(taskListName, Array(
      new Task(taskName, "", taskId, None.orNull, false, Recurrence.NEVER, false)),
      None.orNull))

    commandExecutor ! ModifyTask(nonExistentTaskId, taskListName, None, None, None, Recurrence.NEVER)
    expectMsgPF(maxWaitTime) {
      case Failure(ex) =>
        ex shouldBe an[UnknownTaskException]
      case unexpected => fail(s"It should have returned a failure, instead it responded with $unexpected")
    }
  }

  it should "fail when requested to modify a task from a task list that does not exist" in {
    val backingStorage = BackingStorage(InMemory)
    val commandExecutor = system.actorOf(CommandExecutor.props(backingStorage))
    val taskListName = "i do not exist"
    val taskId = 1

    commandExecutor ! ModifyTask(taskId, taskListName, None, None, None, Recurrence.NEVER)
    expectMsgPF(maxWaitTime) {
      case Failure(ex) =>
        ex shouldBe a[StorageException]
      case unexpected => fail(s"It should have returned a failure, instead it responded with $unexpected")
    }
  }

  it should "modify the name of a task" in {
    val backingStorage = BackingStorage(InMemory)
    val commandExecutor = system.actorOf(CommandExecutor.props(backingStorage))

    val taskListName = "test list"
    val taskName = "task name"
    val taskDescription = ""
    val taskId = 1
    backingStorage.createTaskList(new TaskList(taskListName, Array(
      new Task(taskName, taskDescription, taskId, None.orNull, false, Recurrence.NEVER, false)),
      None.orNull))

    val newTaskName = "new task name"
    commandExecutor ! ModifyTask(taskId, taskListName, Some(newTaskName), None, None, Recurrence.NEVER)
    expectMsgPF(maxWaitTime) {
      case t: UpdatedTaskList =>
        val updatedTask = t.list.tasks.find(_.id == taskId).get
        updatedTask.name shouldBe newTaskName
        updatedTask.description shouldBe taskDescription
        updatedTask.dueDate shouldBe None.orNull
        updatedTask.done shouldBe false
      case unexpected => fail(s"It should have returned an UpdatedTaskList, instead it responded with $unexpected")
    }
  }

  it should "modify the description of a task" in {
    val backingStorage = BackingStorage(InMemory)
    val commandExecutor = system.actorOf(CommandExecutor.props(backingStorage))

    val taskListName = "test list"
    val taskName = "task name"
    val taskDescription = "task desc"
    val taskId = 1
    backingStorage.createTaskList(new TaskList(taskListName, Array(
      new Task(taskName, taskDescription, taskId, None.orNull, false, Recurrence.NEVER, false)),
      None.orNull))

    val newDescription = "new task desc"
    commandExecutor ! ModifyTask(taskId, taskListName, None, Some(newDescription), None, Recurrence.NEVER)
    expectMsgPF(maxWaitTime) {
      case t: UpdatedTaskList =>
        val updatedTask = t.list.tasks.find(_.id == taskId).get
        updatedTask.name shouldBe taskName
        updatedTask.description shouldBe newDescription
        updatedTask.dueDate shouldBe None.orNull
        updatedTask.done shouldBe false
      case unexpected => fail(s"It should have returned an UpdatedTaskList, instead it responded with $unexpected")
    }
  }

  it should "modify the due date of a task if the due date is not already set" in {
    val backingStorage = BackingStorage(InMemory)
    val commandExecutor = system.actorOf(CommandExecutor.props(backingStorage))

    val taskListName = "test list"
    val taskName = "task name"
    val taskDescription = ""
    val taskId = 1
    backingStorage.createTaskList(new TaskList(taskListName, Array(
      new Task(taskName, taskDescription, taskId, None.orNull, false, Recurrence.NEVER, false)),
      None.orNull))

    val newTaskDueDate = ZonedDateTime.of(2000, 11, 10, 4, 39, 0, 0, ZoneId.systemDefault())
    commandExecutor ! ModifyTask(taskId, taskListName, None, None, Some(newTaskDueDate), Recurrence.NEVER)
    expectMsgPF(maxWaitTime) {
      case t: UpdatedTaskList =>
        val updatedTask = t.list.tasks.find(_.id == taskId).get
        updatedTask.name shouldBe taskName
        updatedTask.description shouldBe taskDescription
        updatedTask.dueDate shouldBe newTaskDueDate
        updatedTask.done shouldBe false
      case unexpected => fail(s"It should have returned an UpdatedTaskList, instead it responded with $unexpected")
    }
  }

  it should "modify the due date of a task if the due date is already set" in {
    val backingStorage = BackingStorage(InMemory)
    val commandExecutor = system.actorOf(CommandExecutor.props(backingStorage))

    val taskListName = "test list"
    val taskName = "task name"
    val taskDescription = ""
    val taskDueDate = ZonedDateTime.of(2000, 11, 10, 4, 39, 0, 0, ZoneId.systemDefault())
    val taskId = 1
    backingStorage.createTaskList(new TaskList(taskListName, Array(
      new Task(taskName, taskDescription, taskId, taskDueDate, false, Recurrence.NEVER, false)),
      None.orNull))

    val newTaskDueDate = ZonedDateTime.of(2017, 11, 10, 4, 39, 0, 0, ZoneId.systemDefault())
    commandExecutor ! ModifyTask(taskId, taskListName, None, None, Some(newTaskDueDate), Recurrence.NEVER)
    expectMsgPF(maxWaitTime) {
      case t: UpdatedTaskList =>
        val updatedTask = t.list.tasks.find(_.id == taskId).get
        updatedTask.name shouldBe taskName
        updatedTask.description shouldBe taskDescription
        updatedTask.dueDate shouldBe newTaskDueDate
        updatedTask.done shouldBe false
      case unexpected => fail(s"It should have returned an UpdatedTaskList, instead it responded with $unexpected")
    }
  }

  it should "modify the recurrence of a task" in {
    val backingStorage = BackingStorage(InMemory)
    val commandExecutor = system.actorOf(CommandExecutor.props(backingStorage))

    val taskListName = "test list"
    val taskName = "task name"
    val taskDescription = "task desc"
    val taskId = 1
    val recurrence = Recurrence.NEVER
    backingStorage.createTaskList(new TaskList(taskListName, Array(
      new Task(taskName, taskDescription, taskId, None.orNull, false, recurrence, false)),
      None.orNull))

    val newRecurrence = Recurrence.DAILY
    commandExecutor ! ModifyTask(taskId, taskListName, None, None, None, newRecurrence)
    expectMsgPF(maxWaitTime) {
      case t: UpdatedTaskList =>
        val updatedTask = t.list.tasks.find(_.id == taskId).get
        updatedTask.name shouldBe taskName
        updatedTask.description shouldBe taskDescription
        updatedTask.dueDate shouldBe None.orNull
        updatedTask.done shouldBe false
        updatedTask.recurrence shouldBe newRecurrence
      case unexpected => fail(s"It should have returned an UpdatedTaskList, instead it responded with $unexpected")
    }
  }

  it should "replicate a recurring daily task" in {
    val backingStorage = BackingStorage(InMemory)
    val commandExecutor = system.actorOf(CommandExecutor.props(backingStorage))

    val testListName = "test list"
    val taskId = 0xBEEF
    val dueDate = ZonedDateTime.now()
    val testTaskName = "test task"
    val testTask = new Task(
      testTaskName, "", taskId, dueDate, false, Recurrence.DAILY, false)

    backingStorage.createTaskList(new TaskList(testListName, Array.empty, None.orNull))
    backingStorage.replaceTaskList(testListName, new TaskList(testListName, Array(testTask), None.orNull))

    commandExecutor ! MarkDone(taskId, testListName)
    expectMsgPF(maxWaitTime) {
      case t: UpdatedTaskList =>
        t.list.tasks.length shouldBe 1
      case unexpected => fail(s"It should have returned the updated task list, instead it responded with $unexpected")
    }

    // A new task should be in the list with an updated due date
    eventually {
      commandExecutor ! GetTaskList(testListName, true)
      expectMsgPF(maxWaitTime) {
        case t: SingleList =>
          val recurringTask = t.list.tasks.find(_.name == testTaskName).get
          recurringTask.dueDate shouldBe dueDate.plusDays(1)

        case unexpected => fail(s"It should have returned the tasks, instead it responded with $unexpected")
      }
    }
  }

  it should "mark recurrent task as not done and the reminder as not sent" in {
    val backingStorage = BackingStorage(InMemory)
    val commandExecutor = system.actorOf(CommandExecutor.props(backingStorage))

    val testListName = "test list"
    val taskId = 0xBEEF
    val dueDate = ZonedDateTime.now()
    val testTaskName = "test task"
    val testTask = new Task(testTaskName, "", taskId, dueDate, false, Recurrence.DAILY, true)
    testTask.done shouldBe false

    backingStorage.createTaskList(new TaskList(testListName, Array.empty, None.orNull))
    backingStorage.replaceTaskList(testListName, new TaskList(testListName, Array(testTask), None.orNull))

    commandExecutor ! MarkDone(taskId, testListName)
    expectMsgPF(maxWaitTime) {
      case t: UpdatedTaskList =>
        t.list.tasks.length shouldBe 1
        t.list.tasks.head.done shouldBe true
      case unexpected => fail(s"It should have returned the updated task list, instead it responded with $unexpected")
    }

    // A new task should be in the list marked as not done and the reminder as not sent
    eventually {
      commandExecutor ! GetTaskList(testListName, true)
      expectMsgPF(maxWaitTime) {
        case t: SingleList =>
          val recurringTask = t.list.tasks.find(_.name == testTaskName).get
          recurringTask.done shouldBe false
          recurringTask.reminderSent shouldBe false

        case unexpected => fail(s"It should have returned the tasks, instead it responded with $unexpected")
      }
    }
  }

  it should "return the requested task list when the GetTaskList command is received" in {
    val backingStorage = BackingStorage(InMemory)
    val commandExecutor = system.actorOf(CommandExecutor.props(backingStorage))

    // Add tasklist to backing storage
    val testListName = "testlist"
    backingStorage.createTaskList(new TaskList(testListName, Array.empty, None.orNull))

    commandExecutor ! GetTaskList(testListName, true)
    expectMsgPF(maxWaitTime) {
      case t: SingleList =>
        t.list.name shouldBe testListName
      case unexpected => fail(s"It should have returned a TaskLists message, instead it responded with $unexpected")
    }
  }

  it should "return a ServerError when the GetTaskList command is received but the task list does not exist" in {
    val backingStorage = BackingStorage(InMemory)
    val commandExecutor = system.actorOf(CommandExecutor.props(backingStorage))

    commandExecutor ! GetTaskList("Non existent task list", true)
    expectMsgPF(maxWaitTime) {
      case Failure(ex) =>
        ex shouldBe a[StorageException]
      case unexpected => fail(s"It should have returned a Failure, instead it responded with $unexpected")
    }
  }
}
