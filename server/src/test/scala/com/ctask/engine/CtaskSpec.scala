package com.ctask.engine

import java.time.ZonedDateTime

import akka.actor.ActorSystem
import akka.pattern.ask
import akka.testkit.{EventFilter, ImplicitSender, TestKit, TestProbe}
import akka.util.Timeout
import com.ctask.data.Task.Recurrence
import com.ctask.data.{Task, TaskList}
import com.ctask.engine.Ctask.Terminate
import com.ctask.messages.ServerResponses.{AddedTaskList, ServerError}
import com.ctask.messages._
import com.ctask.storage.{BackingStorage, InMemory}
import com.ctask.utils.PortProvider
import com.typesafe.config.ConfigFactory
import org.scalatest.{BeforeAndAfterAll, FlatSpecLike, Matchers}
import org.scalatest.concurrent.Eventually._
import org.scalatest.concurrent.PatienceConfiguration

import scala.jdk.CollectionConverters._
import scala.concurrent.Await
import scala.concurrent.duration._

private object CtaskSpec {
  val configMap = Map("akka.loggers" -> List("akka.testkit.TestEventListener").asJava,
                      "akka.remote.netty.tcp.port" -> PortProvider.unique.toString).asJava

  val actorSysConfig = ConfigFactory.parseMap(configMap)
}

/**
  * Spec for Ctask.
  */
class CtaskSpec extends TestKit(ActorSystem("CtaskSpec", CtaskSpec.actorSysConfig.withFallback(ConfigFactory.load())))
  with FlatSpecLike with Matchers with BeforeAndAfterAll with ImplicitSender {

  val maxWaitTime = 100.millis

  override def afterAll: Unit = {
    TestKit.shutdownActorSystem(system)
  }

  behavior of "Ctask"

  object NotValidCommand

  it should "receive an InvalidMessage and log error when an unknown message is received" in {
    val probe = TestProbe()
    val ctask = system.actorOf(Ctask.props(probe.ref))
    val invalidCommand = NotValidCommand

    ctask ! invalidCommand

    expectMsgPF() {
      case e: ServerError => e.msg should startWith (s"Server could not recognize command: ")
    }
  }

  it should "send the command executor a proper command when a valid command line is received" in {
    val commandExecutorProbe = TestProbe()
    val ctask = system.actorOf(Ctask.props(commandExecutorProbe.ref))

    val listName = "testname"
    val addTaskListCommand = AddTaskList(listName, None)

    ctask ! addTaskListCommand

    eventually {
      commandExecutorProbe.expectMsg(AddTaskList(listName, None))
    }
  }

  it should "log warn when an unknown message is received" in {
    implicit val system = ActorSystem("CtaskSpec",
      ConfigFactory.parseString("""akka.loggers = ["akka.testkit.TestEventListener"]"""))

    val probe = TestProbe()
    val ctask = system.actorOf(Ctask.props(probe.ref))
    val unknownCommand = None

    EventFilter.warning(message = s"Received and ignored command: $unknownCommand",
      occurrences = 1) intercept {
      ctask ! unknownCommand
    }
    expectMsg(new ServerError("Server could not recognize command: None"))
  }

  it should "return the originator a valid Response if the command executor is successful" in {
    val commandExecutor = system.actorOf(CommandExecutor.props(BackingStorage(InMemory)))
    val ctask = system.actorOf(Ctask.props(commandExecutor))

    val listName = "testname"
    val addTaskListCommand = AddTaskList(listName, None)

    ctask ! addTaskListCommand
    expectMsg(new AddedTaskList(new TaskList(listName, Array.empty, None.orNull)))
  }

  it should "return a ServerError if the server fails with an error" in {
    val commandExecutor = system.actorOf(CommandExecutor.props(BackingStorage(InMemory)))
    val ctask = system.actorOf(Ctask.props(commandExecutor))

    val listName = "testname"
    val addTaskListCommand = AddTaskList(listName, None)

    // Add same list twice and expect an error.
    ctask ! addTaskListCommand
    expectMsg(new AddedTaskList(new TaskList(listName, Array.empty, None.orNull)))
    ctask ! addTaskListCommand
    expectMsgClass(classOf[ServerError])
  }

  it should "periodically send a message to check expired tasks" in {
    implicit val timeout = Timeout(5 seconds)

    val reminderProperty = "com.ctask.reminder.intervalSec"
    val originalPropValue = Option(System.getProperty(reminderProperty))
    System.setProperty(reminderProperty, "1")

    val commandExecutor = system.actorOf(CommandExecutor.props(BackingStorage(InMemory)))
    val ctask = system.actorOf(Ctask.props(commandExecutor, CountingDueDateHandler))

    val listName = "testList"
    val addTaskListCommand = AddTaskList(listName, Some("test@email.com"))
    Await.ready(ctask ? addTaskListCommand, 10 seconds)

    val addExpiredTaskCommand = AddTaskToTaskList("testTask", listName, Some("desc"), Some(ZonedDateTime.now.minusDays(1)), Recurrence.NEVER)
    Await.ready(ctask ? addExpiredTaskCommand, 10 seconds)

    eventually(PatienceConfiguration.Timeout(10 seconds)) {
      CountingDueDateHandler.counter should be > 0
    }

    if(originalPropValue.isEmpty) System.clearProperty(reminderProperty) else System.setProperty(reminderProperty, originalPropValue.get)
  }

  // TODO: review this test since it terminates the actor system and hence it must be the last of
  //       the spec.
  it should "shut down the command executor as well as itself when a Terminate message is received" in {
    val ctaskProbe = TestProbe()
    val commandExecutorProbe = TestProbe()
    val commandExecutor = system.actorOf(CommandExecutor.props(BackingStorage(InMemory)))
    val ctask = system.actorOf(Ctask.props(commandExecutor))

    commandExecutorProbe watch commandExecutor
    ctaskProbe watch ctask

    ctask ! Terminate

    commandExecutorProbe.expectTerminated(commandExecutor)
    ctaskProbe.expectTerminated(ctask)
  }

  object CountingDueDateHandler extends ExpiredDueDateHandler {
    var counter = 0

    override def onDueDateExpired(task: Task, taskList: TaskList): Unit = counter += 1
  }
}
