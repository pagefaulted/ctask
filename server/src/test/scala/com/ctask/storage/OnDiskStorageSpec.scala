package com.ctask.storage

import java.nio.file.{Files, Paths}

import com.ctask.utils.FileUtils
import org.scalatest.BeforeAndAfterEach

import scala.util.Failure

/**
  * Test suite for on disk storage.
  */
class OnDiskStorageSpec extends StorageSpec with BeforeAndAfterEach {

  private val testRoot = "/tmp/" + System.nanoTime()

  override def afterEach(): Unit = {
    FileUtils.rmDir(testRoot)
  }

  behavior of "OnDiskStorage"

  it should "create an on disk backing storage" in {
    val onDiskBackingStorage = BackingStorage(OnDisk(testRoot))
    onDiskBackingStorage shouldBe a[OnDiskStorage]
  }

  it should "fail creating the on disk folder if the provided root is not accessible" in {
    a[RuntimeException] should be thrownBy BackingStorage(OnDisk(s"$testRoot/invalid/folder"))
  }

  it should "fail to deserialize a tasklist containing invalid json" in {
    val onDiskBackingStorage = BackingStorage(OnDisk(testRoot))
    val invalidJson = "{name : \"test\"}"
    val testTaskList = "testList"

    val path = Paths.get(s"$testRoot/$testTaskList")
    Files.write(path, invalidJson.getBytes)

    onDiskBackingStorage.loadTaskList(testTaskList) shouldBe a[Failure[_]]
  }

  override def getBackingStorage: BackingStorage = new OnDiskStorage(testRoot)
}
