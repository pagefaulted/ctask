package com.ctask.storage

import com.ctask.data.Task.Recurrence
import com.ctask.data.{Task, TaskList}
import org.scalatest.{FlatSpec, Matchers}

import scala.util.{Failure, Success}

/**
  * Common abstract class for the storage specs implementations.
  */
abstract class StorageSpec extends FlatSpec with Matchers {

  def getBackingStorage: BackingStorage

  behavior of "Storage"

  it should "add a new task list to the storage" in {
    val backingStorage = getBackingStorage
    val newTaskListName = "new test tasklist"
    val addedTaskListTry = backingStorage.createTaskList(new TaskList(newTaskListName, Array.empty, None.orNull))

    addedTaskListTry match {
      case Success(addedTaskList) => addedTaskList.name shouldBe newTaskListName
      case Failure(ex) => fail(s"Unexpected exception: $ex")
    }
  }

  it should "trim task list names when creating a new task list" in {
    val backingStorage = getBackingStorage
    val newTaskListName = "new test tasklist    "
    val newTaskListTry = backingStorage.createTaskList(new TaskList(newTaskListName, Array.empty, None.orNull))
    val newTaskList = newTaskListTry.get

    newTaskList.name shouldBe newTaskListName.trim
  }

  it should "generate an error when adding a task list with an already existing name" in {
    val backingStorage = getBackingStorage
    val newTaskListName = "new test tasklist"
    backingStorage.createTaskList(new TaskList(newTaskListName, Array.empty, None.orNull))
    val alreadyExistingTaskTry = backingStorage.createTaskList(new TaskList(newTaskListName, Array.empty, None.orNull))

    alreadyExistingTaskTry match {
      case Failure(ex) => ex shouldBe a[StorageException]
      case Success(_) => fail(s"Should have failed since the $newTaskListName list " +
        "already exists")
    }
  }

  it should "generate an error when adding a task list with an empty name" in {
    val backingStorage = getBackingStorage
    val newTaskListName = ""
    val addedTaskListTry = backingStorage.createTaskList(new TaskList(newTaskListName, Array.empty, None.orNull))

    addedTaskListTry match {
      case Failure(ex) => ex shouldBe a[StorageException]
      case Success(_) => fail(s"Should have failed since the task list name is empty")
    }
  }

  it should "generate an error when adding a task list with a name that contains only spaces" in {
    val backingStorage = getBackingStorage
    val newTaskListName = "   "
    val addedTaskListTry = backingStorage.createTaskList(new TaskList(newTaskListName, Array.empty, None.orNull))

    addedTaskListTry match {
      case Failure(ex) => ex shouldBe a[StorageException]
      case Success(_) => fail(s"Should have failed since the task list name is empty")
    }
  }

  it should "remove an existing task list" in {
    val backingStorage = getBackingStorage
    val newTaskListName = "new test tasklist"

    val addedTaskListTry = backingStorage.createTaskList(new TaskList(newTaskListName, Array.empty, None.orNull))
    val removedTaskListTry = backingStorage.deleteTaskList(newTaskListName)

    addedTaskListTry match {
      case Success(addedTaskList) =>
        removedTaskListTry match {
          case Success(removedTaskList) =>
            addedTaskList shouldEqual removedTaskList
          case Failure(ex) => fail(s"Unexpected exception $ex")
        }
      case Failure(ex) => fail(s"Unexpected exception $ex")
    }

    backingStorage.loadTaskList(newTaskListName) match {
      case Success(_) => fail(s"Task list $newTaskListName was not removed")
      case Failure(ex) => ex shouldBe a[StorageException]
    }
  }

  it should "trim task list names when removing a task list" in {
    val backingStorage = getBackingStorage
    val trimmedTaskListName = "new test tasklist"
    backingStorage.createTaskList(new TaskList(trimmedTaskListName, Array.empty, None.orNull))

    val nonTrimmedTaskListName = trimmedTaskListName + "    "
    val removedTaskListTry = backingStorage.deleteTaskList(nonTrimmedTaskListName)

    removedTaskListTry match {
      case Success(removedTaskList) => removedTaskList.name shouldBe trimmedTaskListName
      case Failure(ex) => fail(s"Unexpected exception $ex")
    }
  }

  it should "generate an error when trying to remove a task list that does not exist" in {
    val backingStorage = getBackingStorage
    val nonExistentTaskListName = "i do not exist"

    val removedTaskListTry = backingStorage.deleteTaskList(nonExistentTaskListName)

    removedTaskListTry match {
      case Failure(ex) => ex shouldBe a[StorageException]
      case Success(_) => fail("Should have failed since a task list with the provided" +
        "name does not exist.")
    }
  }

  it should "load a task list from storage" in {
    val backingStorage = getBackingStorage
    val newTaskListName = "new test tasklist"
    val newTaskListTry = backingStorage.createTaskList(new TaskList(newTaskListName, Array.empty, None.orNull))
    val newTaskList = newTaskListTry.get

    val loadedTaskListTry = backingStorage.loadTaskList(newTaskListName)

    loadedTaskListTry match {
      case Success(loadedTaskList) => loadedTaskList shouldEqual newTaskList
      case Failure(ex) => fail(s"Unexpected exception: $ex")
    }
  }

  it should "generate an error when trying to load a non-existent task list" in {
    val backingStorage = getBackingStorage
    val nonExistentTaskListName = "i do not exist"
    val loadedTaskListTry = backingStorage.loadTaskList(nonExistentTaskListName)

    loadedTaskListTry match {
      case Failure(ex) => ex shouldBe a[StorageException]
      case Success(_) => fail("Should have failed since the task list does not exist.")
    }
  }

  it should "trim task list names when loading a task list" in {
    val backingStorage = getBackingStorage
    val trimmedTaskListName = "new test tasklist"
    backingStorage.createTaskList(new TaskList(trimmedTaskListName, Array.empty, None.orNull))

    val nonTrimmedTaskListName = trimmedTaskListName + "    "
    val loadedTaskListTry = backingStorage.loadTaskList(nonTrimmedTaskListName)

    loadedTaskListTry match {
      case Success(loadedTaskList) => loadedTaskList.name shouldBe trimmedTaskListName
      case Failure(ex) => fail(s"Unexpected exception: $ex")
    }
  }

  it should "load all tasklists names" in {
    val backingStorage = getBackingStorage
    val newTaskListNames = "new test tasklist1" :: "new test tasklist2" :: Nil
    newTaskListNames.foreach { newListName =>
      backingStorage.createTaskList(new TaskList(newListName, Array.empty, None.orNull))
    }

    val taskListsNames = backingStorage.loadTaskListNames

    taskListsNames.size shouldBe newTaskListNames.size

    taskListsNames.foreach { loadedListName =>
      newTaskListNames contains loadedListName shouldBe true
    }
  }

  it should "replace an existing task list" in {
    val backingStorage = getBackingStorage
    val newTaskListName = "new test tasklist"
    val addedTaskListTry = backingStorage.createTaskList(new TaskList(newTaskListName, Array.empty, None.orNull))

    addedTaskListTry match {
      case Success(addedTaskList) => addedTaskList.name shouldBe newTaskListName
      case Failure(ex) => fail(s"Unexpected exception: $ex")
    }

    val replacementTaskList = new TaskList(newTaskListName, Array(
      new Task("taskName", "", 1L, None.orNull, false, Recurrence.NEVER, false)),
      None.orNull)

    backingStorage.replaceTaskList(newTaskListName, replacementTaskList) match {
      case Success(replacedTaskList) => replacedTaskList shouldBe replacementTaskList
      case Failure(ex) => fail(s"Unexpected exception: $ex")
    }
  }

  it should "fail to replace a non-existing task list" in {
    val backingStorage = getBackingStorage
    val replacementTaskList = new TaskList("replacement task list", Array.empty, None.orNull)

    backingStorage.replaceTaskList(replacementTaskList.name, replacementTaskList) match {
      case Success(_) => fail("Should have failed to replace a non-existing task list.")
      case Failure(ex) => ex shouldBe a[StorageException]
    }
  }

  it should "remove a task from a tasklist" in {
    // Add tasklist and add task to it
    val backingStorage = getBackingStorage
    val newTaskListName = "zerg"
    backingStorage.createTaskList(new TaskList(newTaskListName, Array.empty, None.orNull))
    val task = new Task("kill protoss", "", 1L, None.orNull, false, Recurrence.NEVER, false)
    backingStorage.replaceTaskList(newTaskListName, new TaskList(newTaskListName, Array(task), None.orNull)) should not be a[Failure[_]]

    // remote task from tasklist
    backingStorage.removeTaskFromTaskList(task.id, newTaskListName).get shouldBe task
  }

  it should "fail to remove a task from a non existing list" in {
    val backingStorage = getBackingStorage
    val nonExistingTaskListName = "i do not exist"
    val taskId = 1

    try {
      backingStorage.removeTaskFromTaskList(taskId, nonExistingTaskListName).get
      fail("Should have thrown a StorageException but did not throw.")
    }
    catch {
      case se: StorageException =>
        se.message shouldBe s"The task with id $taskId cannot be removed because list $nonExistingTaskListName does not exist"
      case ex: Throwable =>
        fail(s"Should have throws a StorageException but $ex was thrown instead")
    }
  }

  it should "fail to remove a non existing task" in {
    val backingStorage = getBackingStorage
    val newTaskListName = "zerg"
    backingStorage.createTaskList(new TaskList(newTaskListName, Array.empty, None.orNull))
    val nonExistingTaskId = 1

    try {
      backingStorage.removeTaskFromTaskList(nonExistingTaskId, newTaskListName).get
      fail("Should have thrown a StorageException but did not throw.")
    }
    catch {
      case se: StorageException =>
        se.message shouldBe s"Could not find task $nonExistingTaskId in list $newTaskListName"
      case ex: Throwable =>
        fail(s"Should have throws a StorageException but $ex was thrown instead")
    }
  }

  it should "load all task lists" in {
    val backingStorage = getBackingStorage
    val taskList1 = "taskList1"
    val taskList2 = "taskList2"
    val taskList3 = "taskList3"
    backingStorage.createTaskList(new TaskList(taskList1, Array.empty, None.orNull))
    backingStorage.createTaskList(new TaskList(taskList2, Array.empty, None.orNull))
    backingStorage.createTaskList(new TaskList(taskList3, Array.empty, None.orNull))

    backingStorage.loadAllTaskLists.size shouldBe 3
  }

  it should "return an empty task lists result when there are no task lists to load" in {
    val backingStorage = getBackingStorage
    backingStorage.loadAllTaskLists.size shouldBe 0
  }
}
