package com.ctask.storage

/**
  * Test suite for the InMemoryStorage.
  */
class InMemoryStorageSpec extends StorageSpec {

  behavior of "InMemoryStorage"

  it should "create in memory backing storage" in {
    val inMemoryBackingStorage = BackingStorage(InMemory)
    inMemoryBackingStorage shouldBe a[InMemoryStorage]
  }

  override def getBackingStorage: BackingStorage = new InMemoryStorage
}
