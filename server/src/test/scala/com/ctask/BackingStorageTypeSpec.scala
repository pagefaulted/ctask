package com.ctask

import com.ctask.storage.{InMemory, OnDisk}
import org.scalatest.{FlatSpec, Matchers}

class BackingStorageTypeSpec extends FlatSpec with Matchers {

  behavior of "BackingStorageType"

  it should "return InMemory if the memory property is provided" in {
    BackingStorageType("memory") shouldBe InMemory
  }

  it should "return InMemory if an invalid property is provided" in {
    BackingStorageType("invalid property") shouldBe InMemory
  }

  it should "return the default backing storage if no property is provided" in {
    BackingStorageType("") shouldBe InMemory
  }

  it should "return OnDisk if the disk property is provided" in {
    BackingStorageType("disk") shouldBe an[OnDisk]
  }
}
