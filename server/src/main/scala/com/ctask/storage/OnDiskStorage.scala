package com.ctask.storage

import java.io.File
import java.nio.file.{Files, Paths, StandardOpenOption}

import com.ctask.data.{Task, TaskList, TaskListJsonUtils}
import com.ctask.utils.FileUtils
import play.api.libs.json.Json

import scala.util.{Failure, Success, Try}

/**
  * On disk storage for tasks and task lists.
  */
final class OnDiskStorage(root: String) extends BackingStorage {

  // Root of the on-disk storage.
  private val rootFile = init()

  override def doLoadTaskList(name: String): Try[TaskList] = deserializeTaskList(name)

  override def doCreateTaskList(taskList: TaskList): Try[TaskList] = serializeTaskList(taskList)

  override def doDeleteTaskList(name: String): Try[TaskList] = {
    val deletedTaskList = doLoadTaskList(name)
    deletedTaskList match {
      case Success(taskList) =>
        Try {
          new File(rootFile, name).delete()
          taskList
        }
      case Failure(ex) =>
        // TODO: log failure
        Failure(ex)
    }
  }

  override def doInsertTaskList(taskList: TaskList): Try[TaskList] = serializeTaskList(taskList)

  override def doRemoveTaskFromTaskList(taskId: Long, taskListName: String): Try[Task] = {
    val taskList = doLoadTaskList(taskListName)
    taskList match {
      case Success(taskList) =>
        val newTaskList = new TaskList(taskList.name, taskList.tasks.filter(_.id != taskId), taskList.email)
        doInsertTaskList(newTaskList)
        taskList.tasks.find(_.id == taskId) match {
          case Some(task) => Success(task)
          case None => Failure(new StorageException(s"Could not find task $taskId in list $taskListName"))
        }
      case Failure(ex) => Failure(ex)
    }
  }

  override def loadAllTaskLists: List[TaskList] = {
    loadTaskListNames.map { taskListName =>
      deserializeTaskList(taskListName).getOrElse(new TaskList(s"$taskListName was INVALID", Array.empty, None.orNull))
    }
  }

  private def init(): File = {
    val rootFile = new File(root)
    if (!rootFile.exists()) {
      FileUtils.mkdir(rootFile.getCanonicalPath) match {
        case Success(_) => // TODO: consider adding log
        case Failure(ex) => throw ex
      }
    }
    rootFile
  }

  override def taskListExists(name: String): Boolean = new File(rootFile, name).exists()

  override def loadTaskListNames: List[String] = new File(root).list().toList

  private def serializeTaskList(taskList: TaskList): Try[TaskList] = {
    Try {
      val jsValue = Json.toJson(taskList)(TaskListJsonUtils.taskListWrites)
      val path = Paths.get(s"${rootFile.getAbsolutePath}/${taskList.name}")
      Files.write(path, jsValue.toString().getBytes(), StandardOpenOption.CREATE, StandardOpenOption.TRUNCATE_EXISTING)
      taskList
    }
  }

  private def deserializeTaskList(name: String): Try[TaskList] = {
    Try {
      val path = Paths.get(s"${rootFile.getAbsolutePath}/$name")
      val jsonString = String.join("", Files.readAllLines(path))
      Json.parse(jsonString).validate[TaskList](TaskListJsonUtils.taskListJsonRead).get
    }
  }
}
