package com.ctask.storage


import com.ctask.ServerProperties
import com.ctask.data.{Task, TaskList}

import scala.util.{Failure, Success, Try}

sealed trait StorageType
case object InMemory extends StorageType
case class OnDisk(rootFolder: String = ServerProperties.rootCtaskStorageFolder) extends StorageType

/**
  * Factory object for backing storage.
  */
object BackingStorage {

  /**
    * Returns a backing storage, as specified by the provided storage type.
    *
    * @param storageType the storage type of the backing storage.
    * @return an instance of the specific storage type requested.
    */
  def apply(storageType: StorageType): BackingStorage = {
    storageType match {
      case InMemory => new InMemoryStorage
      case OnDisk(rootFolder) =>
        new OnDiskStorage(rootFolder)
    }
  }
}

/**
  * Backing storage trait.
  */
trait BackingStorage {

  /**
    * Loads the task list with the provided name.
    *
    * @param name the name of the task list to load.
    * @return a Try wrapping the requested task list or a failure wrapping the exception.
    */
  def loadTaskList(name: String): Try[TaskList] = this.synchronized {
    val trimmedName = name.trim

    if(!taskListExists(trimmedName)) {
      Failure(new StorageException(s"Task list $trimmedName does not exist"))
    } else {
      doLoadTaskList(trimmedName)
    }
  }

  /**
    * Creates a new task list with the provided name.
    *
    * @param taskList the task list to create
    * @return a Try wrapping the newly created task list, or a Failure wrapping an exception.
    */
  def createTaskList(taskList: TaskList): Try[TaskList] = this.synchronized {
    if(taskList.name.isEmpty) {
      Failure(new StorageException(s"Task list name cannot be empty"))
    } else if(taskListExists(taskList.name)) {
      Failure(new StorageException(s"Task list ${taskList.name} already exists"))
    } else {
      doCreateTaskList(taskList)
    }
  }

  /**
    * Deletes the task list with the provided name.
    *
    * @param name the name of the task list to be deleted.
    * @return a Try wrapping the deleted task list, or a Failure wrapping an exception.
    */
  def deleteTaskList(name: String): Try[TaskList] = this.synchronized {
    val trimmedName = name.trim

    if(!taskListExists(trimmedName)) {
      Failure(StorageException(s"The task list $trimmedName does not exist"))
    } else {
      doDeleteTaskList(trimmedName)
    }
  }

  /**
    * Replaces an existing task list with the provided one.
    * @param taskList the new task list that will replace the existing one.
    * @return the new task list.
    */
  def replaceTaskList(taskListName: String, taskList: TaskList): Try[TaskList] = this.synchronized {
    if(!taskListExists(taskListName)) {
      Failure(StorageException(s"The task list ${taskList.name} cannot be replaced because it does not exist"))
    } else {
      deleteTaskList(taskListName) match {
        case Success(_) =>
          doInsertTaskList(taskList)
        case Failure(ex) => Failure(ex)
      }
    }
  }

  /**
    * Removes the task from the task list, if it exists.
    *
    * @param taskId       the id of the task to remove.
    * @param taskListName the name of the task list to remove the task from.
    * @return the removed task, wrapped in a Try.
    */
  def removeTaskFromTaskList(taskId: Long, taskListName: String): Try[Task] = this.synchronized {
    // TODO: use getOrElse on option
    if(!taskListExists(taskListName)){
      Failure(StorageException(s"The task with id $taskId cannot be removed because list ${taskListName} does not exist"))
    } else {
      doRemoveTaskFromTaskList(taskId, taskListName)
    }
  }

  /**
    * Loads all the task lists names.
    * @return a list containing the task list names.
    */
  def loadTaskListNames: List[String]

  /**
    * Loads all tasks lists.
    * @return all the task lists
    */
  def loadAllTaskLists: List[TaskList]

  /**
    * Specific implementation of the task list loading.
    * @param name the task list name.
    * @return the corresponding task list.
    */
  protected def doLoadTaskList(name: String): Try[TaskList]

  /**
    * Specific implementation of the task list creation.
    * @param taskList the task list to create.
    * @return the newly created task list.
    */
  protected def doCreateTaskList(taskList: TaskList): Try[TaskList]

  /**
    * Specific implementation of the task list deletion.
    * @param name the name of task list to delete.
    * @return the deleted task list.
    */
  protected def doDeleteTaskList(name: String): Try[TaskList]

  /**
    * Specific implementation to insert a task list.
    * @param taskList the task list to be inserted.
    * @return the inserted task list.
    */
  protected def doInsertTaskList(taskList: TaskList): Try[TaskList]

  /**
    * Specific implementation to remove a task from a tasklist.
    *
    * @param taskId       the id of the task to remove.
    * @param taskListName the name of the task list to remove the task from.
    * @return a Try, with the removed task or a Failure.
    */
  protected def doRemoveTaskFromTaskList(taskId: Long, taskListName: String): Try[Task]

  /**
    * Checks if the task list exists.
    * @param name the task list name.
    * @return true if the task list exists.
    */
  protected def taskListExists(name: String): Boolean
}
