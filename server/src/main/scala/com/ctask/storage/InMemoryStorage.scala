package com.ctask.storage

import com.ctask.data.{Task, TaskList}

import scala.util.{Failure, Success, Try}

/**
  * In-memory backing storage for tasks and task lists.
  */
final class InMemoryStorage extends BackingStorage {

  private var taskLists: Map[String, TaskList] = Map.empty

  override def doLoadTaskList(name: String): Try[TaskList] = Try(taskLists(name))

  override def doCreateTaskList(taskList: TaskList): Try[TaskList] = {
    Try {
      taskLists += (taskList.name -> taskList)
      taskList
    }
  }

  override def doDeleteTaskList(name: String): Try[TaskList] = {
    Try {
      val removedTaskList = taskLists(name)
      taskLists -= name
      removedTaskList
    }
  }

  override def doInsertTaskList(taskList: TaskList): Try[TaskList] = {
    Try {
      taskLists += (taskList.name -> taskList)
      taskList
    }
  }

  override def doRemoveTaskFromTaskList(taskId: Long, taskListName: String): Try[Task] = {
    taskLists.get(taskListName).map { taskList =>
      val removedTask = taskList.tasks.find(_.id == taskId)
      removedTask match {
        case Some(task) =>
          // TODO: this doesnt remove anything
          new TaskList(taskList.name, taskList.tasks.filter(_.id == taskId), taskList.email)
          Success(task)
        case None => Failure(new StorageException(s"Could not find task $taskId in list $taskListName"))
      }
    } match {
      case Some(result) => result
      case None => Failure(new StorageException(s"Could not find task list $taskListName"))
    }
  }

  override def taskListExists(name: String): Boolean = taskLists.contains(name)

  override def loadTaskListNames: List[String] = taskLists.keys.toList

  override def loadAllTaskLists: List[TaskList] = taskLists.values.toList
}
