package com.ctask.server

import akka.actor.{ActorRef, ActorSystem}
import com.ctask.ServerProperties
import com.ctask.engine.{CommandExecutor, Ctask}
import com.ctask.http.server.HttpService
import com.ctask.http.util.HttpProperties
import com.ctask.storage.BackingStorage
import com.ctask.utils.MailReminder

object CtaskServer extends HttpService {

  override implicit val httpSystem: ActorSystem = ActorSystem("CtaskHttpSystem", HttpProperties.config)
  override val serviceActor: ActorRef = {
    val cfg = ServerProperties.config
    val system = ActorSystem("CtaskServerSystem", cfg)
    val commandExecutor = system.actorOf(CommandExecutor.props(BackingStorage(ServerProperties.backingStorageType)))
    system.actorOf(Ctask.props(commandExecutor, MailReminder), "CtaskServer")
  }

  def main(args: Array[String]): Unit = startHttpService()
}
