package com.ctask

import com.ctask.storage.{InMemory, OnDisk, StorageType}
import com.ctask.utils.PropertiesRetriever
import com.typesafe.config.ConfigFactory

/**
  * Properties for Ctask server.
  */
object ServerProperties extends PropertiesRetriever {

  val config = ConfigFactory.load()

  /**
    * Server root folder for disk storage.
    */
  val rootCtaskStorageFolderProp: String = "com.ctask.storage.root"
  val rootCtaskStorageFolderDefault: String = System.getProperty("user.home") + "/.ctask"
  def rootCtaskStorageFolder: String = getPropertyStr(rootCtaskStorageFolderProp, rootCtaskStorageFolderDefault)

  /**
    * Server backing storage, either in memory or on disk.
    */
  val backingStorageTypeProp: String = "com.ctask.storage.type"
  val backingStorageTypeDefault: String = BackingStorageType.inMemoryStr
  def backingStorageType: StorageType = BackingStorageType(getPropertyStr(backingStorageTypeProp, backingStorageTypeDefault))

  /**
    * Email reminder properties.
    */
  def reminderMailHost: String = getPropertyStr("com.ctask.reminder.emailSmtpServer", "")
  def reminderMailPort: Int = getPropertyInt("com.ctask.reminder.emailPort", 0)
  def reminderMailUser: String = getPropertyStr("com.ctask.reminder.emailUser", "")
  def reminderMailPwd: String = getPropertyStr("com.ctask.reminder.emailPwd", "")

  /**
    * Email reminder interval property. Default is 60 seconds.
    * @return the email reminder interval, in seconds
    */
  def reminderIntervalSec: Int = getPropertyInt("com.ctask.reminder.intervalSec", 60)
}

/**
  * Translate the backing storage property into the relative type.
  */
object BackingStorageType {

  val inMemoryStr = "memory"
  val onDiskStr = "disk"

  def apply(storageTypeString: String): StorageType = {
    storageTypeString match {
      case mem if mem == inMemoryStr => InMemory
      case disk if disk == onDiskStr => OnDisk(ServerProperties.rootCtaskStorageFolder)
      case _ => BackingStorageType(ServerProperties.backingStorageTypeDefault)
    }
  }
}
