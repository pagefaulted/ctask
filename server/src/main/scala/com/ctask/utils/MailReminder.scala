package com.ctask.utils

import java.time.format.DateTimeFormatter

import javax.mail.{Message, Session}
import javax.mail.internet.{InternetAddress, MimeMessage}
import com.ctask.ServerProperties
import com.ctask.data.{Task, TaskList}
import com.ctask.engine.ExpiredDueDateHandler

object MailReminder extends ExpiredDueDateHandler {

  def send(subject: String, body: String, recipient: String, mailHost: String, mailPort: Int, mailUser: String, mailPwd: String): Unit = {
    try {
      val properties = System.getProperties

      properties.put("mail.smtp.starttls.enable", "true")

      val session = Session.getDefaultInstance(properties)
      val message = new MimeMessage(session)
      message.setFrom(new InternetAddress("Ctask"))
      message.addRecipient(Message.RecipientType.TO, new InternetAddress(recipient))
      message.setSubject(subject)
      message.setText(body)

      val transport = session.getTransport("smtp")
      transport.connect(mailHost, mailPort, mailUser, mailPwd)
      transport.sendMessage(message, message.getAllRecipients)
    } catch {
      case t: Throwable =>
        t.printStackTrace()
        println(s"Error sending email to $recipient")
    }
  }

  /**
    * This callback sends an email reminder to the owner of the task list the expired task belongs to.
    *
    * @param task     the task whose due date has expired
    * @param taskList the task list that owns the task
    */
  override def onDueDateExpired(task: Task, taskList: TaskList): Unit = {
    Option(taskList.email).foreach { recipient =>
      val dueDate = Option(task.dueDate).map(_.format(DateTimeFormatter.ofPattern("HH:mm dd/MMM/yyyy VV"))).getOrElse("No due date")
      val subject = s"Ctask reminder: ${task.name}, due on $dueDate"
      val body = s"Task ${task.name} was due on $dueDate.\n" +
        s"Description: ${task.description}"

      send(subject, body, recipient,
        ServerProperties.reminderMailHost,
        ServerProperties.reminderMailPort,
        ServerProperties.reminderMailUser,
        ServerProperties.reminderMailPwd)
    }
  }
}
