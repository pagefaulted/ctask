package com.ctask.engine

import java.time.ZonedDateTime

import akka.actor.{Actor, ActorLogging, Props}
import com.ctask.data.Task.Recurrence
import com.ctask.data._
import com.ctask.messages.ServerResponses.{AddedTaskList, MarkedReminderSent, RemovedTask, RemovedTaskList, ServerError, SingleList, TaskLists, UpdatedTaskList}
import com.ctask.messages._
import com.ctask.storage.BackingStorage

import scala.jdk.CollectionConverters._
import scala.util.{Failure, Success}

/**
  * Companion object for the CommandExecutor actor.
  */
object CommandExecutor {

  /**
    * Props factory for the CommandExecutor.
    *
    * @return a Props for the CommandExecutor actor.
    */
  def props(backingStorage: BackingStorage): Props = Props(new CommandExecutor(backingStorage))
}

/**
  * Actor responsible for the execution of commands.
  */
class CommandExecutor(backingStorage: BackingStorage) extends Actor with ActorLogging {

  override def receive: Receive = handleMessages orElse logIgnore

  //scalastyle:off
  private def handleMessages: Receive = {
    case ShowTaskLists =>
      showTaskLists()
    case AddTaskList(name, emailOpt) =>
      addTaskList(name, emailOpt)
    case RemoveTaskList(name) =>
      removeTaskList(name)
    case AddTaskToTaskList(name, taskListName, description, dueDate, recurrence) =>
      addTaskToTaskList(name, taskListName, description, dueDate, recurrence)
    case RemoveTaskFromTaskList(taskId, taskListName) =>
      removeTaskFromTaskList(taskId, taskListName)
    case MarkDone(taskId, taskListName) =>
      markTaskAsDone(taskId, taskListName)
      handleRecurrence(taskId, taskListName)
    case ModifyTaskList(taskListName, newName, newEmail) =>
      modifyTaskList(taskListName, newName, newEmail)
    case ModifyTask(taskId, taskListName, newTaskNameOpt, newDescriptionOpt, newDueDateOpt, recurrence) =>
      modifyTask(taskId, taskListName, newTaskNameOpt, newDescriptionOpt, newDueDateOpt, recurrence)
    case MarkReminderSent(taskId, taskListName) =>
      markReminderSent(taskId, taskListName)
    case GetTaskList(taskListName, withCompleted) =>
      getTaskList(taskListName, withCompleted)
  }
  //scalastyle:on

  private def getTaskList(taskListName: String, withCompleted: Boolean): Unit = {
    backingStorage.loadTaskList(taskListName) match {
      case Failure(ex) =>
        sender ! Failure(ex)
      case Success(taskList) =>
        val list = if (withCompleted) {
          taskList
        } else {
          new TaskList(taskList.name, taskList.tasks.filterNot(_.done), taskList.email)
        }
        sender ! new SingleList(list)
    }
  }

  private def showTaskLists(): Unit = {
    sender ! new TaskLists(backingStorage.loadAllTaskLists.asJava)
  }

  private def addTaskList(name: String, emailOpt: Option[String]): Unit = {
    backingStorage.createTaskList(new TaskList(name, Array.empty, emailOpt.orNull)) match {
      case Success(taskList) =>
        sender ! new AddedTaskList(taskList)
      case Failure(ex) =>
        sender ! Failure(ex)
    }
  }

  private def removeTaskList(name: String): Unit = {
    backingStorage.deleteTaskList(name) match {
      case Success(taskList) =>
        sender ! new RemovedTaskList(taskList)
      case Failure(ex) =>
        sender ! Failure(ex)
    }
  }

  private def addTaskToTaskList(name: String,
                                taskListName: String,
                                desc: Option[String],
                                dueDate: Option[ZonedDateTime],
                                recurrence: Recurrence): Unit = {

    backingStorage.loadTaskList(taskListName) match {
      case Failure(ex) =>
        sender ! Failure(ex)
      case Success(taskList) =>
        val newTasks = taskList.tasks :+
          new Task(name, desc.getOrElse(""), generateTaskId(taskList), dueDate.orNull, false, recurrence, false)

        val updatedTaskList = new TaskList(taskList.name, newTasks, taskList.email)
        backingStorage.replaceTaskList(taskListName, updatedTaskList) match {
          case Success(insertedTaskList) =>
            sender ! new UpdatedTaskList(insertedTaskList)
          case Failure(ex) =>
            sender ! Failure(ex)
        }
    }
  }

  private def generateTaskId(taskList: TaskList): Long = {
    val usedIds = taskList.tasks.map(_.id)
    var nextAvailableId = 1
    while (usedIds.contains(nextAvailableId)) {
      nextAvailableId += 1
    }
    nextAvailableId
  }

  private def removeTaskFromTaskList(taskId: Long, taskListName: String): Unit = {
    backingStorage.removeTaskFromTaskList(taskId, taskListName) match {
      case Success(removedTask) =>
        sender ! new RemovedTask(removedTask)
      case Failure(ex) =>
        sender ! Failure(ex)
    }
  }

  private def markTaskAsDone(taskId: Long, taskListName: String): Unit = {
    backingStorage.loadTaskList(taskListName) match {
      case Failure(ex) =>
        sender ! Failure(ex)
      case Success(taskList) =>
        taskList.tasks.find(_.id == taskId) match {
          case Some(task) =>
            val doneTask = new Task(task.name, task.description, task.id, task.dueDate, true, task.recurrence, task.reminderSent)
            val newTasks = taskList.tasks.filter(_.id != taskId) :+ doneTask
            val updatedTaskList = new TaskList(taskList.name, newTasks, taskList.email)

            backingStorage.replaceTaskList(taskListName, updatedTaskList) match {
              case Success(insertedTaskList) =>
                sender ! new UpdatedTaskList(insertedTaskList)
              case Failure(ex) =>
                sender ! Failure(ex)
            }
          case None =>
            sender ! Failure(UnknownTaskException(taskId, taskListName))
        }
    }
  }

  private def markReminderSent(taskId: Long, taskListName: String): Unit = {
    backingStorage.loadTaskList(taskListName) match {
      case Failure(ex) =>
        sender ! Failure(ex)
      case Success(taskList) =>
        taskList.tasks.find(_.id == taskId) match {
          case Some(task) =>
            val taskWithReminderSent = new Task(
              task.name, task.description, task.id, task.dueDate, task.done, task.recurrence, true)

            val newTasks = taskList.tasks.filter(_.id != taskId) :+ taskWithReminderSent
            val updatedTaskList = new TaskList(taskList.name, newTasks, taskList.email)

            backingStorage.replaceTaskList(taskListName, updatedTaskList) match {
              case Success(insertedTaskList) =>
                sender ! new MarkedReminderSent(taskWithReminderSent, insertedTaskList.name)
              case Failure(ex) =>
                sender ! Failure(ex)
            }
          case None =>
            sender ! Failure(UnknownTaskException(taskId, taskListName))
        }
    }
  }

  private def handleRecurrence(taskId: Long, taskListName: String): Unit = {
    backingStorage.loadTaskList(taskListName) match {
      case Failure(ex) =>
        println(s"Could not handle recurrence for task $taskId in list $taskListName: ${ex.getMessage}")
      case Success(taskList) =>
        taskList.tasks.find(_.id == taskId) match {
          case Some(task) =>
            if (task.recurrence != Recurrence.NEVER) {
              Option(task.dueDate).map(task.recurrence.applyRecurrenceToDate) match {
                case Some(newDueDate) =>
                  val recurrentTask = new Task(
                    task.name, task.description, task.id, newDueDate, false, task.recurrence, false)

                  val newTasks = taskList.tasks.filter(_.id != taskId) :+ recurrentTask
                  val updatedTaskList = new TaskList(taskList.name, newTasks, taskList.email)
                  backingStorage.replaceTaskList(taskListName, updatedTaskList) match {
                    case Success(insertedTaskList) =>
                      println(s"Recurrence updated for task ${recurrentTask.id} in list ${insertedTaskList.name}.")
                    case Failure(ex) =>
                      println(s"Could not update task with recurrence ${ex.getMessage}.")
                  }
                case None =>
                  println(s"Could not handle recurrence ${task.recurrence.name} for task $taskId in list $taskListName.")
              }
            }
          case None =>
            println(s"Could not handle recurrence for task $taskId in list $taskListName. Task id does not exist.")
        }
    }
  }

  private def modifyTaskList(taskListName: String, newTaskListName: Option[String], newEmail: Option[String]): Unit = {
    backingStorage.loadTaskList(taskListName) match {
      case Success(taskList) =>
        val newTaskList = new TaskList(newTaskListName.getOrElse(taskList.name),
          taskList.tasks,
          newEmail.getOrElse(taskList.email))

        backingStorage.replaceTaskList(taskListName, newTaskList) match {
          case Success(insertedTaskList) =>
            sender ! new UpdatedTaskList(insertedTaskList)
          case Failure(ex) =>
            sender ! Failure(ex)
        }
      case Failure(ex) =>
        sender ! Failure(ex)
    }
  }

  private def modifyTask(taskId: Long,
                         taskListName: String,
                         newTaskNameOpt: Option[String],
                         newDescriptionOpt: Option[String],
                         newDueDateOpt: Option[ZonedDateTime],
                         recurrence: Recurrence): Unit = {

    backingStorage.loadTaskList(taskListName) match {
      case Success(taskList) =>
        taskList.tasks.find(_.id == taskId) match {
          case Some(task) =>
            val newDueDate: ZonedDateTime = newDueDateOpt.getOrElse(task.dueDate)
            val updatedTask = new Task(newTaskNameOpt.getOrElse(task.name),
              newDescriptionOpt.getOrElse(task.description),
              task.id,
              newDueDate,
              task.done,
              recurrence,
              task.reminderSent)

            val updatedTasks = taskList.tasks.filter(_.id != taskId) :+ updatedTask
            val updatedTaskList = new TaskList(taskList.name, updatedTasks, taskList.email)

            backingStorage.replaceTaskList(taskListName, updatedTaskList) match {
              case Success(insertedTaskList) =>
                sender ! new UpdatedTaskList(insertedTaskList)
              case Failure(ex) =>
                sender ! Failure(ex)
            }
          case None =>
            sender ! Failure(UnknownTaskException(taskId, taskListName))
        }
      case Failure(ex) =>
        sender ! Failure(ex)
    }
  }

  /**
    * Handles unknown command.
    */
  private def logIgnore: Receive = {
    case command =>
      log.warning(s"Received and ignored command: $command")
      sender ! new ServerError(s"Command executor could not handle command: $command")
  }
}

case class UnknownTaskException(taskId: Long, taskListName: String) extends Exception {
  override def toString: String = s"Could not find task id $taskId in list $taskListName"
}
