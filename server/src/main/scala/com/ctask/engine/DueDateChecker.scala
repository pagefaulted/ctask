package com.ctask.engine

import java.time.ZonedDateTime
import java.util.concurrent.Executors

import akka.actor.{Actor, ActorLogging, Props}
import com.ctask.data.{Task, TaskList}

import scala.concurrent.{ExecutionContext, Future}

object DueDateChecker {

  def props(expiredDueDateHandler: ExpiredDueDateHandler, reminderListener: ReminderListener): Props = {
    Props(new DueDateChecker(expiredDueDateHandler, reminderListener))
  }

  // Messages

  /**
    * Checks the due date of the tasks in the task list.
    * If a task is not done and its due date is expired, a callback is executed.
    * The callback implementation is passed in the actor creation.
    * @param taskList the task list to check the tasks of
    */
  case class CheckDueDate(taskList: TaskList)

  private val dueDateCheckerExCtx = ExecutionContext.fromExecutor(Executors.newSingleThreadExecutor())
}

/**
  * Implementors can expect a callback when a task reminder is sent.
  */
trait ReminderListener {

  /**
    * Callback executed when a reminder is sent out for the task.
    *
    * @param taskListName the name of the tasklist
    * @param taskId       the task id
    */
  def reminderSent(taskListName: String, taskId: Long): Unit
}

/**
  * Checks tasks due dates and executes a callback if the due date has expired.
  */
class DueDateChecker(expiredDueDateHandler: ExpiredDueDateHandler, reminderUpdater: ReminderListener) extends Actor with ActorLogging {
  import DueDateChecker._

  override def receive: Receive = handleMessages orElse logIgnore

  def handleMessages: Receive = {
    case CheckDueDate(taskList) =>
      checkDueDate(taskList)
  }

  /**
    * Checks the due date of all the tasks in the provided task list.
    *
    * @param taskList the task list
    */
  def checkDueDate(taskList: TaskList): Unit = {
    val currentDateTime = ZonedDateTime.now

    Future {
      taskList.tasks.foreach { task =>
        Option(task.dueDate) match {
          case Some(dueDate) =>
            if (!task.done && !task.reminderSent && currentDateTime.isAfter(dueDate)) {
              expiredDueDateHandler.onDueDateExpired(task, taskList)
              reminderUpdater.reminderSent(taskList.name, task.id)
            } else {
              expiredDueDateHandler.onDueDateNonExpired
            }
          case None =>
            expiredDueDateHandler.onDueDateNonExpired
        }
      }
    }(dueDateCheckerExCtx)
  }

  /**
    * Handles unknown msg.
    */
  private def logIgnore: Receive = {
    case msg =>
      log.warning(s"Received and ignored message: $msg")
  }
}

object NopExpiredDueDateHandler extends ExpiredDueDateHandler {
  override def onDueDateExpired(task: Task, taskList: TaskList): Unit = {}
}

/**
  * Handler for tasks with expired due date.
  */
trait ExpiredDueDateHandler {

  /**
    * Callback invoked on task's due date expired.
    *
    * @param task     the task whose due date has expired
    * @param taskList the task list that owns the task
    */
  def onDueDateExpired(task: Task, taskList: TaskList): Unit

  /**
    * Callback invoked on task's due date non expired.
    */
  def onDueDateNonExpired: Unit = {}
}
