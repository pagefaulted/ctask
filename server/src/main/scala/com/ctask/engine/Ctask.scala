package com.ctask.engine

import akka.actor.{Actor, ActorLogging, ActorRef, Props}
import akka.pattern.ask
import akka.util.Timeout
import com.ctask.ServerProperties
import com.ctask.engine.Ctask._
import com.ctask.engine.DueDateChecker.CheckDueDate
import com.ctask.messages.ServerResponses.{Answer, MarkedReminderSent, ServerError, TaskLists}
import com.ctask.messages._

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.{Await, Future}
import scala.concurrent.duration._
import scala.util.{Failure, Success}

object Ctask {

  val pongMsg = "I am the Ctask actor!"

  /**
    * Returns a props for the ctask actor.
    *
    * @param commandExecutor       the command executor the ctask actor will be initialized with
    * @param expiredDueDateHandler the handler of tasks's expired due date
    * @return the ctask actor props.
    */
  def props(commandExecutor: ActorRef, expiredDueDateHandler: ExpiredDueDateHandler = NopExpiredDueDateHandler): Props = {
    Props(new Ctask(commandExecutor, expiredDueDateHandler))
  }

  // Ctask messages
  case class Ping(msg: String)

  case class Pong(msg: String)

  case class ReminderSent(taskId: Long, taskListName: String)

  case object Terminate

  case object SendReminders

}

/**
  * Ctask actor.
  */
class Ctask(commandExecutor: ActorRef, expiredDueDateHandler: ExpiredDueDateHandler)
  extends Actor with ActorLogging with ReminderListener {

  val dueDateChecker = context.actorOf(DueDateChecker.props(expiredDueDateHandler, this))

  context.system.scheduler.scheduleWithFixedDelay(0 milliseconds, ServerProperties.reminderIntervalSec seconds, self, SendReminders)

  override def receive: Receive = handleMessages orElse logIgnore

  /**
    * Sends a message to self to mark the task in order to know that the reminder has been sent.
    *
    * @param taskListName the name of the tasklist
    * @param taskId       the task id
    */
  override def reminderSent(taskListName: String, taskId: Long): Unit = self ! ReminderSent(taskId, taskListName)

  private def handleMessages: Receive = {
    case Ping(_) =>
      sender() ! Pong(Ctask.pongMsg)
    case command: Command =>
      invokeExecutor(command, sender)
    case SendReminders =>
      sendReminders
    case ReminderSent(taskId, taskListName) =>
      invokeExecutor(MarkReminderSent(taskId, taskListName), self)
    case m: MarkedReminderSent =>
      println(s"Task ${m.markedTask.id} in list ${m.taskListName} marked with reminder sent.")
    case Terminate =>
      stopCommandExecutor
      stopDueDateChecker
      context.system.terminate()
  }

  private def invokeExecutor(command: Command, originator: ActorRef): Unit = {
    commandExecutor.ask(command)(8 seconds).onComplete {
      case Success(result) =>
        result match {
          case Failure(ex) =>
            originator ! new ServerError(ex.getMessage)
          case response: Answer =>
            originator ! response
        }
      case Failure(ex) =>
        originator ! new ServerError(ex.getMessage)
    }
  }

  private def sendReminders: Unit = {
    implicit val timeout = Timeout(5 seconds)

    Future {
      val taskListsFut = commandExecutor ? ShowTaskLists
      val taskLists = Await.result(taskListsFut, 10 seconds).asInstanceOf[TaskLists]
      taskLists.taskLists.forEach(dueDateChecker ! CheckDueDate(_))
    }
  }

  /**
    * Handles unknown commands.
    */
  private def logIgnore: Receive = {
    case command =>
      log.warning(s"Received and ignored command: $command")
      sender() ! new ServerError(s"Server could not recognize command: $command")
  }

  private def stopCommandExecutor: Unit = {
    context.stop(commandExecutor)
    log.debug("Stopped command executor")
  }

  private def stopDueDateChecker: Unit = {
    context.stop(dueDateChecker)
    log.debug("Stopped due date checker")
  }
}
