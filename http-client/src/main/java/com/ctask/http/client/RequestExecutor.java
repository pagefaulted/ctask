package com.ctask.http.client;

import com.ctask.data.Task;
import com.ctask.data.TaskList;
import com.ctask.http.client.api.CtaskHttpClient;
import com.ctask.messages.ServerResponses;
import com.ctask.protocol.Envelope;
import com.ctask.protocol.data.Data;
import com.ctask.protocol.response.Response;
import com.ctask.protocol.util.Proto2Response;
import com.ctask.protocol.util.RequestEnvelopeUtils;
import com.ctask.protocol.util.Response2Proto;
import com.google.protobuf.InvalidProtocolBufferException;

import java.time.ZonedDateTime;

public class RequestExecutor {

    public RequestExecutor(final CtaskHttpClient httpClient) {
        this.httpClient = httpClient;
    }

    private final CtaskHttpClient httpClient;

    public static class AnswerOrError<T extends ServerResponses.Answer> {
        public final T response;
        public final ServerResponses.ServerError error;

        private AnswerOrError(final T response) {
            this.response = response;
            this.error = null;
        }

        private AnswerOrError(final ServerResponses.ServerError error) {
            this.response = null;
            this.error = error;
        }

        public static <T extends ServerResponses.Answer> AnswerOrError<T> right(final T response) {
            return new AnswerOrError<T>(response);
        }

        public static <T extends ServerResponses.Answer> AnswerOrError<T> left(final ServerResponses.ServerError error) {
            return new AnswerOrError<T>(error);
        }

        public boolean isAnswer() {
            return error == null;
        }
    }

    public AnswerOrError<ServerResponses.UpdatedTaskList> execAddTaskRequest(final String taskName,
                                                                             final String taskListName,
                                                                             final String description,
                                                                             final String dueDateStr,
                                                                             final Task.Recurrence recurrence) {

        final Envelope.RootEnvelopeProto reqEnvelope =
                RequestEnvelopeUtils.createAddTaskToTasklistCommandEnvelope(taskName,
                        taskListName,
                        description,
                        dueDateStr,
                        Response2Proto.recurrenceToRecurrenceType(recurrence));

        return execTaskListUpdate(reqEnvelope);
    }

    public AnswerOrError<ServerResponses.AddedTaskList> execAddTaskList(final String taskListName, final String email) {
        final Envelope.RootEnvelopeProto reqEnvelope = RequestEnvelopeUtils.createAddTaskListCommandEnvelope(taskListName, email);

        final Response.ResponseProto resProto = httpClient.execRequest(reqEnvelope.toByteArray());

        switch (resProto.getResponseType()) {
            case TASK_LIST:
                return proto2AddedTaskList(resProto);
            case ERROR:
                return AnswerOrError.left(proto2ServerError(resProto));
            default:
                return AnswerOrError.left(new ServerResponses.ServerError("Unknown response type " + resProto.getResponseType().name()));
        }
    }

    public AnswerOrError<ServerResponses.UpdatedTaskList> execMaskTaskDoneRequest(final long taskId, final String taskListName) {
        final Envelope.RootEnvelopeProto reqEnvelope =
                RequestEnvelopeUtils.createMarkTaskDoneEnvelope(taskId, taskListName);

        return execTaskListUpdate(reqEnvelope);
    }

    public AnswerOrError<ServerResponses.SingleList> execGetTaskList(final String taskListName, final Boolean withCompleted) {
        final Envelope.RootEnvelopeProto reqEnvelope = RequestEnvelopeUtils.createGetTaskListCommandEnvelope(taskListName, withCompleted);

        final Response.ResponseProto resProto = httpClient.execRequest(reqEnvelope.toByteArray());

        switch (resProto.getResponseType()) {
            case TASK_LIST:
                return proto2SingleList(resProto);
            case ERROR:
                return AnswerOrError.left(proto2ServerError(resProto));
            default:
                return AnswerOrError.left(new ServerResponses.ServerError("Unknown response type " + resProto.getResponseType().name()));
        }
    }

    public AnswerOrError<ServerResponses.UpdatedTaskList> execModifyTaskRequest(final long taskId,
                                                                                final String taskListName,
                                                                                final String newTaskName,
                                                                                final String newDesc,
                                                                                final String newDueDate,
                                                                                final Task.Recurrence newRecurrence) {

        final Envelope.RootEnvelopeProto reqEnvelope =
                RequestEnvelopeUtils.createModifyTaskCommandEnvelope(taskId, taskListName, newTaskName, newDesc, newDueDate,
                        Response2Proto.recurrenceToRecurrenceType(newRecurrence));

        return execTaskListUpdate(reqEnvelope);
    }

    private AnswerOrError<ServerResponses.UpdatedTaskList> execTaskListUpdate(final Envelope.RootEnvelopeProto updateEnvelope) {
        final Response.ResponseProto resProto = httpClient.execRequest(updateEnvelope.toByteArray());

        switch (resProto.getResponseType()) {
            case TASK_LIST:
                return proto2UpdatedTaskList(resProto);
            case ERROR:
                return AnswerOrError.left(proto2ServerError(resProto));
            default:
                return AnswerOrError.left(new ServerResponses.ServerError("Unknown response type " + resProto.getResponseType().name()));
        }
    }

    private ServerResponses.ServerError proto2ServerError(final Response.ResponseProto error) {
        if (error.getResponseType() == Response.ResponseType.ERROR) {
            try {
                return new ServerResponses.ServerError(Data.ErrorProto.parseFrom(error.getPayload()).getMessage());
            } catch (final InvalidProtocolBufferException ex) {
                return new ServerResponses.ServerError(ex.getMessage());
            }
        }
        else {
            return new ServerResponses.ServerError("Response was not an error, it was " + error.getResponseType().name());
        }
    }

    private AnswerOrError<ServerResponses.UpdatedTaskList> proto2UpdatedTaskList(final Response.ResponseProto responseProto) {
        try {
            final Data.TaskListProto taskListProto = Data.TaskListProto.parseFrom(responseProto.getPayload());

            final Task[] tasks = extractTasksFromTaskListProto(taskListProto);
            return AnswerOrError.right(new ServerResponses.UpdatedTaskList(new TaskList(taskListProto.getName(), tasks, null)));
        }
        catch (final InvalidProtocolBufferException ex) {
            return AnswerOrError.left(new ServerResponses.ServerError(ex.getMessage()));
        }
    }

    private AnswerOrError<ServerResponses.SingleList> proto2SingleList(final Response.ResponseProto responseProto) {
        try {
            final Data.TaskListProto taskListProto = Data.TaskListProto.parseFrom(responseProto.getPayload());

            final Task[] tasks = extractTasksFromTaskListProto(taskListProto);
            return AnswerOrError.right(new ServerResponses.SingleList(new TaskList(taskListProto.getName(), tasks, null)));
        }
        catch (final InvalidProtocolBufferException ex) {
            return AnswerOrError.left(new ServerResponses.ServerError(ex.getMessage()));
        }
    }

    private AnswerOrError<ServerResponses.AddedTaskList> proto2AddedTaskList(final Response.ResponseProto responseProto) {
        try {
            final Data.TaskListProto taskListProto = Data.TaskListProto.parseFrom(responseProto.getPayload());

            final Task[] tasks = extractTasksFromTaskListProto(taskListProto);
            return AnswerOrError.right(new ServerResponses.AddedTaskList(new TaskList(taskListProto.getName(), tasks, null)));
        }
        catch (final InvalidProtocolBufferException ex) {
            return AnswerOrError.left(new ServerResponses.ServerError(ex.getMessage()));
        }
    }

    private Task[] extractTasksFromTaskListProto(final Data.TaskListProto taskListProto) {
        return taskListProto.getTasksList().stream().map((taskProto) -> {
            final ZonedDateTime dueDate = taskProto.hasDueDate() ?
                    ZonedDateTime.parse(taskProto.getDueDate().getDateTimeStr()) : null;

            return new Task(taskProto.getName(),
                    taskProto.getDescription(),
                    taskProto.getId(),
                    dueDate,
                    taskProto.getDone(),
                    Proto2Response.recurrenceTypeToRecurrence(taskProto.getRecurrence().getRecurrence()),
                    false);
        }).toArray(Task[]::new);
    }
}
