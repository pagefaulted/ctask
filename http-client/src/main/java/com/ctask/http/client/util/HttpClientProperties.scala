package com.ctask.http.client.util

import com.ctask.utils.PropertiesRetriever
import com.typesafe.config.{Config, ConfigFactory}

object HttpClientProperties extends PropertiesRetriever {

  override def config: Config = ConfigFactory.load()

  /**
    * Server host.
    */
  val serverHostProp: String = "com.ctask.http.client.serverHost"
  val serverHostDefault: String = "localhost"
  def serverHost: String = getPropertyStr(serverHostProp, serverHostDefault)

  /**
    * Server port.
    */
  val serverPortProp: String = "com.ctask.http.client.serverPort"
  val serverPortDefault: Int = 2552
  def serverPort: Int = getPropertyInt(serverPortProp, serverPortDefault)
}
