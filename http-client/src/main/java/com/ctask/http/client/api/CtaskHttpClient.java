package com.ctask.http.client.api;

import com.ctask.protocol.response.Response;

public interface CtaskHttpClient {
    Response.ResponseProto execRequest(final byte[] reqBytes);
}
