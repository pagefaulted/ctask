package com.ctask.http.client.akka;

import akka.actor.ActorSystem;
import akka.http.javadsl.Http;
import akka.http.javadsl.model.ContentTypes;
import akka.http.javadsl.model.HttpRequest;
import akka.http.javadsl.model.HttpResponse;
import akka.stream.javadsl.Sink;
import akka.util.ByteString;
import com.ctask.http.client.api.CtaskHttpClient;
import com.ctask.http.client.util.HttpClientProperties;
import com.ctask.protocol.Envelope;
import com.ctask.protocol.data.Data;
import com.ctask.protocol.response.Response;
import com.google.protobuf.InvalidProtocolBufferException;

import java.util.concurrent.CompletionStage;
import java.util.concurrent.ExecutionException;
import java.util.function.Function;

import static com.ctask.protocol.Envelope.RootEnvelopeProto.MissiveType.RESPONSE;

public class AkkaClient implements CtaskHttpClient {

    public AkkaClient(final ActorSystem system) {
        this.system = system;
    }

    private final ActorSystem system;

    @Override
    public Response.ResponseProto execRequest(byte[] reqBytes) {
        return execRequest(reqBytes, system);
    }

    private Response.ResponseProto execRequest(final byte[] reqBytes, final ActorSystem system) {
        final HttpRequest httpRequest = getHttpRequest(reqBytes);
        final CompletionStage<Response.ResponseProto> reqFuture = Http.get(system)
                .singleRequest(httpRequest)
                .thenApply(unmarshallResponseFn(system));

        try {
            return reqFuture.toCompletableFuture().get();
        } catch (InterruptedException | ExecutionException e) {
            e.printStackTrace();
            return unmarshallingErrorResponse(e.getMessage());
        }
    }

    private Function<HttpResponse, Response.ResponseProto> unmarshallResponseFn(final ActorSystem system) {
        return ( httpResponse -> {
            try {
                return httpResponse.entity().getDataBytes()
                        .runWith(Sink.seq(), system)
                        .thenApply((bytes) -> {
                            final ByteString payload = bytes.stream()
                                    .reduce(ByteString.emptyByteString(), ByteString::concat);

                            try {
                                final Envelope.RootEnvelopeProto rep = Envelope.RootEnvelopeProto.parseFrom(payload.toArray());
                                if (rep.getMissiveType() == RESPONSE) {
                                    return Response.ResponseProto.parseFrom(rep.getPayload());
                                }
                                else {
                                    return unmarshallingErrorResponse("Unexpected missive type: " + rep.getMissiveType().name());
                                }

                            } catch (InvalidProtocolBufferException e) {
                                e.printStackTrace();
                                return unmarshallingErrorResponse(e.getMessage());
                            }
                        }).toCompletableFuture().get();
            } catch (InterruptedException | ExecutionException e) {
                e.printStackTrace();
                return unmarshallingErrorResponse(e.getMessage());
            }
        });
    }

    private Response.ResponseProto unmarshallingErrorResponse(final String message) {
        return Response.ResponseProto.newBuilder()
                .setResponseType(Response.ResponseType.ERROR)
                .setPayload(Data.ErrorProto.newBuilder()
                        .setName("UnmarshallingError")
                        .setMessage(message)
                        .build().toByteString())
                .build();
    }

    private HttpRequest getHttpRequest(final byte[] reqBytes) {
        return HttpRequest.POST("http://" + HttpClientProperties.serverHost() + ":" + HttpClientProperties.serverPort() + "/command")
                .withEntity(ContentTypes.APPLICATION_OCTET_STREAM, reqBytes);
    }
}
