// Scalastyle plugin
addSbtPlugin("org.scalastyle" %% "scalastyle-sbt-plugin" % "1.0.0")

// Coverage report
addSbtPlugin("org.scoverage" % "sbt-scoverage" % "1.6.1")

// Packager
addSbtPlugin("com.typesafe.sbt" % "sbt-native-packager" % "1.2.2")

// Bintray publisher
addSbtPlugin("org.foundweekends" % "sbt-bintray" % "0.5.4")

// Protobuf
addSbtPlugin("com.github.gseitz" % "sbt-protobuf" % "0.6.5")

// Project dependencies
addSbtPlugin("net.virtual-void" % "sbt-dependency-graph" % "0.10.0-RC1")

addSbtPlugin("net.virtual-void" % "sbt-dependency-graph" % "0.9.2")