package com.ctask.protocol.util;

import com.ctask.data.Task;
import com.ctask.data.TaskList;
import com.ctask.messages.ServerResponses;
import com.ctask.protocol.data.Data;
import com.ctask.protocol.data.Data.DueDateProto;
import com.ctask.protocol.data.Data.RecurrenceProto;
import com.ctask.protocol.data.Data.RecurrenceType;
import com.ctask.protocol.Envelope.RootEnvelopeProto;
import com.ctask.protocol.Envelope.RootEnvelopeProto.MissiveType;
import com.ctask.protocol.data.Data.TaskListProto;
import com.ctask.protocol.data.Data.TaskProto;
import com.ctask.protocol.response.Response;
import com.ctask.protocol.response.Response.ResponseProto;

import java.time.ZonedDateTime;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

/**
  * From server response to proto converters.
  */
public class Response2Proto {

  public static RootEnvelopeProto singleList2TaskListProto(final ServerResponses.SingleList singleList) {
      return taskList2TaskListProto(singleList.list);
  }

  public static RootEnvelopeProto updatedList2TaskListProto(final ServerResponses.UpdatedTaskList updatedTaskList) {
      return taskList2TaskListProto(updatedTaskList.list);
  }

  public static RootEnvelopeProto addedTaskList2TaskListProto(final ServerResponses.AddedTaskList addedTaskList) {
      return taskList2TaskListProto(addedTaskList.list);
  }

  public static RootEnvelopeProto taskList2TaskListProto(final TaskList taskList) {
      final List<TaskProto> allTasksProto = Arrays.stream(taskList.tasks).map(t -> {
          final DueDateProto dueDate = DueDateProto
                  .newBuilder()
                  .setIsPresent(t.dueDate != null)
                  .setDateTimeStr(Optional.ofNullable(t.dueDate).orElseGet(ZonedDateTime::now).toString())
                  .build();

          final RecurrenceProto recurrenceProto = RecurrenceProto
                  .newBuilder()
                  .setRecurrence(recurrenceToRecurrenceType(t.recurrence))
                  .build();

          return TaskProto
                  .newBuilder()
                  .setId(t.id)
                  .setName(t.name)
                  .setDescription(t.description)
                  .setDueDate(dueDate)
                  .setDone(t.done)
                  .setRecurrence(recurrenceProto)
                  .build();
      }).collect(Collectors.toList());

      final TaskListProto taskListProto = TaskListProto
              .newBuilder()
              .setName(taskList.name)
              .addAllTasks(allTasksProto)
              .build();

      final ResponseProto responseProto = ResponseProto
              .newBuilder()
              .setResponseType(Response.ResponseType.TASK_LIST)
              .setPayload(taskListProto.toByteString())
              .build();

      return RootEnvelopeProto
              .newBuilder()
              .setMissiveType(MissiveType.RESPONSE)
              .setPayload(responseProto.toByteString())
              .build();
  }

  public static RootEnvelopeProto serverError2ErrorProto(final ServerResponses.ServerError serverError) {
      final Data.ErrorProto errorProto = Data.ErrorProto
              .newBuilder()
              .setName(serverError.getClass().getCanonicalName())
              .setMessage(serverError.msg)
              .build();

      final ResponseProto responseProto = ResponseProto
              .newBuilder()
              .setResponseType(Response.ResponseType.ERROR)
              .setPayload(errorProto.toByteString())
              .build();

      return RootEnvelopeProto
              .newBuilder()
              .setMissiveType(MissiveType.RESPONSE)
              .setPayload(responseProto.toByteString())
              .build();
  }

  public static RecurrenceType recurrenceToRecurrenceType(Task.Recurrence recurrence) {
      if (recurrence.equals(Task.Recurrence.DAILY)) {
          return RecurrenceType.DAILY;
      }
      else if (recurrence.equals(Task.Recurrence.WEEKLY)) {
          return RecurrenceType.WEEKLY;
      }
      else if (recurrence.equals(Task.Recurrence.MONTHLY)) {
          return RecurrenceType.MONTHLY;
      }
      else if (recurrence.equals(Task.Recurrence.YEARLY)) {
          return RecurrenceType.YEARLY;
      }
      else if (recurrence.equals(Task.Recurrence.NEVER)) {
          return RecurrenceType.NEVER;
      }
      else {
          return RecurrenceType.NEVER;
      }
  }
}
