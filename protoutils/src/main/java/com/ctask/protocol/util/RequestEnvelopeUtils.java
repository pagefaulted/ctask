package com.ctask.protocol.util;

import com.ctask.protocol.Envelope;
import com.ctask.protocol.commands.Commands;
import com.ctask.protocol.data.Data;
import com.google.protobuf.ByteString;
import com.google.protobuf.NullValue;

/**
 * Utilities for the creation of request envelopes.
 */
public class RequestEnvelopeUtils {

    public static Envelope.RootEnvelopeProto createGetTaskListCommandEnvelope(final String taskListName, final boolean withCompleted) {
        final Commands.GetTaskListProto getTaskListProto = Commands.GetTaskListProto.newBuilder()
                .setTaskListName(taskListName)
                .setWithCompleted(withCompleted)
                .build();

        final Commands.CommandEnvelopeProto commandEnvelopeProto = createCommandEnvelope(Commands.CommandTypeProto.GET_TASK_LIST,
                getTaskListProto.toByteString());

        return createRootEnvelope(Envelope.RootEnvelopeProto.MissiveType.COMMAND, commandEnvelopeProto.toByteString());

    }

    public static Envelope.RootEnvelopeProto createMarkTaskDoneEnvelope(final long taskId, final String taskListName) {
        final Commands.MarkTaskDoneProto markTaskDoneProto = Commands.MarkTaskDoneProto.newBuilder()
                .setTaskId(taskId)
                .setTaskListName(taskListName)
                .build();

        final Commands.CommandEnvelopeProto commandEnvelopeProto = createCommandEnvelope(Commands.CommandTypeProto.MARK_TASK_DONE,
                markTaskDoneProto.toByteString());

        return createRootEnvelope(Envelope.RootEnvelopeProto.MissiveType.COMMAND, commandEnvelopeProto.toByteString());
    }

    public static Envelope.RootEnvelopeProto createAddTaskListCommandEnvelope(final String name,
                                                                              final String email) {

        final Commands.AddTaskListProto addTaskListProto = Commands.AddTaskListProto.newBuilder().setName(name).setEmail(email).build();

        final Commands.CommandEnvelopeProto commandEnvelopeProto =
                createCommandEnvelope(Commands.CommandTypeProto.ADD_TASK_LIST, addTaskListProto.toByteString());

        return createRootEnvelope(Envelope.RootEnvelopeProto.MissiveType.COMMAND, commandEnvelopeProto.toByteString());
    }

    // TODO: add date validation
    public static Envelope.RootEnvelopeProto createAddTaskToTasklistCommandEnvelope(final String name,
                                                                                    final String taskListName,
                                                                                    final String description,
                                                                                    final String dueDateStr,
                                                                                    final Data.RecurrenceType recurrenceType) {

        final Data.DueDateProto dueDateProto = Data.DueDateProto.newBuilder()
                .setDateTimeStr(dueDateStr)
                .setIsPresent(!dueDateStr.isEmpty())
                .build();

        final Data.RecurrenceProto recurrenceProto = Data.RecurrenceProto.newBuilder()
                .setRecurrence(recurrenceType)
                .build();

        final Commands.AddTaskToTaskListProto addTaskToTaskListProto = Commands.AddTaskToTaskListProto.newBuilder()
                .setName(name)
                .setTaskListName(taskListName)
                .setDescription(description)
                .setDueDate(dueDateProto)
                .setRecurrence(recurrenceProto)
                .build();

        final Commands.CommandEnvelopeProto commandEnvelopeProto =
                createCommandEnvelope(Commands.CommandTypeProto.ADD_TASK_TO_TASKLIST,
                        addTaskToTaskListProto.toByteString());

        return createRootEnvelope(Envelope.RootEnvelopeProto.MissiveType.COMMAND, commandEnvelopeProto.toByteString());
    }

    public static Envelope.RootEnvelopeProto createModifyTaskCommandEnvelope(final Long taskId,
                                                                             final String taskListName,
                                                                             final String newTaskName,
                                                                             final String newDescription,
                                                                             final String dueDateStr,
                                                                             final Data.RecurrenceType recurrenceType) {

        Commands.ModifyTaskProto modifyTaskProto = Commands.ModifyTaskProto.newBuilder()
                .setTaskId(taskId)
                .setTaskListName(taskListName)
                .build();

        if (newTaskName == null) {
            modifyTaskProto = Commands.ModifyTaskProto.newBuilder()
                    .mergeFrom(modifyTaskProto)
                    .setNullTaskName(NullValue.NULL_VALUE)
                    .build();
        } else {
            modifyTaskProto = Commands.ModifyTaskProto.newBuilder()
                    .mergeFrom(modifyTaskProto)
                    .setName(newTaskName)
                    .build();
        }

        if (newDescription == null) {
            modifyTaskProto = Commands.ModifyTaskProto.newBuilder()
                    .mergeFrom(modifyTaskProto)
                    .setNullDesc(NullValue.NULL_VALUE)
                    .build();
        } else {
            modifyTaskProto = Commands.ModifyTaskProto.newBuilder()
                    .mergeFrom(modifyTaskProto)
                    .setDesc(newDescription)
                    .build();
        }

        if (dueDateStr == null) {
            modifyTaskProto = Commands.ModifyTaskProto.newBuilder()
                    .mergeFrom(modifyTaskProto)
                    .setNullDueDate(NullValue.NULL_VALUE)
                    .build();
        } else {
            final Data.DueDateProto dueDateProto = Data.DueDateProto.newBuilder()
                    .setDateTimeStr(dueDateStr)
                    .setIsPresent(true)
                    .build();

            modifyTaskProto = Commands.ModifyTaskProto.newBuilder()
                    .mergeFrom(modifyTaskProto)
                    .setDueDate(dueDateProto)
                    .build();
        }

        final Data.RecurrenceProto recurrenceProto = Data.RecurrenceProto.newBuilder()
                .setRecurrence(recurrenceType)
                .build();

        modifyTaskProto = Commands.ModifyTaskProto.newBuilder()
                .mergeFrom(modifyTaskProto)
                .setRecurrence(recurrenceProto)
                .build();

        final Commands.CommandEnvelopeProto commandEnvelopeProto =
                createCommandEnvelope(Commands.CommandTypeProto.MODIFY_TASK,
                        modifyTaskProto.toByteString());

        return createRootEnvelope(Envelope.RootEnvelopeProto.MissiveType.COMMAND, commandEnvelopeProto.toByteString());

    }

    private static Commands.CommandEnvelopeProto createCommandEnvelope(final Commands.CommandTypeProto commandType,
                                                                       final ByteString payload) {
        return Commands.CommandEnvelopeProto.newBuilder()
                .setCommandType(commandType)
                .setPayload(payload)
                .build();
    }

    private static Envelope.RootEnvelopeProto createRootEnvelope(final Envelope.RootEnvelopeProto.MissiveType missiveType,
                                                                 final ByteString payload) {

        return Envelope.RootEnvelopeProto.newBuilder()
                .setMissiveType(missiveType)
                .setPayload(payload)
                .build();
    }
}
