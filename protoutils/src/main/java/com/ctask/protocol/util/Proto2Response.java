package com.ctask.protocol.util;

import com.ctask.data.Task;
import com.ctask.protocol.data.Data.RecurrenceType;

/**
 * From proto to server response converters.
 */
public class Proto2Response {

    public static Task.Recurrence recurrenceTypeToRecurrence(final RecurrenceType recurrenceType) {
        if (recurrenceType.equals(RecurrenceType.DAILY)) {
            return Task.Recurrence.DAILY;
        }
        else if (recurrenceType.equals(RecurrenceType.WEEKLY)) {
            return Task.Recurrence.WEEKLY;
        }
        else if (recurrenceType.equals(RecurrenceType.MONTHLY)) {
            return Task.Recurrence.MONTHLY;
        }
        else if (recurrenceType.equals(RecurrenceType.YEARLY)) {
            return Task.Recurrence.YEARLY;
        }
        else if (recurrenceType.equals(RecurrenceType.NEVER)) {
            return Task.Recurrence.NEVER;
        }
        else {
            return Task.Recurrence.NEVER;
        }
    }
}
