# ctask [![Build Status](https://travis-ci.org/modsrm/ctask.svg?branch=develop)](https://travis-ci.org/modsrm/ctask) [![codecov](https://codecov.io/gh/modsrm/ctask/branch/develop/graph/badge.svg)](https://codecov.io/gh/modsrm/ctask) [![license](https://img.shields.io/badge/license-GPLv3-red.svg)](https://raw.githubusercontent.com/modsrm/ctask/develop/LICENSE)

**ctask** is a command line based tasks management/reminder app written in Scala.

## Build
To build the project, _sbt build_.

## Package
To package for installation, _sbt pkg_. The _client_ and _server_ tar packages are in _{client,server}/target/universal_, respectively.

## Configuration
Once extracted the client and server tars, the configuration can be found in the respective _conf_ folders.

### Server config
The server config has several sections, as follows.

#### Server details
```
  remote {
    enabled-transports = [akka.remote.netty.tcp]
    netty.tcp {
      hostname = 127.0.0.1
      port = 2552
    }
  }
```

Replace the **hostname** with the ip of the server machine and the **port** with the desired port.

#### Storage config
```
storage {
  root = "/tmp/ctask"
  type = "memory"
}
```

The **root** property defines where the task lists are persisted. Task lists are stored in plain text (unencripted) JSON format. This property is useful only if the _disk_ storage type is enabled.

The **type** property defines where the date is stored. There are 2 options, _memory_ and _disk_. If _memory_ is used, data will be lost when the process is terminated.

#### Reminders config
```
reminder {
  emailUser = "ctask@email"
  emailPwd = "password"
  emailSmtpServer = "smtp.server"
  emailPort = 0
  intervalSec = 60
}
```

If you want reminders to be delivered when a task due date expires, you need to configure the reminder section accordingly. The elements of this section identify the email account that sends out reminders, not the address reminders are sent to.

The **intervalSec** property defines how often are tasks checked to send out reminders, in seconds.

### Client config
```
com {
  ctask {
    client {
      serverHost = 127.0.0.1
      serverPort = 2552

      storage {
        root = ""
      }
    }
  }
}
```
Change **serverHost** and **serverPort** according to the server installation.

The storage **root** is the folder where the client side files will be stored. By default is your home directory.

## Run
To run the **server**, _./bin/ctask-server_.

To run the **client**, _./bin/ctask-client_.

## Client usage, quick start
The client is implemented as a REPL
Here is an example usage.
For a full commands reference, run the _help_ command from the client repl. (TODO: #106)

### Create a task list, add a task with a due date, mark the task as done

The server is assumed to be already running.

1. Start the client
```
mods@cerbero ~/development/ctask/client/target/universal
[13:46:32]$ > ./ctask-client-1.2.3/bin/ctask-client
   _____ _            _      _____  ______ _____  _
  / ____| |          | |    |  __ \|  ____|  __ \| |
 | |    | |_ __ _ ___| | __ | |__) | |__  | |__) | |
 | |    | __/ _` / __| |/ / |  _  /|  __| |  ___/| |
 | |____| || (_| \__ \   <  | | \ \| |____| |    | |____
  \_____|\__\__,_|___/_|\_\ |_|  \_\______|_|    |______|
                                     roberto@pagefault.io


ctask $>
```

2. Add a new task list called TODOs with an associated email. Reminders will be sent to that email when a task due date expires
```
ctask $> add tasklist -n TODOs -e roberto@pagefault.io
Success: Task list added: 'TODOs'
```

3. Try to add a task
```
ctask $> add -n Find a new side project -dd tomorrow
The add command cannot be executed if a task list is not selected
```
In order to interact with tasks, you need to first select a task list to use.

4. Select the newly added task list
```
ctask $> use TODOs
Success: Selected task list TODOs
```

5. Add a task with a due date for tomorrow (tomorrow defaults to tomorrow at 10am)
```
ctask:TODOs $> add -n Find a new side project -dd tomorrow
Success: Task list updated: 'TODOs'
```

6. List all tasks in the task list in use
```
ctask:TODOs $> ls

Tomorrow:
  * 1: Find a new side project - Due: 10:00 11/Nov/2017
```

7. Mark the task as done
```
ctask:TODOs $> done -t 1
Success: Task list updated: 'TODOs'
```
